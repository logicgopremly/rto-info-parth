package com.rtovehicle.information.carbikeinfo.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.Fuel_activity;
import com.rtovehicle.information.carbikeinfo.activity.Main_Activity;
import com.rtovehicle.information.carbikeinfo.adapter.City_Adapter;
import com.rtovehicle.information.carbikeinfo.model.Model_City;
import com.rtovehicle.information.carbikeinfo.common.PrefManager_fuel;

import java.util.List;

public class City_fragment extends Fragment implements City_Adapter.OnClickedState {
    public static final String title = "city";
    public Fuel_activity activity;
    public City_Adapter city_adapter;
    private EditText edit_search;
    private List<Model_City> modelCityList_array;
    public ProgressDialog progressDialog;
    private View view;

    public View onCreateView(@NonNull LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.activity = (Fuel_activity) getActivity();
        if (getArguments() != null) {
            this.modelCityList_array = getArguments().getParcelableArrayList(title);
        }
        this.view = layoutInflater.inflate(R.layout.fragment_city, viewGroup, false);
        hideSoftKeyboard();
        return this.view;
    }

    public void onViewCreated(@NonNull View view2, Bundle bundle) {
        super.onViewCreated(view2, bundle);
        init();
        searchFromEditText();
    }

    public void init() {
        ProgressDialog progressDialog2 = new ProgressDialog(getActivity());
        this.progressDialog = progressDialog2;
        progressDialog2.setTitle("Please wait");
        this.progressDialog.setCancelable(false);
        RecyclerView list_view_city = this.view.findViewById(R.id.list_view_city);
        this.edit_search = this.view.findViewById(R.id.edit_search);
        ImageView rip_back = view.findViewById(R.id.img_view_back);
        rip_back.setOnClickListener(view -> activity.finish());
        City_Adapter adapterCity2 = new City_Adapter(getActivity(), this.modelCityList_array, this.activity);
        this.city_adapter = adapterCity2;
        list_view_city.setAdapter(adapterCity2);
        list_view_city.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.city_adapter.calBack(this);

    }

    private void searchFromEditText() {
        this.edit_search.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                City_fragment.this.city_adapter.filter(charSequence);
            }
        });
    }

    public void getPrice(Model_City modelCity) {
        try {
            if (!modelCity.toString().isEmpty()) {
                Main_Activity.city = true;
                PrefManager_fuel.savePetrol(this.activity, modelCity.getPrice());
                PrefManager_fuel.saveDiesel(this.activity, modelCity.getYPrice());
                PrefManager_fuel.savecity(this.activity, modelCity.getCity());
                this.activity.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callBack(Model_City modelCity) {
        getPrice(modelCity);
    }

    @SuppressLint("WrongConstant")
    public void hideSoftKeyboard() {
        if (requireActivity().getCurrentFocus() != null) {
            ((InputMethodManager) requireActivity().getSystemService("input_method")).hideSoftInputFromWindow(requireActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }
}
