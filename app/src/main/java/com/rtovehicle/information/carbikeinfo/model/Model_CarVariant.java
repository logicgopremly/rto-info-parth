package com.rtovehicle.information.carbikeinfo.model;

public class Model_CarVariant {
    int id;
    String variant_name, variant_price, transmission_type, fuel_type, engine_disp, milege;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVariant_name() {
        return variant_name;
    }

    public void setVariant_name(String variant_name) {
        this.variant_name = variant_name;
    }

    public String getVariant_price() {
        return variant_price;
    }

    public void setVariant_price(String variant_price) {
        this.variant_price = variant_price;
    }

    public String getTransmission_type() {
        return transmission_type;
    }

    public void setTransmission_type(String transmission_type) {
        this.transmission_type = transmission_type;
    }

    public String getFuel_type() {
        return fuel_type;
    }

    public void setFuel_type(String fuel_type) {
        this.fuel_type = fuel_type;
    }

    public String getEngine_disp() {
        return engine_disp;
    }

    public void setEngine_disp(String engine_disp) {
        this.engine_disp = engine_disp;
    }

    public void setMilege(String milege) {
        this.milege = milege;
    }
}
