package com.rtovehicle.information.carbikeinfo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.Fuel_activity;
import com.rtovehicle.information.carbikeinfo.model.Model_City;
import com.rtovehicle.information.carbikeinfo.fragment.State_fragment;

import java.util.ArrayList;
import java.util.List;

public class State_Adapter extends RecyclerView.Adapter<State_Adapter.Holder> {
    private final Context context;
    public final List<Model_City> listState;
    public OnClickedState onClickedState;
    private final List<Model_City> tempList;

    public interface OnClickedState {
        void callBack(List<Model_City> list, String str);
    }

    static class Holder extends RecyclerView.ViewHolder {
        public final RelativeLayout rl_state;
        public final TextView textView;

        public Holder(View view) {
            super(view);
            this.textView = view.findViewById(R.id.tv_state);
            this.rl_state = view.findViewById(R.id.rl_state);
        }
    }

    public State_Adapter(Context context2, List<Model_City> list, Fuel_activity activityBase) {
        ArrayList arrayList = new ArrayList();
        this.tempList = arrayList;
        this.context = context2;
        this.listState = list;
        arrayList.addAll(list);
    }

    public void calBack(OnClickedState onClickedState2) {
        this.onClickedState = onClickedState2;
    }

    @NonNull
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(this.context).inflate(R.layout.state_item_layout, viewGroup, false));
    }

    public void onBindViewHolder(Holder holder, @SuppressLint("RecyclerView") final int i) {
        final String stateName = this.listState.get(i).getStateName();
        holder.textView.setText(this.listState.get(i).getStateName());
        holder.rl_state.setOnClickListener(view -> {
            ArrayList arrayList = new ArrayList<>();
            for (int i1 = 0; i1 < State_fragment.g.size(); i1++) {
                Model_City modelCity = State_fragment.g.get(i1);
                if (stateName.equals(modelCity.getStateName())) {
                    arrayList.add(modelCity);
                }
            }
            State_Adapter.this.onClickedState.callBack(arrayList, State_Adapter.this.listState.get(i).getStateName());
        });
    }

    public int getItemCount() {
        return this.listState.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void filter(CharSequence charSequence) {
        ArrayList arrayList = new ArrayList<>();
        if (TextUtils.isEmpty(charSequence)) {
            arrayList.addAll(this.tempList);
        } else {
            for (Model_City next : this.listState) {
                if (next.getStateName().toLowerCase().contains(charSequence)) {
                    arrayList.add(next);
                }
            }
        }
        this.listState.clear();
        this.listState.addAll(arrayList);
        notifyDataSetChanged();
        arrayList.clear();
    }
}
