package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_Company {
    @SerializedName("status")
    boolean status;

    @SerializedName("data")
    @Expose
    ArrayList<Company_Data> data;

    public static class Company_Data {
        @SerializedName("id")
        int id;

        @SerializedName("company_name")
        String company_name;

        @SerializedName("company_logo")
        String company_logo;

        public int getId() {
            return id;
        }

        public String getCompany_name() {
            return company_name;
        }

        public String getCompany_logo() {
            return company_logo;
        }
    }

    public boolean isStatus() {
        return status;
    }

    public ArrayList<Company_Data> getData() {
        return data;
    }
}
