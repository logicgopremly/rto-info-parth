package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.model.Model_QuestionBank;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class PrepareExam_RTO_Activity extends AppCompatActivity {
    String RTO_JSON;
    int ans;
    Animation fadeinfor_animation;
    Animation fadeoutfor_animation;
    LinearLayout imgLayout_lin;
    int[] img_Arr;

    LinearLayout layoutopt_one;
    LinearLayout layoutopt_two;
    LinearLayout layoutopt_three;
    LinearLayout mainLayout_lin;
    TextView text_option1;
    TextView text_option2;
    TextView text_option3;
    TextView text_que;
    int que_id = 1;
    int que_idex = 0;
    String que_string;
    TextView text_question;
    final ArrayList<Model_QuestionBank> questionBankModels_array = new ArrayList<>();
    ImageView sign_img;
    AdView adView;
    FrameLayout adContainerView;
    Dialog dialog;


    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_prepare_exam_rto);
        getWindow().setBackgroundDrawable(null);
        FindById();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        if (height > 1024) {
            loadBanner();
        }
        this.fadeinfor_animation = AnimationUtils.loadAnimation(this, R.anim.fadeinfor);
        this.fadeoutfor_animation = AnimationUtils.loadAnimation(this, R.anim.fadeoutfor);
        findViewById(R.id.back_all).setOnClickListener(view -> PrepareExam_RTO_Activity.this.onBackPressed());
        ShowDialog();
        String str = "lang";
        String str2 = "english";
        if (getIntent().getStringExtra(str).equalsIgnoreCase(str2)) {
            this.RTO_JSON = loadJSONFromAsset(str2);
            this.que_string = getResources().getStringArray(R.array.que)[0];
            this.img_Arr = Common_Utils.Eng_Arr;
        } else {
            String str3 = "gujarati";
            if (getIntent().getStringExtra(str).equalsIgnoreCase(str3)) {
                this.img_Arr = Common_Utils.Guj_Arr;
                this.que_string = getResources().getStringArray(R.array.que)[1];
                this.RTO_JSON = loadJSONFromAsset(str3);
            } else {
                String stringExtra = getIntent().getStringExtra(str);
                String str4 = "hindi";
                if (stringExtra.equalsIgnoreCase(str4)) {
                    this.img_Arr = Common_Utils.Hindi_Arr;
                    this.que_string = getResources().getStringArray(R.array.que)[2];
                    this.RTO_JSON = loadJSONFromAsset(str4);
                } else {
                    this.que_string = getResources().getStringArray(R.array.que)[3];
                    this.img_Arr = Common_Utils.Marathi_Arr;
                    this.RTO_JSON = loadJSONFromAsset("marathi");
                }
            }
        }
        new LoadData().execute();
        this.layoutopt_one.setOnClickListener(view -> {
            PrepareExam_RTO_Activity.this.que_idex++;
            PrepareExam_RTO_Activity.this.que_id++;
            PrepareExam_RTO_Activity.this.mainLayout_lin.startAnimation(PrepareExam_RTO_Activity.this.fadeoutfor_animation);
            PrepareExam_RTO_Activity.this.layoutopt_one.setClickable(false);
            PrepareExam_RTO_Activity.this.layoutopt_two.setClickable(false);
            PrepareExam_RTO_Activity.this.layoutopt_three.setClickable(false);
        });
        this.layoutopt_two.setOnClickListener(view -> {
            PrepareExam_RTO_Activity.this.que_idex++;
            PrepareExam_RTO_Activity.this.que_id++;
            PrepareExam_RTO_Activity.this.mainLayout_lin.startAnimation(PrepareExam_RTO_Activity.this.fadeoutfor_animation);
            PrepareExam_RTO_Activity.this.layoutopt_one.setClickable(false);
            PrepareExam_RTO_Activity.this.layoutopt_two.setClickable(false);
            PrepareExam_RTO_Activity.this.layoutopt_three.setClickable(false);
        });
        this.layoutopt_three.setOnClickListener(view -> {
            PrepareExam_RTO_Activity.this.que_idex++;
            PrepareExam_RTO_Activity.this.que_id++;
            PrepareExam_RTO_Activity.this.mainLayout_lin.startAnimation(PrepareExam_RTO_Activity.this.fadeoutfor_animation);
            PrepareExam_RTO_Activity.this.layoutopt_one.setClickable(false);
            PrepareExam_RTO_Activity.this.layoutopt_two.setClickable(false);
            PrepareExam_RTO_Activity.this.layoutopt_three.setClickable(false);
        });
        this.fadeinfor_animation.setAnimationListener(new AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                PrepareExam_RTO_Activity.this.layoutopt_one.setClickable(true);
                PrepareExam_RTO_Activity.this.layoutopt_two.setClickable(true);
                PrepareExam_RTO_Activity.this.layoutopt_three.setClickable(true);
            }
        });
        this.fadeoutfor_animation.setAnimationListener(new AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            @SuppressLint("WrongConstant")
            public void onAnimationEnd(Animation animation) {
                if (PrepareExam_RTO_Activity.this.questionBankModels_array.size() == 0) {
                    return;
                }
                if (PrepareExam_RTO_Activity.this.que_idex == PrepareExam_RTO_Activity.this.questionBankModels_array.size() - 1) {
                    PrepareExam_RTO_Activity.this.finish();
                    return;
                }
                Model_QuestionBank questionBankModel = PrepareExam_RTO_Activity.this.questionBankModels_array.get(PrepareExam_RTO_Activity.this.que_idex);
                TextView textView = PrepareExam_RTO_Activity.this.text_que;
                String sb = PrepareExam_RTO_Activity.this.que_string +
                        " " +
                        PrepareExam_RTO_Activity.this.que_id;
                textView.setText(sb);
                PrepareExam_RTO_Activity.this.text_question.setText(questionBankModel.getQuestion());
                String str = "1";
                if (questionBankModel.getIsimage().equalsIgnoreCase(str)) {
                    PrepareExam_RTO_Activity.this.imgLayout_lin.setVisibility(0);
                    PrepareExam_RTO_Activity.this.sign_img.setImageResource(PrepareExam_RTO_Activity.this.img_Arr[PrepareExam_RTO_Activity.this.que_idex]);
                } else {
                    PrepareExam_RTO_Activity.this.imgLayout_lin.setVisibility(8);
                }
                TextView textView2 = PrepareExam_RTO_Activity.this.text_option1;
                String sb2 = "A. " +
                        questionBankModel.getOption()[0];
                textView2.setText(sb2);
                TextView textView3 = PrepareExam_RTO_Activity.this.text_option2;
                String sb3 = "B. " +
                        questionBankModel.getOption()[1];
                textView3.setText(sb3);
                TextView textView4 = PrepareExam_RTO_Activity.this.text_option3;
                String sb4 = "C. " +
                        questionBankModel.getOption()[2];
                textView4.setText(sb4);
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase("0")) {
                    PrepareExam_RTO_Activity.this.layoutopt_one.setBackgroundResource(R.drawable.right_ans);
                    PrepareExam_RTO_Activity.this.text_option1.setTextColor(Color.WHITE);
                    PrepareExam_RTO_Activity.this.text_option2.setTextColor(Color.BLACK);
                    PrepareExam_RTO_Activity.this.text_option3.setTextColor(Color.BLACK);
                    PrepareExam_RTO_Activity.this.ans = 0;
                } else {
                    PrepareExam_RTO_Activity.this.layoutopt_one.setBackgroundResource(R.drawable.ans_bg);


                }
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase(str)) {
                    PrepareExam_RTO_Activity.this.layoutopt_two.setBackgroundResource(R.drawable.right_ans);
                    PrepareExam_RTO_Activity.this.text_option2.setTextColor(Color.WHITE);
                    PrepareExam_RTO_Activity.this.text_option1.setTextColor(Color.BLACK);
                    PrepareExam_RTO_Activity.this.text_option3.setTextColor(Color.BLACK);
                    PrepareExam_RTO_Activity.this.ans = 1;
                } else {
                    PrepareExam_RTO_Activity.this.layoutopt_two.setBackgroundResource(R.drawable.ans_bg);

                }
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase("2")) {
                    PrepareExam_RTO_Activity prepareExamActivity = PrepareExam_RTO_Activity.this;
                    prepareExamActivity.ans = 2;
                    prepareExamActivity.layoutopt_three.setBackgroundResource(R.drawable.right_ans);
                    PrepareExam_RTO_Activity.this.text_option3.setTextColor(Color.WHITE);
                    PrepareExam_RTO_Activity.this.text_option2.setTextColor(Color.BLACK);
                    PrepareExam_RTO_Activity.this.text_option1.setTextColor(Color.BLACK);

                } else {
                    PrepareExam_RTO_Activity.this.layoutopt_three.setBackgroundResource(R.drawable.ans_bg);

                }
                PrepareExam_RTO_Activity.this.mainLayout_lin.startAnimation(PrepareExam_RTO_Activity.this.fadeinfor_animation);
            }
        });
    }

    private void FindById() {
        mainLayout_lin = findViewById(R.id.mainLayout);
        layoutopt_one = findViewById(R.id.layoutopt1);
        layoutopt_two = findViewById(R.id.layoutopt2);
        layoutopt_three = findViewById(R.id.layoutopt3);
        imgLayout_lin = findViewById(R.id.imgLayout);
        text_que = findViewById(R.id.que);
        text_question = findViewById(R.id.question);
        text_option1 = findViewById(R.id.option1);
        text_option2 = findViewById(R.id.option2);
        text_option3 = findViewById(R.id.option3);
        sign_img = findViewById(R.id.sign);
        adContainerView = findViewById(R.id.adContainerView);

    }

    private void loadBanner() {
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);


        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }


    @SuppressLint("StaticFieldLeak")
    private class LoadData extends AsyncTask<Void, Void, Void> {
        private LoadData() {
        }


        public Void doInBackground(Void... voidArr) {
            try {
                JSONArray jSONArray = new JSONObject(PrepareExam_RTO_Activity.this.RTO_JSON).getJSONArray("data");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                    Model_QuestionBank questionBankModel = new Model_QuestionBank();
                    questionBankModel.setAnswer(jSONObject.getString("Answer"));
                    questionBankModel.setQuestion(jSONObject.getString("Question"));
                    questionBankModel.setIsimage(jSONObject.getString("Isimage"));
                    questionBankModel.setCorrectAnswer(jSONObject.getString("correctAnswer"));
                    JSONArray jSONArray2 = jSONObject.getJSONArray("option");
                    String[] strArr = new String[jSONArray2.length()];
                    for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                        strArr[i2] = (String) jSONArray2.get(i2);
                    }
                    questionBankModel.setOption(strArr);
                    PrepareExam_RTO_Activity.this.questionBankModels_array.add(questionBankModel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }


        @SuppressLint("WrongConstant")
        public void onPostExecute(Void voidR) {
            super.onPostExecute(voidR);
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (PrepareExam_RTO_Activity.this.questionBankModels_array.size() != 0) {
                Model_QuestionBank questionBankModel = PrepareExam_RTO_Activity.this.questionBankModels_array.get(PrepareExam_RTO_Activity.this.que_idex);
                TextView textView = PrepareExam_RTO_Activity.this.text_que;
                String sb = PrepareExam_RTO_Activity.this.que_string +
                        " " +
                        PrepareExam_RTO_Activity.this.que_id;
                textView.setText(sb);
                PrepareExam_RTO_Activity.this.text_question.setText(questionBankModel.getQuestion());
                String str = "1";
                if (questionBankModel.getIsimage().equalsIgnoreCase(str)) {
                    PrepareExam_RTO_Activity.this.imgLayout_lin.setVisibility(0);
                    PrepareExam_RTO_Activity.this.sign_img.setImageResource(PrepareExam_RTO_Activity.this.img_Arr[PrepareExam_RTO_Activity.this.que_idex]);
                } else {
                    PrepareExam_RTO_Activity.this.imgLayout_lin.setVisibility(8);
                }
                TextView textView2 = PrepareExam_RTO_Activity.this.text_option1;
                String sb2 = "A. " +
                        questionBankModel.getOption()[0];
                textView2.setText(sb2);
                TextView textView3 = PrepareExam_RTO_Activity.this.text_option2;
                String sb3 = "B. " +
                        questionBankModel.getOption()[1];
                textView3.setText(sb3);
                TextView textView4 = PrepareExam_RTO_Activity.this.text_option3;
                String sb4 = "C. " +
                        questionBankModel.getOption()[2];
                textView4.setText(sb4);
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase("0")) {
                    PrepareExam_RTO_Activity.this.layoutopt_one.setBackgroundResource(R.drawable.right_ans);

                    PrepareExam_RTO_Activity.this.ans = 0;
                } else {
                    PrepareExam_RTO_Activity.this.layoutopt_one.setBackgroundResource(R.drawable.ans_bg);

                }
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase(str)) {
                    PrepareExam_RTO_Activity.this.layoutopt_two.setBackgroundResource(R.drawable.right_ans);

                    PrepareExam_RTO_Activity.this.ans = 1;
                } else {
                    PrepareExam_RTO_Activity.this.layoutopt_two.setBackgroundResource(R.drawable.ans_bg);

                }
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase("2")) {
                    PrepareExam_RTO_Activity prepareExamActivity = PrepareExam_RTO_Activity.this;
                    prepareExamActivity.ans = 2;
                    prepareExamActivity.layoutopt_three.setBackgroundResource(R.drawable.right_ans);
                    return;
                }
                PrepareExam_RTO_Activity.this.layoutopt_three.setBackgroundResource(R.drawable.ans_bg);
            }
        }
    }


    public void onResume() {
        super.onResume();

    }


    @SuppressLint("WrongConstant")
    public void onPause() {
        super.onPause();

    }

    public String loadJSONFromAsset(String str) {
        try {
            AssetManager assets = getAssets();
            String sb = str +
                    ".json";
            InputStream open = assets.open(sb);
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            return new String(bArr, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void ShowDialog() {
        // TODO Auto-generated method stub
        dialog = new Dialog(PrepareExam_RTO_Activity.this,
                android.R.style.Theme_Material_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_progress);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
        dialog.show();
    }
}
