package com.rtovehicle.information.carbikeinfo.activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.ferfalk.simplesearchview.SimpleSearchView;
import com.mikelau.views.shimmer.ShimmerRecyclerViewX;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.CompanyInfo_Adapter;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rtovehicle.information.carbikeinfo.common.GridMarginDecoration;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_Company;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BikeInformation_Activity extends AppCompatActivity {
    ArrayList<Model_Company.Company_Data> companyList_array;
    ShimmerRecyclerViewX recycle_company_list;
    SimpleSearchView searchView_simple;
    CompanyInfo_Adapter companyInfo_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bikeinformation);
        getWindow().setBackgroundDrawable(null);
        init();
        getCompanyListBike();
    }


    private void init() {
        companyList_array = new ArrayList<>();
        recycle_company_list = findViewById(R.id.rv_company_list_bike);
        searchView_simple = findViewById(R.id.searchview);
        recycle_company_list.setHasFixedSize(true);
        recycle_company_list.setLayoutManager(new GridLayoutManager(this, 3));
        float albumGridSpacing = getResources().getDimension(R.dimen.timeline_decorator_spacing);
        recycle_company_list.addItemDecoration(new GridMarginDecoration((int) albumGridSpacing));
        recycle_company_list.setGridChildCount(3);
        recycle_company_list.setDemoChildCount(25);
        findViewById(R.id.back_all).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.searchbtn).setOnClickListener(v -> searchView_simple.showSearch());
    }

    private void getCompanyListBike() {
        recycle_company_list.showShimmerAdapter();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Common_Utils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_Company> call = service.getCompanyistBike(MyApplication.MYSECRET);
        call.enqueue(new Callback<Model_Company>() {
            @Override
            public void onResponse(@NonNull Call<Model_Company> call, @NonNull Response<Model_Company> response) {
                recycle_company_list.hideShimmerAdapter();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            try {
                                companyList_array = response.body().getData();
                                companyInfo_adapter = new CompanyInfo_Adapter(BikeInformation_Activity.this, companyList_array, "bike");
                                recycle_company_list.setAdapter(companyInfo_adapter);

                                searchView_simple.setOnQueryTextListener(new SimpleSearchView.OnQueryTextListener() {
                                    @Override
                                    public boolean onQueryTextSubmit(String query) {
                                        ArrayList<Model_Company.Company_Data> sort_list = new ArrayList<>();
                                        for (int i = 0; i < companyList_array.size(); i++) {
                                            if (companyList_array.get(i).getCompany_name().toLowerCase().contains(query.toLowerCase())) {
                                                sort_list.add(companyList_array.get(i));
                                            }
                                        }
                                        companyInfo_adapter = new CompanyInfo_Adapter(BikeInformation_Activity.this, sort_list, "car");
                                        recycle_company_list.setAdapter(companyInfo_adapter);
                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextChange(String newText) {
                                        ArrayList<Model_Company.Company_Data> sort_list = new ArrayList<>();
                                        for (int i = 0; i < companyList_array.size(); i++) {
                                            if (companyList_array.get(i).getCompany_name().toLowerCase().contains(newText.toLowerCase())) {
                                                sort_list.add(companyList_array.get(i));
                                            }
                                        }
                                        companyInfo_adapter = new CompanyInfo_Adapter(BikeInformation_Activity.this, sort_list, "car");
                                        recycle_company_list.setAdapter(companyInfo_adapter);
                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextCleared() {
                                        companyInfo_adapter = new CompanyInfo_Adapter(BikeInformation_Activity.this, companyList_array, "car");
                                        recycle_company_list.setAdapter(companyInfo_adapter);
                                        return false;
                                    }
                                });
                            } catch (Exception ignored) {
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_Company> call, @NonNull Throwable t) {
                recycle_company_list.hideShimmerAdapter();
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
    }

}