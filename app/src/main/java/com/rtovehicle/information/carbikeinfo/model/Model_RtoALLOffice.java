package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_RtoALLOffice {
    @SerializedName("status")
    boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    public ArrayList<DATA_ALL> rto_All;


    public static class DATA_ALL {
        @SerializedName("id")
        private String id;

        @SerializedName("code")
        private String code;

        @SerializedName("name")
        private String name;

        @SerializedName("rto_details")
        ArrayList<Rto_details> Rto_all_city;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<Rto_details> getRto_all_city() {
            return Rto_all_city;
        }

        public void setRto_all_city(ArrayList<Rto_details> rto_all_city) {
            Rto_all_city = rto_all_city;
        }

        public static class Rto_details {
            @SerializedName("id")
            private String id;

            @SerializedName("state")
            private String state;

            @SerializedName("city_name")
            private String city_name;


            @SerializedName("rto_address")
            private String rto_address;

            @SerializedName("rto_phone")
            private String rto_phone;

            @SerializedName("rto_url")
            private String rto_url;

            @SerializedName("rto_code")
            private String rto_code;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity_name() {
                return city_name;
            }

            public void setCity_name(String city_name) {
                this.city_name = city_name;
            }

            public String getRto_address() {
                return rto_address;
            }

            public void setRto_address(String rto_address) {
                this.rto_address = rto_address;
            }

            public String getRto_phone() {
                return rto_phone;
            }

            public void setRto_phone(String rto_phone) {
                this.rto_phone = rto_phone;
            }

            public String getRto_url() {
                return rto_url;
            }

            public void setRto_url(String rto_url) {
                this.rto_url = rto_url;
            }

            public String getRto_code() {
                return rto_code;
            }

            public void setRto_code(String rto_code) {
                this.rto_code = rto_code;
            }
        }

    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DATA_ALL> getRto_All() {
        return rto_All;
    }

    public void setRto_All(ArrayList<DATA_ALL> rto_All) {
        this.rto_All = rto_All;
    }
}
