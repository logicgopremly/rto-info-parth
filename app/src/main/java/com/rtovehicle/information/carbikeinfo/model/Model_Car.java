package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_Car {
    @SerializedName("status")
    boolean status;

    @SerializedName("data")
    ArrayList<Car_Data> data;

    @SerializedName("url")
    String url;

    public static class Car_Data {
        @SerializedName("id")
        int id;

        @SerializedName("model_name")
        String model_name;

        @SerializedName("ex_showroom_price")
        String ex_showroom_price;

        @SerializedName("image")
        String image;

        @SerializedName("model_color")
        String[] model_color;

        @SerializedName("model_images")
        String[] model_images;

        @SerializedName("model_variants")
        String model_variants;

        public int getId() {
            return id;
        }

        public String getModel_name() {
            return model_name;
        }

        public String getEx_showroom_price() {
            return ex_showroom_price;
        }

        public String getImage() {
            return image;
        }

        public String[] getModel_color() {
            return model_color;
        }

        public String[] getModel_images() {
            return model_images;
        }

        public String getModel_variants() {
            return model_variants;
        }
    }

    public boolean isStatus() {
        return status;
    }

    public ArrayList<Car_Data> getData() {
        return data;
    }

    public String getUrl() {
        return url;
    }
}
