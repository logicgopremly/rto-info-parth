package com.rtovehicle.information.carbikeinfo.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.Fuel_activity;
import com.rtovehicle.information.carbikeinfo.adapter.State_Adapter;
import com.rtovehicle.information.carbikeinfo.model.Model_City;
import com.rtovehicle.information.carbikeinfo.common.PrefManager_fuel;

import java.util.ArrayList;
import java.util.List;

public class State_fragment extends Fragment implements State_Adapter.OnClickedState {
    public static List<Model_City> g = null;
    public Fuel_activity activity;
    public State_Adapter adapterState;
    private EditText edit_search;
    private RecyclerView list_view_state;
    public ProgressDialog progressDialog;

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.activity = (Fuel_activity) getActivity();
        return layoutInflater.inflate(R.layout.fragment_state, viewGroup, false);
    }

    public void onViewCreated(@NonNull View view2, Bundle bundle) {
        super.onViewCreated(view2, bundle);
        requireActivity().getWindow().setSoftInputMode(3);
        this.list_view_state = view2.findViewById(R.id.list_view_state);
        this.edit_search = view2.findViewById(R.id.edit_search);
        ImageView rip_back = view2.findViewById(R.id.img_view_back);
        ProgressDialog progressDialog2 = new ProgressDialog(getActivity());
        this.progressDialog = progressDialog2;
        progressDialog2.setTitle("Please wait");
        this.progressDialog.setCancelable(false);
        showList(PrefManager_fuel.getSateLsit(this.activity, "State"));
        searchFromEditText();
        rip_back.setOnClickListener(view -> State_fragment.this.activity.finish());
    }

    private void searchFromEditText() {
        this.edit_search.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                State_fragment.this.adapterState.filter(charSequence);
            }
        });
    }


    public void showList(List<Model_City> list) {
        ArrayList arrayList = new ArrayList<>();
        g = arrayList;
        arrayList.addAll(list);
        ArrayList arrayList2 = new ArrayList<>();
        String str = "";
        for (int i = 0; i < list.size(); i++) {
            Model_City modelCity = list.get(i);
            if (!str.equals(modelCity.getStateName())) {
                str = modelCity.getStateName();
                arrayList2.add(modelCity);
            }
        }
        State_Adapter adapterState2 = new State_Adapter(getActivity(), arrayList2, this.activity);
        this.adapterState = adapterState2;
        this.list_view_state.setAdapter(adapterState2);
        this.list_view_state.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.adapterState.calBack(this);
    }

    public void callBack(List<Model_City> list, String str) {
        ArrayList arrayList = new ArrayList<>();
        arrayList.addAll(list);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(City_fragment.title, arrayList);
        City_fragment fragmentCity = new City_fragment();
        fragmentCity.setArguments(bundle);
        this.activity.setFragment(fragmentCity, City_fragment.title);
    }
}
