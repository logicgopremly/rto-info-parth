package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.SerializedName;

public class Model_Vehicle {
    @SerializedName("status")
    boolean status;

    @SerializedName("date")
    Bike_Vehicle bike_vehicle;

    public Bike_Vehicle getBike_vehicle() {
        return bike_vehicle;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


    public static class Bike_Vehicle {
        @SerializedName("status")
        int status;

        @SerializedName("datafrom")
        String datafrom;

        @SerializedName("rc_owner_name")
        String rc_owner_name;

        @SerializedName("rc_regn_no")
        String rc_regn_no;

        @SerializedName("rc_maker_desc")
        String rc_maker_desc;

        @SerializedName("rc_maker_model")
        String rc_maker_model;

        @SerializedName("rc_regn_dt")
        String rc_regn_dt;
        @SerializedName("rc_fuel_desc")
        String rc_fuel_desc;

        @SerializedName("rc_vh_class_desc")
        String rc_vh_class_desc;
        @SerializedName("rc_registered_at")
        String rc_registered_at;
        @SerializedName("rc_eng_no")
        String rc_eng_no;
        @SerializedName("rc_chasi_no")
        String rc_chasi_no;

        @SerializedName("rc_insurance_upto")
        String rc_insurance_upto;
        @SerializedName("rc_fit_upto")
        String rc_fit_upto;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getDatafrom() {
            return datafrom;
        }

        public String getRc_owner_name() {
            return rc_owner_name;
        }

        public String getRc_regn_no() {
            return rc_regn_no;
        }

        public String getRc_maker_desc() {
            return rc_maker_desc;
        }

        public String getRc_maker_model() {
            return rc_maker_model;
        }

        public String getRc_regn_dt() {
            return rc_regn_dt;
        }

        public String getRc_fuel_desc() {
            return rc_fuel_desc;
        }

        public String getRc_vh_class_desc() {
            return rc_vh_class_desc;
        }

        public String getRc_registered_at() {
            return rc_registered_at;
        }

        public String getRc_eng_no() {
            return rc_eng_no;
        }

        public String getRc_chasi_no() {
            return rc_chasi_no;
        }

        public String getRc_insurance_upto() {
            return rc_insurance_upto;
        }

        public String getRc_fit_upto() {
            return rc_fit_upto;
        }
    }

    public boolean isStatus() {
        return status;
    }
}
