package com.rtovehicle.information.carbikeinfo.activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

public class ExamOption_Activity extends AppCompatActivity {

    TextView header;
    String keyname;
    String[] lang_array;
    String[] list_item_array;
    FrameLayout adContainerView;
    AdView adView;


    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_exam_option);
        getWindow().setBackgroundDrawable(null);
        click();
        try {
            loadBanner();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void click() {
        adContainerView = findViewById(R.id.framelayout);
        this.header = findViewById(R.id.id_header);
        this.lang_array = getResources().getStringArray(R.array.Lang);
        String str = "lang";
        if (getIntent().getStringExtra(str).equalsIgnoreCase("english")) {
            this.header.setText(this.lang_array[0]);
            this.list_item_array = getResources().getStringArray(R.array.qustion_page_english);
        } else if (getIntent().getStringExtra(str).equalsIgnoreCase("gujarati")) {
            this.header.setText(this.lang_array[1]);
            this.list_item_array = getResources().getStringArray(R.array.qustion_page_gujarati);
        } else if (getIntent().getStringExtra(str).equalsIgnoreCase("hindi")) {
            this.header.setText(this.lang_array[2]);
            this.list_item_array = getResources().getStringArray(R.array.qustion_page_hindi);
        } else {
            this.header.setText(this.lang_array[3]);
            this.list_item_array = getResources().getStringArray(R.array.qustion_page_marathi);
        }
        findViewById(R.id.back_all).setOnClickListener(view -> ExamOption_Activity.this.onBackPressed());

        findViewById(R.id.ll_queastionbank).setOnClickListener(view -> {
            ExamOption_Activity selectRtoExamOptionActivity = ExamOption_Activity.this;
            selectRtoExamOptionActivity.keyname = "ll_queastionbank";
            selectRtoExamOptionActivity.openExam_menu();
        });
        findViewById(R.id.ll_prepare_exam).setOnClickListener(view -> {
            ExamOption_Activity selectRtoExamOptionActivity = ExamOption_Activity.this;
            selectRtoExamOptionActivity.keyname = "ll_prepare_exam";
            selectRtoExamOptionActivity.openExame_prepare();
        });
        findViewById(R.id.ll_start_exam).setOnClickListener(view -> {
            ExamOption_Activity selectRtoExamOptionActivity = ExamOption_Activity.this;
            selectRtoExamOptionActivity.keyname = "ll_start_exam";
            selectRtoExamOptionActivity.openStartExame();
        });
        findViewById(R.id.ll_result).setOnClickListener(view -> {
            ExamOption_Activity selectRtoExamOptionActivity = ExamOption_Activity.this;
            selectRtoExamOptionActivity.keyname = "ll_result";
            selectRtoExamOptionActivity.openSymbolActivity();
        });

    }


    public void openExam_menu() {
        Intent intent = new Intent(this, QuestionRTOBank_Activity.class);
        String str = "lang";
        intent.putExtra(str, getIntent().getStringExtra(str));
        intent.putExtra("menu", 0);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }


    public void openExame_prepare() {
        Intent intent = new Intent(this, PrepareExam_RTO_Activity.class);
        String str = "lang";
        intent.putExtra(str, getIntent().getStringExtra(str));
        intent.putExtra("header", "0");
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }


    public void openStartExame() {
        Intent intent = new Intent(this, ExamStart_Activity.class);
        String str = "lang";
        intent.putExtra(str, getIntent().getStringExtra(str));
        intent.putExtra("header", "0");
        intent.putExtra("menu", 2);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }


    public void openSymbolActivity() {
        Intent intent = new Intent(this, HistoryOfExam_Activity.class);
        String str = "lang";
        intent.putExtra(str, getIntent().getStringExtra(str));
        intent.putExtra("header", "0");
        intent.putExtra("menu", 3);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    private void loadBanner() {
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);
        AdRequest adRequest =
                new AdRequest.Builder()
                        .build();
        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }


}
