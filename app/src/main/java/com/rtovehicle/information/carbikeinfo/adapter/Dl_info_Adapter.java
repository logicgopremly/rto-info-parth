package com.rtovehicle.information.carbikeinfo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;


public class Dl_info_Adapter extends RecyclerView.Adapter<Dl_info_Adapter.ViewHolder> {
    final Context context;
    final LayoutInflater inflater;
    final String[] number;
    final int[] image;
    final OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int i);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener {
        final TextView info_Name;
        final ImageView info_icon;

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        }

        public ViewHolder(@NonNull final View view) {
            super(view);
            this.info_icon = view.findViewById(R.id.celeb_icon);
            this.info_Name = view.findViewById(R.id.celebName);
            view.setOnClickListener(view1 -> Dl_info_Adapter.this.onItemClickListener.onItemClick(view1, ViewHolder.this.getAdapterPosition()));
        }
    }

    public Dl_info_Adapter(Context context2, String[] strArr2, int[] intArr, OnItemClickListener onItemClickListener2) {
        this.context = context2;
        this.number = strArr2;
        this.image = intArr;
        this.onItemClickListener = onItemClickListener2;
        this.inflater = LayoutInflater.from(context2);
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(this.inflater.inflate(R.layout.dl_item_layout, viewGroup, false));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        try {
            viewHolder.info_Name.setText(this.number[i]);
            Resources resources = context.getResources();
            viewHolder.info_icon.setImageDrawable(resources.getDrawable(image[i]));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public int getItemCount() {
        return this.number.length;
    }
}
