package com.rtovehicle.information.carbikeinfo.common;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "rto_vehicle.db";


    public static final String COL1 = "datafrom";
    public static final String COL2 = "rc_owner_name";
    public static final String COL3 = "rc_regn_no";
    public static final String COL4 = "rc_maker_desc";
    public static final String COL5 = "rc_maker_model";
    public static final String COL6 = "rc_regn_dt";
    public static final String COL7 = "rc_fuel_desc";
    public static final String COL8 = "rc_vh_class_desc";
    public static final String COL9 = "rc_registered_at";
    public static final String COL10 = "rc_eng_no";
    public static final String COL11 = "rc_chasi_no";
    public static final String COL12 = "rc_insurance_upto";
    public static final String COL13 = "rc_fit_upto";
    public static final String COL14 = "rc_norms_desc";


    public DataBaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, 1);

    }


    final String vehicle = "create table vehicle(ID INTEGER PRIMARY KEY AUTOINCREMENT " +
            ",datafrom TEXT" +
            ",rc_owner_name TEXT" +
            ",rc_regn_no TEXT" +
            ",rc_maker_desc TEXT" +
            ",rc_maker_model TEXT" +
            ",rc_regn_dt TEXT" +
            ",rc_fuel_desc TEXT" +
            ",rc_vh_class_desc TEXT" +
            ",rc_registered_at TEXT" +
            ",rc_eng_no TEXT" +
            ",rc_chasi_no TEXT" +
            ",rc_insurance_upto TEXT" +
            ",rc_fit_upto TEXT" +
            ",rc_norms_desc TEXT)";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(vehicle);
    }


    public void Insert_vehicle(String datafrom, String rc_owner_name, String rc_regn_no, String rc_maker_desc, String rc_maker_model, String rc_regn_dt, String rc_fuel_desc, String rc_vh_class_desc, String rc_registered_at, String rc_eng_no, String rc_chasi_no, String rc_insurance_upto, String rc_fit_upto, String rc_norms_desc) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL1, datafrom);
        contentValues.put(COL2, rc_owner_name);
        contentValues.put(COL3, rc_regn_no);
        contentValues.put(COL4, rc_maker_desc);
        contentValues.put(COL5, rc_maker_model);
        contentValues.put(COL6, rc_regn_dt);
        contentValues.put(COL7, rc_fuel_desc);
        contentValues.put(COL8, rc_vh_class_desc);
        contentValues.put(COL9, rc_registered_at);
        contentValues.put(COL10, rc_eng_no);
        contentValues.put(COL11, rc_chasi_no);
        contentValues.put(COL12, rc_insurance_upto);
        contentValues.put(COL13, rc_fit_upto);
        contentValues.put(COL14, rc_norms_desc);


        @SuppressLint("Recycle") Cursor c = db.rawQuery("SELECT * FROM vehicle" + " WHERE rc_regn_no" + " = '" + rc_regn_no + "'", null);
        try {
            if (!c.moveToFirst()) {
                db.insert("vehicle", null, contentValues);
            }
            c.close();
            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS vehicle");
    }


    public Cursor getvehicle() {
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery("select * from vehicle", null);
    }


    public void Deletevehicle(int character_Id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("vehicle", "id=?", new String[]{String.valueOf(character_Id)});
        db.close();
    }

}
