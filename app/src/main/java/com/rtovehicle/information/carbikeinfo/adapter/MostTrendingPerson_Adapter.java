package com.rtovehicle.information.carbikeinfo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.MostFamousPerson_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_MostTrending;

import java.util.ArrayList;

public class MostTrendingPerson_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final ArrayList<Model_MostTrending.DATA_ALL_Most> most_trending_array;
    public static final ArrayList<Model_MostTrending.DATA_ALL_Most.vehicle_details_most> v_array = new ArrayList<>();
    final Context moContext;


    public MostTrendingPerson_Adapter(Context context2, ArrayList<Model_MostTrending.DATA_ALL_Most> arrayList) {
        this.moContext = context2;
        this.most_trending_array = arrayList;
    }


    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new RTO_Most_trending_person(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.most_trending_item, viewGroup, false));
    }

    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {

        RTO_Most_trending_person rTODetail = (RTO_Most_trending_person) viewHolder;
        Glide.with(moContext).load(most_trending_array.get(i).getImage()).placeholder(R.drawable.placeholder).into(rTODetail.img_thumb);
        rTODetail.name_trending_text.setText(most_trending_array.get(i).getCategory_name());

        rTODetail.click_lin.setOnClickListener(view -> {
            v_array.addAll(most_trending_array.get(i).getVehicle_details_most());
            Intent intent = new Intent(moContext, MostFamousPerson_Activity.class);
            intent.putExtra("txt_name",most_trending_array.get(i).getCategory_name());
            moContext.startActivity(intent);
        });


    }

    public int getItemCount() {
        return this.most_trending_array.size();
    }

    static class RTO_Most_trending_person extends RecyclerView.ViewHolder {

        final ImageView img_thumb;
        final TextView name_trending_text;
        final LinearLayout click_lin;


        public RTO_Most_trending_person(View view) {
            super(view);

            img_thumb = view.findViewById(R.id.thumb_image);
            name_trending_text = view.findViewById(R.id.text_name);
            click_lin = view.findViewById(R.id.click_lin);
        }
    }


}
