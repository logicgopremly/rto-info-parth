package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_UpcomingVehicle {
    @SerializedName("status")
    boolean status;

    @SerializedName("date")
    ArrayList<Upcoming_Data> Upcoming_Data_array;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ArrayList<Upcoming_Data> getUpcoming_Data_array() {
        return Upcoming_Data_array;
    }

    public void setUpcoming_Data_array(ArrayList<Upcoming_Data> upcoming_Data_array) {
        Upcoming_Data_array = upcoming_Data_array;
    }

    public static class Upcoming_Data {
        @SerializedName("id")
        int id;

        @SerializedName("name")
        String name;

        @SerializedName("maker")
        String maker;

        @SerializedName("capicity")
        String capicity;

        @SerializedName("mileage")
        String mileage;

        @SerializedName("expected_price")
        String expected_price;

        @SerializedName("image")
        String image;
        @SerializedName("description1")
        String description1;

        @SerializedName("power")
        String power;

        @SerializedName("expected_launch")
        String expected_launch;

        @SerializedName("top_speed")
        String top_speed;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMaker() {
            return maker;
        }

        public void setMaker(String maker) {
            this.maker = maker;
        }

        public String getCapicity() {
            return capicity;
        }

        public void setCapicity(String capicity) {
            this.capicity = capicity;
        }

        public String getMileage() {
            return mileage;
        }

        public void setMileage(String mileage) {
            this.mileage = mileage;
        }

        public String getExpected_price() {
            return expected_price;
        }

        public void setExpected_price(String expected_price) {
            this.expected_price = expected_price;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDescription1() {
            return description1;
        }

        public void setDescription1(String description1) {
            this.description1 = description1;
        }

        public String getPower() {
            return power;
        }

        public void setPower(String power) {
            this.power = power;
        }

        public String getExpected_launch() {
            return expected_launch;
        }

        public void setExpected_launch(String expected_launch) {
            this.expected_launch = expected_launch;
        }

        public String getTop_speed() {
            return top_speed;
        }

        public void setTop_speed(String top_speed) {
            this.top_speed = top_speed;
        }
    }
}
