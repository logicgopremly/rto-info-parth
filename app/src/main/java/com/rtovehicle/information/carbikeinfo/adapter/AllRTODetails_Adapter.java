package com.rtovehicle.information.carbikeinfo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.AllRTO_FullDetails_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_RtoALLOffice;

import java.util.ArrayList;

public class AllRTODetails_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Model_RtoALLOffice.DATA_ALL> data_allArrayList;
    final Context moContext;


    public AllRTODetails_Adapter(Context context2, ArrayList<Model_RtoALLOffice.DATA_ALL> arrayList) {
        this.moContext = context2;
        this.data_allArrayList = arrayList;
    }


    @NonNull
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new RTODetail_State_holder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rto_detail_header, viewGroup, false));
    }

    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @SuppressLint("RecyclerView") int i) {

        RTODetail_State_holder rTODetail_State_holder = (RTODetail_State_holder) viewHolder;
        rTODetail_State_holder.TxtStatename.setText(data_allArrayList.get(i).getName());
        rTODetail_State_holder.TxtStatecode.setText(data_allArrayList.get(i).getCode());

        rTODetail_State_holder.RrRtocityname.setLayoutManager(new LinearLayoutManager(moContext));
        rTODetail_State_holder.RrRtocityname.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                return new RTODetail_City_holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rto_detail_item, parent, false));

            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

                RTODetail_City_holder cityholder = (RTODetail_City_holder) holder;
                cityholder.TxtCityname.setText(data_allArrayList.get(i).getRto_all_city().get(position).getCity_name());
                cityholder.TxtCityname.setSelected(true);
                cityholder.TxtRtoCode.setText(data_allArrayList.get(i).getRto_all_city().get(position).getRto_code());

                cityholder.CntCity.setOnClickListener(view -> {
                    Intent intent = new Intent(moContext, AllRTO_FullDetails_Activity.class);
                    intent.putExtra("tv_title", data_allArrayList.get(i).getName());
                    intent.putExtra("tv_district", data_allArrayList.get(i).getRto_all_city().get(position).getCity_name());
                    intent.putExtra("tv_code", data_allArrayList.get(i).getRto_all_city().get(position).getRto_code());
                    intent.putExtra("tv_address", data_allArrayList.get(i).getRto_all_city().get(position).getRto_address());
                    intent.putExtra("tv_phone", data_allArrayList.get(i).getRto_all_city().get(position).getRto_phone());
                    intent.putExtra("tv_link", data_allArrayList.get(i).getRto_all_city().get(position).getRto_url());
                    moContext.startActivity(intent);
                });

            }

            @Override
            public int getItemCount() {
                return data_allArrayList.get(i).getRto_all_city().size();
            }

        });

    }

    public int getItemCount() {
        return this.data_allArrayList.size();
    }

    static class RTODetail_State_holder extends RecyclerView.ViewHolder {
        private final RecyclerView RrRtocityname;
        public final TextView TxtStatecode;
        public final TextView TxtStatename;

        public RTODetail_State_holder(View view) {
            super(view);
            this.TxtStatename = view.findViewById(R.id.txt_statename);
            this.TxtStatecode = view.findViewById(R.id.txt_statecode);
            this.RrRtocityname = view.findViewById(R.id.rr_rtocityname);
        }
    }


    static class RTODetail_City_holder extends RecyclerView.ViewHolder {
        private final RelativeLayout CntCity;
        public final TextView TxtCityname;
        public final TextView TxtRtoCode;

        public RTODetail_City_holder(View view) {
            super(view);
            this.TxtCityname = view.findViewById(R.id.txt_cityname);
            this.TxtRtoCode = view.findViewById(R.id.txt_rtoCode);
            this.CntCity = view.findViewById(R.id.cnt_city);
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    public void filterList(ArrayList<Model_RtoALLOffice.DATA_ALL> filterdNames) {
        this.data_allArrayList = filterdNames;
        notifyDataSetChanged();
    }
}
