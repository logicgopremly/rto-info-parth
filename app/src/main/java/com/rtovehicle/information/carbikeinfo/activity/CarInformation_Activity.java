package com.rtovehicle.information.carbikeinfo.activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.ferfalk.simplesearchview.SimpleSearchView;
import com.mikelau.views.shimmer.ShimmerRecyclerViewX;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.CompanyInfo_Adapter;
import com.rtovehicle.information.carbikeinfo.common.GridMarginDecoration;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_Company;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CarInformation_Activity extends AppCompatActivity {

    ArrayList<Model_Company.Company_Data> company_dataArrayList;
    ShimmerRecyclerViewX recyclerViewX;
    SimpleSearchView searchView_simple;
    CompanyInfo_Adapter companyInfo_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carinformation);
        getWindow().setBackgroundDrawable(null);
        init();
        getCompanyListCar();
    }

    private void init() {
        company_dataArrayList = new ArrayList<>();
        searchView_simple = findViewById(R.id.searchview);
        recyclerViewX = findViewById(R.id.rv_company_list);
        recyclerViewX.setHasFixedSize(true);
        recyclerViewX.setLayoutManager(new GridLayoutManager(this, 3));
        float albumGridSpacing = getResources().getDimension(R.dimen.timeline_decorator_spacing);
        recyclerViewX.addItemDecoration(new GridMarginDecoration((int) albumGridSpacing));
        recyclerViewX.setGridChildCount(3);
        recyclerViewX.setDemoChildCount(25);


        findViewById(R.id.back_all).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.searchbtn).setOnClickListener(v -> searchView_simple.showSearch());
    }

    private void getCompanyListCar() {
        recyclerViewX.showShimmerAdapter();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_Company> call = service.getCompanyistCar(MyApplication.MYSECRET);
        call.enqueue(new Callback<Model_Company>() {
            @Override
            public void onResponse(@NonNull Call<Model_Company> call, @NonNull Response<Model_Company> response) {
                recyclerViewX.hideShimmerAdapter();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            try {
                                company_dataArrayList = response.body().getData();
                                companyInfo_adapter = new CompanyInfo_Adapter(CarInformation_Activity.this, company_dataArrayList, "car");
                                recyclerViewX.setAdapter(companyInfo_adapter);
                                searchView_simple.setOnQueryTextListener(new SimpleSearchView.OnQueryTextListener() {
                                    @Override
                                    public boolean onQueryTextSubmit(String query) {
                                        ArrayList<Model_Company.Company_Data> sortlist = new ArrayList<>();
                                        for (int i = 0; i < company_dataArrayList.size(); i++) {

                                            if (company_dataArrayList.get(i).getCompany_name().toLowerCase().contains(query.toLowerCase())) {
                                                sortlist.add(company_dataArrayList.get(i));
                                            }
                                        }
                                        companyInfo_adapter = new CompanyInfo_Adapter(CarInformation_Activity.this, sortlist, "car");
                                        recyclerViewX.setAdapter(companyInfo_adapter);
                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextChange(String newText) {
                                        ArrayList<Model_Company.Company_Data> sortlist = new ArrayList<>();
                                        for (int i = 0; i < company_dataArrayList.size(); i++) {

                                            if (company_dataArrayList.get(i).getCompany_name().toLowerCase().contains(newText.toLowerCase())) {
                                                sortlist.add(company_dataArrayList.get(i));
                                            }
                                        }
                                        companyInfo_adapter = new CompanyInfo_Adapter(CarInformation_Activity.this, sortlist, "car");
                                        recyclerViewX.setAdapter(companyInfo_adapter);
                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextCleared() {
                                        companyInfo_adapter = new CompanyInfo_Adapter(CarInformation_Activity.this, company_dataArrayList, "car");
                                        recyclerViewX.setAdapter(companyInfo_adapter);
                                        return false;
                                    }
                                });

                            } catch (Exception ignored) {
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_Company> call, @NonNull Throwable t) {
                recyclerViewX.hideShimmerAdapter();
            }
        });
    }

}