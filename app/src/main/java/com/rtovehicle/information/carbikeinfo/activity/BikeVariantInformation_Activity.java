package com.rtovehicle.information.carbikeinfo.activity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.CarVariantsinfo_Adapter;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_BikeVariantDetails;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class BikeVariantInformation_Activity extends AppCompatActivity {

    Toolbar header;
    Dialog dialog;
    String name;
    int variants_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bikevariantinformation);
        getWindow().setBackgroundDrawable(null);
        init();
        getVariantDetails(variants_id);
    }

    private void init() {
        header = findViewById(R.id.toolbar);
        name = getIntent().getStringExtra(CarVariantsinfo_Adapter.VARIANT_NAME);
        variants_id = getIntent().getIntExtra(CarVariantsinfo_Adapter.VARIANT_ID, -1);

        header.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.back_rto_new_1));
        ((TextView) header.findViewById(R.id.app_title)).setText(name);
        setSupportActionBar(header);
        header.setNavigationOnClickListener(v -> onBackPressed());
    }


    private void getVariantDetails(int variants_id) {
        ShowDialog();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Common_Utils.BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_BikeVariantDetails> call = service.getVariantDetailsBike(MyApplication.MYSECRET, variants_id);
        call.enqueue(new Callback<Model_BikeVariantDetails>() {
            @Override
            public void onResponse(@NonNull Call<Model_BikeVariantDetails> call, @NonNull Response<Model_BikeVariantDetails> response) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            Model_BikeVariantDetails.Variant_Data.Engine engine = response.body().getData().get(0).getEngine();
                            setEngineDetails(engine);
                            Model_BikeVariantDetails.Variant_Data.Milege millage = response.body().getData().get(0).getMilege();
                            setMileagesDetails(millage);
                            Model_BikeVariantDetails.Variant_Data.Features features = response.body().getData().get(0).getFeatures();
                            setFeaturesDetails(features);
                            Model_BikeVariantDetails.Variant_Data.Chassis chassis = response.body().getData().get(0).getChassis();
                            setChassisDetails(chassis);
                            Model_BikeVariantDetails.Variant_Data.Dimension dimension = response.body().getData().get(0).getDimension();
                            setDimensionDetails(dimension);
                            Model_BikeVariantDetails.Variant_Data.Electicals electrics = response.body().getData().get(0).getElecticals();
                            setElectricsDetails(electrics);
                            Model_BikeVariantDetails.Variant_Data.Type type = response.body().getData().get(0).getType();
                            setTypeDetails(type);
                            Model_BikeVariantDetails.Variant_Data.Others others = response.body().getData().get(0).getOthers();
                            setOthersDetails(others);
                        }
                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<Model_BikeVariantDetails> call, @NonNull Throwable t) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void setEngineDetails(Model_BikeVariantDetails.Variant_Data.Engine engine) {
        ((TextView) findViewById(R.id.txtEngineType)).setText(engine.getEngineType());
        ((TextView) findViewById(R.id.txtDisplacement)).setText(engine.getDisplacement());
        ((TextView) findViewById(R.id.txtMaxPower)).setText(engine.getMaxPower());
        ((TextView) findViewById(R.id.txtMaxTorque)).setText(engine.getMaxTorque());
        ((TextView) findViewById(R.id.txtNoOfCylinder)).setText(engine.getNoOfCylinder());
        ((TextView) findViewById(R.id.txtCoolingSystem)).setText(engine.getCoolingSystem());
        ((TextView) findViewById(R.id.txtValvesPerCylinder)).setText(engine.getValvesPerCylinder());
        ((TextView) findViewById(R.id.txtDriveType)).setText(engine.getDriveType());
        ((TextView) findViewById(R.id.txtStarting)).setText(engine.getStarting());
        ((TextView) findViewById(R.id.txtFuelSupply)).setText(engine.getFuelSupply());
        ((TextView) findViewById(R.id.txtClutch)).setText(engine.getClutch());
        ((TextView) findViewById(R.id.txtTransmission)).setText(engine.getTransmission());
        ((TextView) findViewById(R.id.txtGearBox)).setText(engine.getGearBox());
        ((TextView) findViewById(R.id.txtBore)).setText(engine.getBore());
        ((TextView) findViewById(R.id.txtStroke)).setText(engine.getStroke());
        ((TextView) findViewById(R.id.txtCompressionRatio)).setText(engine.getCompressionRatio());
        ((TextView) findViewById(R.id.txtEmissionType)).setText(engine.getEmissionType());
        ((TextView) findViewById(R.id.txtIgnition)).setText(engine.getIgnition());
    }

    private void setMileagesDetails(Model_BikeVariantDetails.Variant_Data.Milege millage) {
        ((TextView) findViewById(R.id.txtCityMileage)).setText(millage.getCityMileage());
        ((TextView) findViewById(R.id.txtHighwayMileage)).setText(millage.getHighwayMileage());
        ((TextView) findViewById(R.id.txtMaxSpeed)).setText(millage.getMaxSpeed());
        ((TextView) findViewById(R.id.txtAccelerationZtoSixty)).setText(millage.getAccelerationZtoSixty());
        ((TextView) findViewById(R.id.txtAccelerationZtoEighty)).setText(millage.getAccelerationZtoEighty());
        ((TextView) findViewById(R.id.txtQuarterMile)).setText(millage.getQuarterMile());
        ((TextView) findViewById(R.id.txtBraking)).setText(millage.getBraking());
    }

    private void setFeaturesDetails(Model_BikeVariantDetails.Variant_Data.Features features) {
        ((TextView) findViewById(R.id.txtABS)).setText(features.getABS());
        ((TextView) findViewById(R.id.txtBrakingType)).setText(features.getBrakingType());
        ((TextView) findViewById(R.id.txtSpeedometer)).setText(features.getSpeedometer());
        ((TextView) findViewById(R.id.txtTachometer)).setText(features.getTachometer());
        ((TextView) findViewById(R.id.txtOdometer)).setText(features.getOdometer());
        ((TextView) findViewById(R.id.txtTripmeter)).setText(features.getTripmeter());
        ((TextView) findViewById(R.id.txtFuelGuage)).setText(features.getFuelGuage());
        ((TextView) findViewById(R.id.txtConsole)).setText(features.getConsole());
        ((TextView) findViewById(R.id.txtPassSwitch)).setText(features.getPassSwitch());
        ((TextView) findViewById(R.id.txtPassengerFootrest)).setText(features.getPassengerFootrest());
        ((TextView) findViewById(R.id.txtClock)).setText(features.getClock());
        ((TextView) findViewById(R.id.txtAdditionalFeatures)).setText(features.getAdditionalFeatures());
        ((TextView) findViewById(R.id.txtDisplay)).setText(features.getDisplay());
        ((TextView) findViewById(R.id.txtServiceDueIndicator)).setText(features.getServiceDueIndicator());
        ((TextView) findViewById(R.id.txtExternalFuelFilling)).setText(features.getExternalFuelFilling());
        ((TextView) findViewById(R.id.txtCarryHook)).setText(features.getCarryHook());
        ((TextView) findViewById(R.id.txtRealTimeMileageIndicator)).setText(features.getRealTimeMileageIndicator());
        ((TextView) findViewById(R.id.txtChargingPoint)).setText(features.getChargingPoint());
        ((TextView) findViewById(R.id.txtStepupSeat)).setText(features.getStepupSeat());
        ((TextView) findViewById(R.id.txtI3sTechnology)).setText(features.getI3sTechnology());
    }

    private void setChassisDetails(Model_BikeVariantDetails.Variant_Data.Chassis chassis) {
        ((TextView) findViewById(R.id.txtChassis)).setText(chassis.getChassis());
        ((TextView) findViewById(R.id.txtBodyType)).setText(chassis.getBodyType());
        ((TextView) findViewById(R.id.txtFrontSuspension)).setText(chassis.getFrontSuspension());
        ((TextView) findViewById(R.id.txtRearSuspension)).setText(chassis.getRearSuspension());
        ((TextView) findViewById(R.id.txtBodyGraphics)).setText(chassis.getBodyGraphics());
    }

    private void setDimensionDetails(Model_BikeVariantDetails.Variant_Data.Dimension dimension) {

        ((TextView) findViewById(R.id.txtLength)).setText(dimension.getLength());
        ((TextView) findViewById(R.id.txtWidth)).setText(dimension.getWidth());
        ((TextView) findViewById(R.id.txtHeight)).setText(dimension.getHeight());
        ((TextView) findViewById(R.id.txtFuelCapacity)).setText(dimension.getFuelCapacity());
        ((TextView) findViewById(R.id.txtGroundClearance)).setText(dimension.getGroundClearance());
        ((TextView) findViewById(R.id.txtWheelBase)).setText(dimension.getWheelBase());
        ((TextView) findViewById(R.id.txtKerbWeight)).setText(dimension.getKerbWeight());
        ((TextView) findViewById(R.id.txtSaddleHeight)).setText(dimension.getSaddleHeight());
        ((TextView) findViewById(R.id.txtUnderseatStorage)).setText(dimension.getUnderseatStorage());
        ((TextView) findViewById(R.id.txtLoadCapacity)).setText(dimension.getLoadCapacity());

    }

    private void setElectricsDetails(Model_BikeVariantDetails.Variant_Data.Electicals electrical) {

        ((TextView) findViewById(R.id.txtHeadlight)).setText(electrical.getHeadlight());
        ((TextView) findViewById(R.id.txtTailLight)).setText(electrical.getTailLight());
        ((TextView) findViewById(R.id.txtTurnsignalLamp)).setText(electrical.getTurnsignalLamp());
        ((TextView) findViewById(R.id.txtDRLs)).setText(electrical.getDRLs());
        ((TextView) findViewById(R.id.txtLowBatteryIndicator)).setText(electrical.getLowBatteryIndicator());
        ((TextView) findViewById(R.id.txtLowFuelIndicator)).setText(electrical.getLowFuelIndicator());
        ((TextView) findViewById(R.id.txtBatteryCapacity)).setText(electrical.getBatteryCapacity());
        ((TextView) findViewById(R.id.txtNavigation)).setText(electrical.getNavigation());
        ((TextView) findViewById(R.id.txtLEDTailLights)).setText(electrical.getLEDTailLights());
        ((TextView) findViewById(R.id.txtBatteryType)).setText(electrical.getBatteryType());
        ((TextView) findViewById(R.id.txtMobileConnectivity)).setText(electrical.getMobileConnectivity());
        ((TextView) findViewById(R.id.txtBootLight)).setText(electrical.getBootbLight());
    }

    private void setTypeDetails(Model_BikeVariantDetails.Variant_Data.Type type) {

        ((TextView) findViewById(R.id.txtTyreSize)).setText(type.getTyreSize());
        ((TextView) findViewById(R.id.txtTyreType)).setText(type.getTyreType());
        ((TextView) findViewById(R.id.txtWheelSize)).setText(type.getWheelSize());
        ((TextView) findViewById(R.id.txtWheelsType)).setText(type.getWheelsType());
        ((TextView) findViewById(R.id.txtFrontBrake)).setText(type.getFrontBrake());
        ((TextView) findViewById(R.id.txtRearBrake)).setText(type.getRearBrake());
        ((TextView) findViewById(R.id.txtFrontBrakeDiameter)).setText(type.getFrontBrakeDiameter());
        ((TextView) findViewById(R.id.txtRearBrakeDiameter)).setText(type.getRearBrakeDiameter());
        ((TextView) findViewById(R.id.txtRadialTyre)).setText(type.getRadialTyre());
        ((TextView) findViewById(R.id.txtFrontTyrePressureRider)).setText(type.getFrontTyrePressureRider());
        ((TextView) findViewById(R.id.txtRearTyrePressureRider)).setText(type.getRearTyrePressureRider());
        ((TextView) findViewById(R.id.txtFrontTyrePressureRiderPillion)).setText(type.getFrontTyrePressureRiderPillion());
        ((TextView) findViewById(R.id.txtRearTyrePressureRiderPillion)).setText(type.getRearTyrePressureRiderPillion());

    }

    private void setOthersDetails(Model_BikeVariantDetails.Variant_Data.Others others) {
        ((TextView) findViewById(R.id.txtFuelReserve)).setText(others.getFuelReserve());
        ((TextView) findViewById(R.id.txtPilotLamps)).setText(others.getPilotLamps());
        ((TextView) findViewById(R.id.txtTractionControl)).setText(others.getTractionControl());
        ((TextView) findViewById(R.id.txtEngineKillSwitch)).setText(others.getEngineKillSwitch());
        ((TextView) findViewById(R.id.txtSeatOpeningSwitch)).setText(others.getSeatOpeningSwitch());
        ((TextView) findViewById(R.id.txtDistancetoEmptyIndicator)).setText(others.getDistancetoEmptyIndicator());
        ((TextView) findViewById(R.id.txtMotorType)).setText(others.getMotorType());
        ((TextView) findViewById(R.id.txtMotorPower)).setText(others.getMotorPower());
        ((TextView) findViewById(R.id.txtRange)).setText(others.getRange());
        ((TextView) findViewById(R.id.txtBatteryChargingTime)).setText(others.getBatteryChargingTime());
        ((TextView) findViewById(R.id.txtFastCharging)).setText(others.getFastCharging());
        ((TextView) findViewById(R.id.txtRidingModes)).setText(others.getRidingModes());
        ((TextView) findViewById(R.id.txtProjectorHeadlights)).setText(others.getProjectorHeadlights());
        ((TextView) findViewById(R.id.txtEngineImmobilizer)).setText(others.getEngineImmobilizer());
        ((TextView) findViewById(R.id.txtAntiTheftAlarm)).setText(others.getAntiTheftAlarm());
        ((TextView) findViewById(R.id.txtARAIMileage)).setText(others.getARAIMileage());
        ((TextView) findViewById(R.id.txtAccelerationZtoForty)).setText(others.getAccelerationZtoForty());
        ((TextView) findViewById(R.id.txtAverageFuelEconomyIndicator)).setText(others.getAverageFuelEconomyIndicator());

    }

    private void ShowDialog() {
        // TODO Auto-generated method stub
        dialog = new Dialog(BikeVariantInformation_Activity.this, android.R.style.Theme_Material_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_progress);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
        dialog.show();
    }


}