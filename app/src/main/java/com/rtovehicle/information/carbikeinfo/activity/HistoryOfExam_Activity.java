package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog.Builder;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.internal.view.SupportMenu;
import androidx.core.view.ViewCompat;

import com.airbnb.lottie.LottieAnimationView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.model.Model_ExamHistory;
import com.rtovehicle.information.carbikeinfo.common.DBTiny;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

import java.util.ArrayList;
import java.util.Collections;

public class HistoryOfExam_Activity extends AppCompatActivity {
  ImageView clear_all;
  TextView header;
  ArrayList<Model_ExamHistory> history_list_array = new ArrayList<>();
  ListView listView;
  String[] list_item_array;
  MyAdapter myAdapter;
  DBTiny tinyDB;
  LottieAnimationView txt_no_data;

  @SuppressLint("WrongConstant")
  public void onCreate(Bundle bundle) {
    super.onCreate(bundle);
    setContentView(R.layout.activity_history_exam);
    getWindow().setBackgroundDrawable(null);
    this.tinyDB = new DBTiny(this);
    findid();
    setdata();
  }

  public void findid() {
    this.header = findViewById(R.id.id_header);
    this.txt_no_data = findViewById(R.id.txt_nodata);
    this.listView = findViewById(R.id.listview);
    this.clear_all = findViewById(R.id.clrarall);
    findViewById(R.id.back_all).setOnClickListener(view -> HistoryOfExam_Activity.this.onBackPressed());
  }

  @SuppressLint("WrongConstant")
  public void setdata() {
    this.list_item_array = getResources().getStringArray(R.array.result);
    registerForContextMenu(this.listView);
    String str = "lang";
    if (getIntent().getStringExtra(str).equalsIgnoreCase("english")) {
      this.history_list_array = this.tinyDB.getAdListObject(Common_Utils.key_eng_history_list);
      this.header.setText(this.list_item_array[0]);
    } else if (getIntent().getStringExtra(str).equalsIgnoreCase("gujarati")) {
      this.history_list_array = this.tinyDB.getAdListObject(Common_Utils.key_guj_history_list);
      this.header.setText(this.list_item_array[1]);
    } else if (getIntent().getStringExtra(str).equalsIgnoreCase("hindi")) {
      this.history_list_array = this.tinyDB.getAdListObject(Common_Utils.key_hindi_history_list);
      this.header.setText(this.list_item_array[2]);
    } else {
      this.history_list_array = this.tinyDB.getAdListObject(Common_Utils.key_marathi_history_list);
      this.header.setText(this.list_item_array[3]);
    }
    if (this.history_list_array.size() != 0) {
      this.listView.setVisibility(0);
      this.txt_no_data.setVisibility(8);
      Collections.reverse(this.history_list_array);
      this.myAdapter = new MyAdapter(this, this.history_list_array);
      this.listView.setAdapter(this.myAdapter);
    } else {
      this.listView.setVisibility(8);
      this.txt_no_data.setVisibility(0);
    }
    if (this.history_list_array.isEmpty()) {
      this.clear_all.setVisibility(0);
      this.clear_all.setEnabled(false);
      this.clear_all.setClickable(false);
      this.clear_all.setAlpha(0.5f);
    } else {
      this.clear_all.setVisibility(0);
      this.clear_all.setEnabled(true);
      this.clear_all.setClickable(true);
      this.clear_all.setAlpha(1.0f);
    }
    this.clear_all.setOnClickListener(view -> {
      Builder builder = new Builder(HistoryOfExam_Activity.this);
      builder.setTitle("Delete?");
      builder.setMessage("Are you sure want to clear history");
      builder.setPositiveButton("Yes", (dialogInterface, i) -> {
        HistoryOfExam_Activity.this.history_list_array.clear();
        String str1 = "lang";
        if (HistoryOfExam_Activity.this.getIntent().getStringExtra(str1).equalsIgnoreCase("english")) {
          HistoryOfExam_Activity.this.tinyDB.putAdListObject(Common_Utils.key_eng_history_list, HistoryOfExam_Activity.this.history_list_array);
        } else if (HistoryOfExam_Activity.this.getIntent().getStringExtra(str1).equalsIgnoreCase("gujarati")) {
          HistoryOfExam_Activity.this.tinyDB.putAdListObject(Common_Utils.key_guj_history_list, HistoryOfExam_Activity.this.history_list_array);
        } else if (HistoryOfExam_Activity.this.getIntent().getStringExtra(str1).equalsIgnoreCase("hindi")) {
          HistoryOfExam_Activity.this.tinyDB.putAdListObject(Common_Utils.key_hindi_history_list, HistoryOfExam_Activity.this.history_list_array);
        } else {
          HistoryOfExam_Activity.this.tinyDB.putAdListObject(Common_Utils.key_marathi_history_list, HistoryOfExam_Activity.this.history_list_array);
        }
        if (HistoryOfExam_Activity.this.history_list_array.size() == 0) {
          HistoryOfExam_Activity.this.clear_all.setVisibility(0);
          HistoryOfExam_Activity.this.clear_all.setEnabled(false);
          HistoryOfExam_Activity.this.clear_all.setClickable(false);
          HistoryOfExam_Activity.this.clear_all.setAlpha(0.5f);
          HistoryOfExam_Activity.this.txt_no_data.setVisibility(0);
          HistoryOfExam_Activity.this.listView.setVisibility(8);
        } else {
          HistoryOfExam_Activity.this.clear_all.setEnabled(true);
          HistoryOfExam_Activity.this.clear_all.setClickable(true);
          HistoryOfExam_Activity.this.clear_all.setAlpha(1.0f);
          HistoryOfExam_Activity.this.clear_all.setVisibility(0);
          HistoryOfExam_Activity.this.txt_no_data.setVisibility(8);
          HistoryOfExam_Activity.this.listView.setVisibility(0);
          HistoryOfExam_Activity.this.myAdapter.notifyDataSetChanged();
          HistoryOfExam_Activity.this.registerForContextMenu(HistoryOfExam_Activity.this.listView);
        }
        Toast.makeText(HistoryOfExam_Activity.this, "History Delete Successfully.. ", Toast.LENGTH_SHORT).show();
      });
      builder.setNegativeButton("No", null);
      builder.show();
    });
  }

  class MyAdapter extends BaseAdapter {
    final String attendQ;
    final Context context;
    final ArrayList<Model_ExamHistory> examHistoryModels;
    final String examdate;
    final String rightQ;
    final String wrongQ;

    public long getItemId(int i) {
      return i;
    }

    public MyAdapter(Context context2, ArrayList<Model_ExamHistory> arrayList) {
      this.context = context2;
      this.examHistoryModels = arrayList;
      String str = "lang";
      if (HistoryOfExam_Activity.this.getIntent().getStringExtra(str).equalsIgnoreCase("english")) {
        this.examdate = "Exam Date";
        this.attendQ = "Attended Question";
        this.rightQ = "Correct Answer";
        this.wrongQ = "Wrong Answer";
      } else if (HistoryOfExam_Activity.this.getIntent().getStringExtra(str).equalsIgnoreCase("gujarati")) {
        this.examdate = "પરીક્ષા તારીખ";
        this.attendQ = "પ્રશ્ન હાજરી";
        this.rightQ = "સાચો જવાબ";
        this.wrongQ = "ખોટો જવાબ";
      } else {
        boolean equalsIgnoreCase = HistoryOfExam_Activity.this.getIntent().getStringExtra(str).equalsIgnoreCase("hindi");
        String str2 = "गलत जवाब";
        String str3 = "सही उत्तर";
        String str4 = "परीक्षा तारीख";
        if (equalsIgnoreCase) {
          this.examdate = str4;
          this.attendQ = "सवाल उपस्थित";
          this.rightQ = str3;
          this.wrongQ = str2;
          return;
        }
        this.examdate = str4;
        this.attendQ = "प्रश्न उपस्थित";
        this.rightQ = str3;
        this.wrongQ = str2;
      }
    }

    public int getCount() {
      return this.examHistoryModels.size();
    }

    public Object getItem(int i) {
      return this.examHistoryModels.get(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
      if (view == null) {
        view = LayoutInflater.from(this.context).inflate(R.layout.history_exam_item, viewGroup, false);
      }
      TextView textView = view.findViewById(R.id.examdate);
      TextView textView2 = view.findViewById(R.id.date);
      TextView textView3 = view.findViewById(R.id.pass_result);
      TextView textView4 = view.findViewById(R.id.txtattend);
      TextView textView5 = view.findViewById(R.id.attend);
      TextView textView6 = view.findViewById(R.id.txtcorrect);
      TextView textView7 = view.findViewById(R.id.correct);
      TextView textView8 = view.findViewById(R.id.txtwrong);
      TextView textView9 = view.findViewById(R.id.wrong);
      Model_ExamHistory examHistoryModel = this.examHistoryModels.get(i);
      textView.setText(this.examdate);
      textView4.setText(this.attendQ);
      textView6.setText(this.rightQ);
      textView8.setText(this.wrongQ);
      textView2.setText(examHistoryModel.getDate());
      textView3.setText(examHistoryModel.getReuslt());
      String str = "";
      String sb = str +
              examHistoryModel.getAttend_que();
      textView5.setText(sb);
      String sb2 = str +
              examHistoryModel.getCorrect_ans();
      textView7.setText(sb2);
      String sb3 = str +
              examHistoryModel.getWrong_ans();
      textView9.setText(sb3);
      if (examHistoryModel.getPass() == 0) {
        textView.setTextColor(SupportMenu.CATEGORY_MASK);
        textView2.setTextColor(SupportMenu.CATEGORY_MASK);
        textView3.setTextColor(SupportMenu.CATEGORY_MASK);
        textView4.setTextColor(SupportMenu.CATEGORY_MASK);
        textView5.setTextColor(SupportMenu.CATEGORY_MASK);
        textView6.setTextColor(SupportMenu.CATEGORY_MASK);
        textView7.setTextColor(SupportMenu.CATEGORY_MASK);
        textView8.setTextColor(SupportMenu.CATEGORY_MASK);
        textView9.setTextColor(SupportMenu.CATEGORY_MASK);
      } else {
        textView.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        textView2.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        textView3.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        textView4.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        textView5.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        textView6.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        textView7.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        textView8.setTextColor(ViewCompat.MEASURED_STATE_MASK);
        textView9.setTextColor(ViewCompat.MEASURED_STATE_MASK);
      }
      return view;
    }
  }


  public boolean onContextItemSelected(MenuItem menuItem) {
    if (menuItem.getTitle().equals("Delete")) {
      Builder builder = new Builder(this);
      builder.setTitle("Delete?");
      builder.setMessage("Are you sure want to delete?");
      builder.setPositiveButton("Yes", (dialogInterface, i1) -> {
        HistoryOfExam_Activity.this.history_list_array.remove(i1);
        String str = "lang";
        if (HistoryOfExam_Activity.this.getIntent().getStringExtra(str).equalsIgnoreCase("english")) {
          HistoryOfExam_Activity.this.tinyDB.putAdListObject(Common_Utils.key_eng_history_list, HistoryOfExam_Activity.this.history_list_array);
        } else if (HistoryOfExam_Activity.this.getIntent().getStringExtra(str).equalsIgnoreCase("gujarati")) {
          HistoryOfExam_Activity.this.tinyDB.putAdListObject(Common_Utils.key_guj_history_list, HistoryOfExam_Activity.this.history_list_array);
        } else if (HistoryOfExam_Activity.this.getIntent().getStringExtra(str).equalsIgnoreCase("hindi")) {
          HistoryOfExam_Activity.this.tinyDB.putAdListObject(Common_Utils.key_hindi_history_list, HistoryOfExam_Activity.this.history_list_array);
        } else {
          HistoryOfExam_Activity.this.tinyDB.putAdListObject(Common_Utils.key_marathi_history_list, HistoryOfExam_Activity.this.history_list_array);
        }
        if (HistoryOfExam_Activity.this.history_list_array.size() == 0) {
          HistoryOfExam_Activity.this.clear_all.setVisibility(View.GONE);
          HistoryOfExam_Activity.this.txt_no_data.setVisibility(View.VISIBLE);
          HistoryOfExam_Activity.this.listView.setVisibility(View.GONE);
        } else {
          HistoryOfExam_Activity.this.clear_all.setVisibility(View.VISIBLE);
          HistoryOfExam_Activity.this.txt_no_data.setVisibility(View.GONE);
          HistoryOfExam_Activity.this.listView.setVisibility(View.VISIBLE);
          HistoryOfExam_Activity.this.myAdapter.notifyDataSetChanged();
          HistoryOfExam_Activity examHistoryActivity = HistoryOfExam_Activity.this;
          examHistoryActivity.registerForContextMenu(examHistoryActivity.listView);
        }
        Toast.makeText(HistoryOfExam_Activity.this, "Delete Successfully.. ", Toast.LENGTH_SHORT).show();
      });
      builder.setNegativeButton("No", null);
      builder.show();
    }
    return super.onContextItemSelected(menuItem);
  }
}
