package com.rtovehicle.information.carbikeinfo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.CarFullDetails_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_Car;

import java.util.ArrayList;

public class CarListinfo_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final Context context;
    final ArrayList<Model_Car.Car_Data> carList_array;
    public static final String CAR_ID = "car_id";
    public static final String CAR_IMAGES = "car_images";
    public static final String CAR_NAME = "car_name";
    public static final String CAR_PRICE = "car_price";
    public static final String CAR_COLOR_VARIENTS = "car_color_varients";
    public static final String CAR_VARIENTS = "car_varients";
    public static final String IMAGE_URL = "image_url";
    final String image_url;

    public CarListinfo_Adapter(Context context, ArrayList<Model_Car.Car_Data> carList, String image_url) {
        this.context = context;
        this.carList_array = carList;
        this.image_url = image_url;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_car_list, parent, false);
        return new CarHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Model_Car.Car_Data car_data = carList_array.get(position);

        CarHolder carHolder = (CarHolder) holder;
        carHolder.car_ShapeableImageView.setShapeAppearanceModel(
                new ShapeAppearanceModel.Builder()
                        .setAllCorners(CornerFamily.ROUNDED, 8)
                        .build());

        Glide.with(context)
                .load(car_data.getImage())
                .placeholder(R.drawable.placeholder)
                .centerCrop()
                .into(carHolder.car_ShapeableImageView);
        carHolder.car_name_text.setText(car_data.getModel_name());
        carHolder.car_showroom_price_text.setText(car_data.getEx_showroom_price());
        carHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, CarFullDetails_Activity.class);
            intent.putExtra(CAR_ID, car_data.getId());
            intent.putExtra(CAR_IMAGES, car_data.getModel_images());
            intent.putExtra(CAR_PRICE, car_data.getEx_showroom_price());
            intent.putExtra(CAR_NAME, car_data.getModel_name());
            intent.putExtra(CAR_COLOR_VARIENTS, car_data.getModel_color());
            intent.putExtra(IMAGE_URL, image_url);
            intent.putExtra(CAR_VARIENTS, car_data.getModel_variants());
            intent.putExtra(CompanyInfo_Adapter.VEHICLE_TYPE, "car");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return carList_array.size();
    }

    public static class CarHolder extends RecyclerView.ViewHolder {
        final ShapeableImageView car_ShapeableImageView;
        final TextView car_name_text;
        final TextView car_showroom_price_text;

        public CarHolder(@NonNull View itemView) {
            super(itemView);
            car_ShapeableImageView = itemView.findViewById(R.id.car_image);
            car_name_text = itemView.findViewById(R.id.car_name);
            car_showroom_price_text = itemView.findViewById(R.id.car_showroom_price);
        }
    }
}
