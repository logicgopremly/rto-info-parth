package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_BikeVariantDetails {

    @SerializedName("status")
    boolean status;

    @SerializedName("data")
    @Expose
    ArrayList<Variant_Data> data;

    public boolean isStatus() {
        return status;
    }


    public ArrayList<Variant_Data> getData() {
        return data;
    }

    public static class Variant_Data {
        @SerializedName("id")
        int id;

        @SerializedName("engine")
        @Expose
        Engine engine;

        @SerializedName("milege")
        @Expose
        Milege milege;

        @SerializedName("features")
        @Expose
        Features features;

        @SerializedName("chassis")
        @Expose
        Chassis chassis;

        @SerializedName("dimensions")
        @Expose
        Dimension dimension;

        @SerializedName("electicals")
        @Expose
        Electicals electicals;

        @SerializedName("type")
        @Expose
        Type type;

        @SerializedName("others")
        @Expose
        Others others;

        public int getId() {
            return id;
        }

        public Engine getEngine() {
            return engine;
        }

        public Dimension getDimension() {
            return dimension;
        }

        public Milege getMilege() {
            return milege;
        }

        public Features getFeatures() {
            return features;
        }

        public Chassis getChassis() {
            return chassis;
        }

        public Electicals getElecticals() {
            return electicals;
        }

        public Type getType() {
            return type;
        }

        public Others getOthers() {
            return others;
        }

        public static class Engine {
            @SerializedName("Engine Type")
            String engineType;

            @SerializedName("Displacement")
            String displacement;

            @SerializedName("Max Power")
            String maxPower;

            @SerializedName("Max Torque")
            String maxTorque;

            @SerializedName("No. of Cylinders")
            String noOfCylinder;

            @SerializedName("Cooling System")
            String coolingSystem;

            @SerializedName("Valve Per Cylinder")
            String valvesPerCylinder;

            @SerializedName("Drive Type")
            String driveType;

            @SerializedName("Starting")
            String starting;

            @SerializedName("Fuel Supply")
            String fuelSupply;

            @SerializedName("Clutch")
            String clutch;

            @SerializedName("Transmission")
            String transmission;

            @SerializedName("Gear Box")
            String gearBox;

            @SerializedName("Bore")
            String bore;

            @SerializedName("Stroke")
            String stroke;

            @SerializedName("Compression Ratio")
            String compressionRatio;

            @SerializedName("Emission Type")
            String emissionType;

            @SerializedName("Ignition")
            String ignition;

            public String getEngineType() {
                return engineType;
            }

            public String getDisplacement() {
                return displacement;
            }

            public String getMaxPower() {
                return maxPower;
            }

            public String getMaxTorque() {
                return maxTorque;
            }

            public String getNoOfCylinder() {
                return noOfCylinder;
            }

            public String getCoolingSystem() {
                return coolingSystem;
            }

            public String getValvesPerCylinder() {
                return valvesPerCylinder;
            }

            public String getDriveType() {
                return driveType;
            }

            public String getStarting() {
                return starting;
            }

            public String getFuelSupply() {
                return fuelSupply;
            }

            public String getClutch() {
                return clutch;
            }

            public String getTransmission() {
                return transmission;
            }

            public String getGearBox() {
                return gearBox;
            }

            public String getBore() {
                return bore;
            }

            public String getStroke() {
                return stroke;
            }

            public String getCompressionRatio() {
                return compressionRatio;
            }

            public String getEmissionType() {
                return emissionType;
            }

            public String getIgnition() {
                return ignition;
            }
        }

        public static class Milege {
            @SerializedName("City Mileage")
            String cityMileage;

            @SerializedName("Highway Mileage")
            String highwayMileage;

            @SerializedName("Max Speed")
            String maxSpeed;

            @SerializedName("Acceleration (0-60 Kmph)")
            String accelerationZtoSixty;

            @SerializedName("Acceleration (0-80 Kmph)")
            String accelerationZtoEighty;

            @SerializedName("Quarter Mile")
            String quarterMile;

            @SerializedName("Braking (60-0 Kmph)")
            String braking;

            public String getCityMileage() {
                return cityMileage;
            }

            public String getHighwayMileage() {
                return highwayMileage;
            }

            public String getMaxSpeed() {
                return maxSpeed;
            }

            public String getAccelerationZtoSixty() {
                return accelerationZtoSixty;
            }

            public String getAccelerationZtoEighty() {
                return accelerationZtoEighty;
            }

            public String getQuarterMile() {
                return quarterMile;
            }

            public String getBraking() {
                return braking;
            }
        }

        public static class Features {
            @SerializedName("ABS")
            String ABS;

            @SerializedName("Braking Type")
            String brakingType;

            @SerializedName("Speedometer")
            String speedometer;

            @SerializedName("Tachometer")
            String tachometer;

            @SerializedName("Odometer")
            String odometer;

            @SerializedName("Tripmeter")
            String tripmeter;

            @SerializedName("Fuel Gauge")
            String fuelGuage;

            @SerializedName("Console")
            String console;

            @SerializedName("Pass Switch")
            String passSwitch;

            @SerializedName("Passenger Footrest")
            String passengerFootrest;

            @SerializedName("Clock")
            String clock;

            @SerializedName("Additional Features")
            String additionalFeatures;

            @SerializedName("Display")
            String display;

            @SerializedName("Service Due Indicator")
            String serviceDueIndicator;

            @SerializedName("External Fuel Filling")
            String externalFuelFilling;

            @SerializedName("Carry Hook")
            String carryHook;

            @SerializedName("Real Time Mileage Indicator")
            String realTimeMileageIndicator;

            @SerializedName("Charging Point")
            String chargingPoint;

            @SerializedName("Stepup Seat")
            String stepupSeat;

            @SerializedName("i3s Technology")
            String i3sTechnology;

            public String getABS() {
                return ABS;
            }

            public String getBrakingType() {
                return brakingType;
            }

            public String getSpeedometer() {
                return speedometer;
            }

            public String getTachometer() {
                return tachometer;
            }

            public String getOdometer() {
                return odometer;
            }

            public String getTripmeter() {
                return tripmeter;
            }

            public String getFuelGuage() {
                return fuelGuage;
            }

            public String getConsole() {
                return console;
            }

            public String getPassSwitch() {
                return passSwitch;
            }

            public String getPassengerFootrest() {
                return passengerFootrest;
            }

            public String getClock() {
                return clock;
            }

            public String getAdditionalFeatures() {
                return additionalFeatures;
            }

            public String getDisplay() {
                return display;
            }

            public String getServiceDueIndicator() {
                return serviceDueIndicator;
            }

            public String getExternalFuelFilling() {
                return externalFuelFilling;
            }

            public String getCarryHook() {
                return carryHook;
            }

            public String getRealTimeMileageIndicator() {
                return realTimeMileageIndicator;
            }

            public String getChargingPoint() {
                return chargingPoint;
            }

            public String getStepupSeat() {
                return stepupSeat;
            }

            public String getI3sTechnology() {
                return i3sTechnology;
            }
        }

        public static class Chassis {
            @SerializedName("Chassis")
            String Chassis;

            @SerializedName("Body Type")
            String bodyType;

            @SerializedName("Front Suspension")
            String frontSuspension;

            @SerializedName("Rear Suspension")
            String rearSuspension;

            @SerializedName("Body Graphics")
            String bodyGraphics;

            public String getChassis() {
                return Chassis;
            }

            public String getBodyType() {
                return bodyType;
            }

            public String getFrontSuspension() {
                return frontSuspension;
            }

            public String getRearSuspension() {
                return rearSuspension;
            }

            public String getBodyGraphics() {
                return bodyGraphics;
            }
        }

        public static class Dimension {
            @SerializedName("Length")
            String length;

            @SerializedName("Width")
            String width;

            @SerializedName("Height")
            String height;

            @SerializedName("Fuel Capacity")
            String fuelCapacity;

            @SerializedName("Ground Clearance")
            String groundClearance;

            @SerializedName("Wheelbase")
            String wheelBase;

            @SerializedName("Kerb Weight")
            String kerbWeight;

            @SerializedName("Saddle Height")
            String saddleHeight;

            @SerializedName("Underseat storage")
            String underseatStorage;

            @SerializedName("Load Capacity")
            String loadCapacity;

            public String getLength() {
                return length;
            }

            public String getWidth() {
                return width;
            }

            public String getHeight() {
                return height;
            }

            public String getFuelCapacity() {
                return fuelCapacity;
            }

            public String getGroundClearance() {
                return groundClearance;
            }

            public String getWheelBase() {
                return wheelBase;
            }

            public String getKerbWeight() {
                return kerbWeight;
            }

            public String getSaddleHeight() {
                return saddleHeight;
            }

            public String getUnderseatStorage() {
                return underseatStorage;
            }

            public String getLoadCapacity() {
                return loadCapacity;
            }
        }

        public static class Electicals {
            @SerializedName("Headlight")
            String headlight;

            @SerializedName("Tail Light")
            String tailLight;

            @SerializedName("Turn Signal Lamp")
            String turnsignalLamp;

            @SerializedName("DRLs")
            String DRLs;

            @SerializedName("Low Battery Indicator")
            String lowBatteryIndicator;

            @SerializedName("Low Fuel Indicator")
            String lowFuelIndicator;

            @SerializedName("Battery Capacity")
            String batteryCapacity;

            @SerializedName("Navigation")
            String navigation;

            @SerializedName("LED Tail Lights")
            String LEDTailLights;

            @SerializedName("Battery Type")
            String batteryType;

            @SerializedName("Mobile Connectivity")
            String mobileConnectivity;

            @SerializedName("Boot Light")
            String bootLight;

            public String getHeadlight() {
                return headlight;
            }

            public String getTailLight() {
                return tailLight;
            }

            public String getTurnsignalLamp() {
                return turnsignalLamp;
            }

            public String getDRLs() {
                return DRLs;
            }

            public String getLowBatteryIndicator() {
                return lowBatteryIndicator;
            }

            public String getLowFuelIndicator() {
                return lowFuelIndicator;
            }

            public String getBatteryCapacity() {
                return batteryCapacity;
            }

            public String getNavigation() {
                return navigation;
            }

            public String getLEDTailLights() {
                return LEDTailLights;
            }

            public String getBatteryType() {
                return batteryType;
            }

            public String getMobileConnectivity() {
                return mobileConnectivity;
            }

            public String getBootbLight() {
                return bootLight;
            }
        }

        public static class Type {
            @SerializedName("Tyre Size")
            String tyreSize;

            @SerializedName("Tyre Type")
            String tyreType;

            @SerializedName("Wheel Size")
            String wheelSize;

            @SerializedName("Wheels Type")
            String wheelsType;

            @SerializedName("Front Brake")
            String frontBrake;

            @SerializedName("Rear Brake")
            String rearBrake;

            @SerializedName("Front Brake Diameter")
            String frontBrakeDiameter;

            @SerializedName("Rear Brake Diameter")
            String rearBrakeDiameter;

            @SerializedName("Radial Tyre")
            String radialTyre;

            @SerializedName("Front Tyre Pressure (Rider)")
            String frontTyrePressureRider;

            @SerializedName("Rear Tyre Pressure (Rider)")
            String rearTyrePressureRider;

            @SerializedName("Front Tyre Pressure (Rider & Pillion)")
            String frontTyrePressureRiderPillion;

            @SerializedName("Rear Tyre Pressure (Rider & Pillion)")
            String RearTyrePressureRiderPillion;

            public String getTyreSize() {
                return tyreSize;
            }

            public String getTyreType() {
                return tyreType;
            }

            public String getWheelSize() {
                return wheelSize;
            }

            public String getWheelsType() {
                return wheelsType;
            }

            public String getFrontBrake() {
                return frontBrake;
            }

            public String getRearBrake() {
                return rearBrake;
            }

            public String getFrontBrakeDiameter() {
                return frontBrakeDiameter;
            }

            public String getRearBrakeDiameter() {
                return rearBrakeDiameter;
            }

            public String getRadialTyre() {
                return radialTyre;
            }

            public String getFrontTyrePressureRider() {
                return frontTyrePressureRider;
            }

            public String getRearTyrePressureRider() {
                return rearTyrePressureRider;
            }

            public String getFrontTyrePressureRiderPillion() {
                return frontTyrePressureRiderPillion;
            }

            public String getRearTyrePressureRiderPillion() {
                return RearTyrePressureRiderPillion;
            }
        }

        public static class Others {
            @SerializedName("Fuel Reserve")
            String fuelReserve;

            @SerializedName("Pilot Lamps")
            String pilotLamps;

            @SerializedName("Traction Control")
            String tractionControl;

            @SerializedName("Engine Kill Switch")
            String engineKillSwitch;

            @SerializedName("Seat Opening Switch")
            String seatOpeningSwitch;

            @SerializedName("Distance to Empty Indicator")
            String distancetoEmptyIndicator;

            @SerializedName("Motor Type")
            String motorType;

            @SerializedName("Motor Power")
            String motorPower;

            @SerializedName("Range")
            String range;

            @SerializedName("Battery Charging Time")
            String batteryChargingTime;

            @SerializedName("Fast Charging")
            String fastCharging;

            @SerializedName("Riding Modes")
            String ridingModes;

            @SerializedName("Projector Headlights")
            String projectorHeadlights;

            @SerializedName("Engine Immobilizer")
            String engineImmobilizer;

            @SerializedName("Anti Theft Alarm")
            String antiTheftAlarm;

            @SerializedName("ARAI Mileage")
            String ARAIMileage;

            @SerializedName("Acceleration (0-40 Kmph)")
            String AccelerationZtoForty;

            @SerializedName("Average Fuel economy Indicator")
            String averageFuelEconomyIndicator;

            public String getFuelReserve() {
                return fuelReserve;
            }

            public String getPilotLamps() {
                return pilotLamps;
            }

            public String getTractionControl() {
                return tractionControl;
            }

            public String getEngineKillSwitch() {
                return engineKillSwitch;
            }

            public String getSeatOpeningSwitch() {
                return seatOpeningSwitch;
            }

            public String getDistancetoEmptyIndicator() {
                return distancetoEmptyIndicator;
            }

            public String getMotorType() {
                return motorType;
            }

            public String getMotorPower() {
                return motorPower;
            }

            public String getRange() {
                return range;
            }

            public String getBatteryChargingTime() {
                return batteryChargingTime;
            }

            public String getFastCharging() {
                return fastCharging;
            }

            public String getRidingModes() {
                return ridingModes;
            }

            public String getProjectorHeadlights() {
                return projectorHeadlights;
            }

            public String getEngineImmobilizer() {
                return engineImmobilizer;
            }

            public String getAntiTheftAlarm() {
                return antiTheftAlarm;
            }

            public String getARAIMileage() {
                return ARAIMileage;
            }

            public String getAccelerationZtoForty() {
                return AccelerationZtoForty;
            }

            public String getAverageFuelEconomyIndicator() {
                return averageFuelEconomyIndicator;
            }
        }
    }

}
