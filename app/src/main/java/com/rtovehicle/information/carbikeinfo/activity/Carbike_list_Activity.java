package com.rtovehicle.information.carbikeinfo.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.airbnb.lottie.LottieAnimationView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.BikeListinfo_Adapter;
import com.rtovehicle.information.carbikeinfo.adapter.CarListinfo_Adapter;
import com.rtovehicle.information.carbikeinfo.adapter.CompanyInfo_Adapter;
import com.rtovehicle.information.carbikeinfo.common.GridMarginDecoration;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_Bike;
import com.rtovehicle.information.carbikeinfo.model.Model_Car;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.ferfalk.simplesearchview.SimpleSearchView;
import com.mikelau.views.shimmer.ShimmerRecyclerViewX;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Carbike_list_Activity extends AppCompatActivity {

    ArrayList<Model_Car.Car_Data> array_carList;
    ArrayList<Model_Bike.Bike_Data> array_bikeList;
    ShimmerRecyclerViewX rv_car_list;
    String image_url;
    String vehicle_type;
    SimpleSearchView searchView_simple;
    CarListinfo_Adapter carListinfo_adapter;
    BikeListinfo_Adapter bikeListinfo_adapter;
    LottieAnimationView no_data_found;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carbike_list);
        getWindow().setBackgroundDrawable(null);
        init();

        int company_id = getIntent().getIntExtra(CompanyInfo_Adapter.COMPANY_ID, -1);
        vehicle_type = getIntent().getStringExtra(CompanyInfo_Adapter.VEHICLE_TYPE);

        if (Objects.equals(vehicle_type, "car")) {
            getCarList(company_id);
        } else {
            getBikeList(company_id);
        }
    }

    private void init() {

        array_carList = new ArrayList<>();
        array_bikeList = new ArrayList<>();
        rv_car_list = findViewById(R.id.rv_car_list);
        searchView_simple = findViewById(R.id.searchview);
        no_data_found = findViewById(R.id.nodata_vehicle);
        rv_car_list.setLayoutManager(new LinearLayoutManager(this));
        rv_car_list.setHasFixedSize(true);
        float albumGridSpacing = getResources().getDimension(R.dimen.timeline_decorator_spacing);
        rv_car_list.addItemDecoration(new GridMarginDecoration((int) albumGridSpacing));
        String company_name = getIntent().getStringExtra(CompanyInfo_Adapter.COMPANY_NAME);
        ((TextView) findViewById(R.id.app_title)).setText(company_name);
        rv_car_list.setDemoChildCount(25);
        findViewById(R.id.back_all).setOnClickListener(v -> onBackPressed());
        findViewById(R.id.searchbtn).setOnClickListener(v -> searchView_simple.showSearch());

    }

    private void getCarList(int company_id) {
        rv_car_list.showShimmerAdapter();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_Car> call = service.getCarList(MyApplication.MYSECRET, company_id);
        call.enqueue(new Callback<Model_Car>() {
            @Override
            public void onResponse(@NonNull Call<Model_Car> call, @NonNull Response<Model_Car> response) {
                rv_car_list.hideShimmerAdapter();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            try {
                                array_carList = response.body().getData();
                                image_url = response.body().getUrl();
                                if (array_carList.size() == 0) {
                                    no_data_found.setVisibility(View.VISIBLE);
                                } else {
                                    no_data_found.setVisibility(View.GONE);
                                }
                                carListinfo_adapter = new CarListinfo_Adapter(Carbike_list_Activity.this, array_carList, image_url);
                                rv_car_list.setAdapter(carListinfo_adapter);

                                searchView_simple.setOnQueryTextListener(new SimpleSearchView.OnQueryTextListener() {
                                    @Override
                                    public boolean onQueryTextSubmit(String query) {
                                        ArrayList<Model_Car.Car_Data> sort_list = new ArrayList<>();
                                        for (int i = 0; i < array_carList.size(); i++) {

                                            if (array_carList.get(i).getModel_name().toLowerCase().contains(query.toLowerCase())) {
                                                sort_list.add(array_carList.get(i));
                                            }
                                        }
                                        carListinfo_adapter = new CarListinfo_Adapter(Carbike_list_Activity.this, sort_list, image_url);
                                        rv_car_list.setAdapter(carListinfo_adapter);
                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextChange(String newText) {
                                        ArrayList<Model_Car.Car_Data> sort_list = new ArrayList<>();
                                        for (int i = 0; i < array_carList.size(); i++) {

                                            if (array_carList.get(i).getModel_name().toLowerCase().contains(newText.toLowerCase())) {
                                                sort_list.add(array_carList.get(i));
                                            }
                                        }
                                        carListinfo_adapter = new CarListinfo_Adapter(Carbike_list_Activity.this, sort_list, image_url);
                                        rv_car_list.setAdapter(carListinfo_adapter);
                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextCleared() {
                                        carListinfo_adapter = new CarListinfo_Adapter(Carbike_list_Activity.this, array_carList, image_url);
                                        rv_car_list.setAdapter(carListinfo_adapter);
                                        return false;
                                    }
                                });

                            } catch (Exception ignored) {

                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_Car> call, @NonNull Throwable t) {
                rv_car_list.hideShimmerAdapter();
            }
        });
    }

    private void getBikeList(int company_id) {
        rv_car_list.showShimmerAdapter();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_Bike> call = service.getBikeList(MyApplication.MYSECRET, company_id);
        call.enqueue(new Callback<Model_Bike>() {
            @Override
            public void onResponse(@NonNull Call<Model_Bike> call, @NonNull Response<Model_Bike> response) {
                rv_car_list.hideShimmerAdapter();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            try {
                                array_bikeList = response.body().getData();
                                image_url = response.body().getUrl();
                                if (array_bikeList.size() == 0) {
                                    no_data_found.setVisibility(View.VISIBLE);
                                } else {
                                    no_data_found.setVisibility(View.GONE);
                                }
                                bikeListinfo_adapter = new BikeListinfo_Adapter(Carbike_list_Activity.this, array_bikeList, image_url);
                                rv_car_list.setAdapter(bikeListinfo_adapter);

                                searchView_simple.setOnQueryTextListener(new SimpleSearchView.OnQueryTextListener() {
                                    @Override
                                    public boolean onQueryTextSubmit(String query) {
                                        ArrayList<Model_Bike.Bike_Data> sort_list = new ArrayList<>();
                                        for (int i = 0; i < array_bikeList.size(); i++) {

                                            if (array_bikeList.get(i).getModel_name().toLowerCase().contains(query.toLowerCase())) {
                                                sort_list.add(array_bikeList.get(i));
                                            }
                                        }
                                        bikeListinfo_adapter = new BikeListinfo_Adapter(Carbike_list_Activity.this, sort_list, image_url);
                                        rv_car_list.setAdapter(bikeListinfo_adapter);
                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextChange(String newText) {
                                        ArrayList<Model_Bike.Bike_Data> sort_list = new ArrayList<>();
                                        for (int i = 0; i < array_bikeList.size(); i++) {

                                            if (array_bikeList.get(i).getModel_name().toLowerCase().contains(newText.toLowerCase())) {
                                                sort_list.add(array_bikeList.get(i));
                                            }
                                        }
                                        bikeListinfo_adapter = new BikeListinfo_Adapter(Carbike_list_Activity.this, sort_list, image_url);
                                        rv_car_list.setAdapter(bikeListinfo_adapter);
                                        return false;
                                    }

                                    @Override
                                    public boolean onQueryTextCleared() {
                                        bikeListinfo_adapter = new BikeListinfo_Adapter(Carbike_list_Activity.this, array_bikeList, image_url);
                                        rv_car_list.setAdapter(bikeListinfo_adapter);
                                        return false;
                                    }
                                });

                            } catch (Exception ignored) {

                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_Bike> call, @NonNull Throwable t) {
                rv_car_list.hideShimmerAdapter();
            }
        });
    }
}