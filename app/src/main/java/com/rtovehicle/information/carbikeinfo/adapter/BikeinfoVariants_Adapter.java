package com.rtovehicle.information.carbikeinfo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.BikeVariantInformation_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_BikeVariant;

import java.util.ArrayList;

public class BikeinfoVariants_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final Context context;
    final ArrayList<Model_BikeVariant> variantsList;

    public static final String VARIANT_NAME = "variant_name";
    public static final String VARIANT_ID = "variant_id";

    public BikeinfoVariants_Adapter(Context context, ArrayList<Model_BikeVariant> varientsList) {
        this.context = context;
        this.variantsList = varientsList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_varients_list_bike, parent, false);
        return new VarientsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Model_BikeVariant bikeVariant = variantsList.get(position);

        VarientsHolder varientsHolder = (VarientsHolder) holder;
        varientsHolder.model_name_text.setText(bikeVariant.getVariant_name());
        varientsHolder.bike_price_text.setText(bikeVariant.getVariant_price());
        varientsHolder.bike_cc_text.setText(bikeVariant.getCc());

        varientsHolder.card_varient.setOnClickListener(v -> {
            Intent intent = new Intent(context, BikeVariantInformation_Activity.class);
            intent.putExtra(VARIANT_NAME, bikeVariant.getVariant_name());
            intent.putExtra(VARIANT_ID, bikeVariant.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return variantsList.size();
    }

    public static class VarientsHolder extends RecyclerView.ViewHolder {
        final TextView model_name_text;
        final TextView bike_price_text;
        final TextView bike_cc_text;
        final LinearLayout card_varient;

        public VarientsHolder(@NonNull View itemView) {
            super(itemView);
            card_varient = itemView.findViewById(R.id.card_varient);
            model_name_text = itemView.findViewById(R.id.model_name);
            bike_price_text = itemView.findViewById(R.id.bike_showroom_price);
            bike_cc_text = itemView.findViewById(R.id.bike_cc);
        }
    }

}
