package com.rtovehicle.information.carbikeinfo.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.rtovehicle.information.carbikeinfo.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MyApplication extends MultiDexApplication {
    public static SharedPreferences preferences;
    public static SharedPreferences.Editor mEditor;
    @SuppressLint("StaticFieldLeak")
    public static Context context;


    public static String MYSECRET = "";
    @SuppressLint("StaticFieldLeak")
    public static AppOpenManager appOpenManager;


    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }


    public void onCreate() {
        super.onCreate();
        context = this;
        appOpenManager = new AppOpenManager(this);
        preferences = getSharedPreferences("ps", MODE_PRIVATE);
        mEditor = preferences.edit();
        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.NO_WRAP);
                key = key.replaceAll("[^a-zA-Z0-9]", "");
                MYSECRET = key;

            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException ignored) {

        }
    }
    public static void set_AdsInt(int adsInt) {
        mEditor.putInt("adsInt", adsInt).commit();
    }

    public static int get_AdsInt() {
        return preferences.getInt("adsInt", 3);
    }

    //admob interstitial
    public static void set_Admob_interstitial_Id(String Admob_interstitial_Id) {
        mEditor.putString("Admob_interstitial_Id", Admob_interstitial_Id).commit();
    }

    public static String get_Admob_interstitial_Id() {
        return preferences.getString("Admob_interstitial_Id", context.getString(R.string.interstitial));
    }

    //admob open
    public static void set_Admob_banner_Id(String Admob_banner_Id) {
        mEditor.putString("Admob_banner_Id", Admob_banner_Id).commit();
    }

    public static String get_Admob_banner_Id() {
        return preferences.getString("Admob_banner_Id", context.getString(R.string.banner_id));
    }

    //admob native
    public static void set_Admob_native_Id(String Admob_native_Id) {
        mEditor.putString("Admob_native_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_native_Id() {
        return preferences.getString("Admob_native_Id", context.getString(R.string.native_ad));
    }
    //admob openapp
    public static void set_Admob_openapp(String Admob_native_Id) {
        mEditor.putString("Admob_open_Id", Admob_native_Id).commit();
    }

    public static String get_Admob_openapp() {
        return preferences.getString("Admob_open_Id", context.getString(R.string.open_app_ad_id));
    }



}
