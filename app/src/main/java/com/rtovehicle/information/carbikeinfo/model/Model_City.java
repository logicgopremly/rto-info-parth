package com.rtovehicle.information.carbikeinfo.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Model_City implements Parcelable {
    public static final Creator<Model_City> CREATOR = new Creator<Model_City>() {
        public Model_City createFromParcel(Parcel parcel) {
            return new Model_City(parcel);
        }

        public Model_City[] newArray(int i) {
            return new Model_City[i];
        }
    };
    private String autogas;
    private String city;
    private String createdAt;
    private List<String> data = null;
    private String gas;
    private Integer id;
    public ArrayList<HashMap<String, List>> list;
    private String price;
    private String stateName;
    private String title;
    private String type;
    private String yPrice;

    public int describeContents() {
        return 0;
    }

    public Model_City() {
    }

    public Model_City(Parcel parcel) {
        this.data = parcel.createStringArrayList();
        this.title = parcel.readString();
        this.city = parcel.readString();
        this.createdAt = parcel.readString();
        if (parcel.readByte() == 0) {
            this.id = null;
        } else {
            this.id = parcel.readInt();
        }
        this.price = parcel.readString();
        this.stateName = parcel.readString();
        this.type = parcel.readString();
        this.yPrice = parcel.readString();
        this.gas = parcel.readString();
        this.autogas = parcel.readString();
    }

    public void setData(List<String> list2) {
        this.data = list2;
    }
    public String getTitle() {
        return this.title;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public List<String> getData() {
        return this.data;
    }

    public void setGas(String str) {
        this.gas = str;
    }

    public void setAutogas(String str) {
        this.autogas = str;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String str) {
        this.type = str;
    }

    public String getStateName() {
        return this.stateName;
    }

    public void setStateName(String str) {
        this.stateName = str;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String str) {
        this.city = str;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String getYPrice() {
        return this.yPrice;
    }

    public void setYPrice(String str) {
        this.yPrice = str;
    }

    public void setCreatedAt(String str) {
        this.createdAt = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(this.data);
        parcel.writeString(this.title);
        parcel.writeString(this.city);
        parcel.writeString(this.createdAt);
        if (this.id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.id);
        }
        parcel.writeString(this.price);
        parcel.writeString(this.stateName);
        parcel.writeString(this.type);
        parcel.writeString(this.yPrice);
        parcel.writeString(this.gas);
        parcel.writeString(this.autogas);
    }
}
