package com.rtovehicle.information.carbikeinfo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.Carbike_list_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_Company;

import java.util.ArrayList;

public class CompanyInfo_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String COMPANY_ID = "company_id";
    public static final String COMPANY_NAME = "company_name";
    public static final String VEHICLE_TYPE = "vehicle_type";
    final Context context;
    final ArrayList<Model_Company.Company_Data> company_dataArrayList;
    final String vehicle_type;

    public CompanyInfo_Adapter(Context context, ArrayList<Model_Company.Company_Data> companyList, String vehicle_type) {
        this.context = context;
        this.company_dataArrayList = companyList;
        this.vehicle_type = vehicle_type;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_companyinfo_list, parent, false);
        return new CompanyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Model_Company.Company_Data company_data = company_dataArrayList.get(position);

        CompanyHolder companyHolder = (CompanyHolder) holder;

        Glide.with(context)
                .load(company_data.getCompany_logo())
                .placeholder(R.drawable.placeholder)
                .into(companyHolder.company_logo_img);
        companyHolder.company_name_textview.setText(company_data.getCompany_name());
        companyHolder.company_name_textview.setSelected(true);

        companyHolder.itemView.setOnClickListener(v -> {
            Intent intent = new Intent(context, Carbike_list_Activity.class);
            intent.putExtra(COMPANY_ID, company_data.getId());
            intent.putExtra(COMPANY_NAME, company_data.getCompany_name());
            intent.putExtra(VEHICLE_TYPE, vehicle_type);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return company_dataArrayList.size();
    }

    public static class CompanyHolder extends RecyclerView.ViewHolder {
        final ImageView company_logo_img;
        final TextView company_name_textview;

        public CompanyHolder(@NonNull View itemView) {
            super(itemView);
            company_logo_img = itemView.findViewById(R.id.company_logo);
            company_name_textview = itemView.findViewById(R.id.company_name);
        }
    }

}
