package com.rtovehicle.information.carbikeinfo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;


public class RC_info_Adapter extends RecyclerView.Adapter<RC_info_Adapter.ViewHolder> {
    final Context context;
    final LayoutInflater inflater;
    final String[] number_array;
    final int[] image;
    final OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int i);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemClickListener {
        final TextView rcName_text;
        final ImageView rc_icon_img;

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        }

        public ViewHolder(@NonNull final View view) {
            super(view);
            this.rc_icon_img = view.findViewById(R.id.rc_icon);
            this.rcName_text = view.findViewById(R.id.rcName);
            view.setOnClickListener(view1 -> RC_info_Adapter.this.onItemClickListener.onItemClick(view1, ViewHolder.this.getAdapterPosition()));
        }
    }

    public RC_info_Adapter(Context context2, String[] strArr2, int[] intArr, OnItemClickListener onItemClickListener2) {
        this.context = context2;
        this.number_array = strArr2;
        this.image = intArr;
        this.onItemClickListener = onItemClickListener2;
        this.inflater = LayoutInflater.from(context2);
    }

    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(this.inflater.inflate(R.layout.rc_list_item, viewGroup, false));
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        try {
            viewHolder.rcName_text.setText(this.number_array[i]);
            Resources resources = context.getResources();
            viewHolder.rc_icon_img.setImageDrawable(resources.getDrawable(image[i]));
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public int getItemCount() {
        return this.number_array.length;
    }
}
