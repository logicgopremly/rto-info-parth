package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.rtovehicle.information.carbikeinfo.BuildConfig;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.MostTrendingPerson_Adapter;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.common.PrefManager_fuel;
import com.rtovehicle.information.carbikeinfo.fragment.City_fragment;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_City;
import com.rtovehicle.information.carbikeinfo.model.Model_MostTrending;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Main_Activity extends AppCompatActivity {


    String city_name;
    private RequestQueue mRequestQueue;
    boolean isActivityLeft;
    final List<Model_City> model_cities_array = new ArrayList<>();
    public final ArrayList<Model_MostTrending.DATA_ALL_Most> Most_trading_array = new ArrayList<>();
    private TextView text_diesel;
    private TextView text_petrol;
    private TextView text_state;
    public static boolean city = false;
    SharedPreferences sharedPreferences;
    RecyclerView recyclerView_most_trending;
    private InterstitialAd mInterstitialAd;
    LinearLayout search_linear_module;

    EditText tv_number_vehicle;
    private String vehicleNum_one;
    private String vehicleNum_two;
    FrameLayout adContainerView;
    AdView adView;
    TextView ads_space;
    boolean keyboard;
    private ImageView clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setBackgroundDrawable(null);
        isActivityLeft = false;
        keyboard = false;
        LoadAdInterstitial();
        this.mRequestQueue = Volley.newRequestQueue(this);
        sendAndRequestResponse();
        findviewId();
        Banner();
        checkConnectivity();
        fuel();
        Most_trending_get();
    }

    @SuppressLint("ClickableViewAccessibility")
    public void findviewId() {
        sharedPreferences = getSharedPreferences("rating", Context.MODE_PRIVATE);
        this.text_petrol = findViewById(R.id.tv_rs_petrol);
        this.text_diesel = findViewById(R.id.tv_rs_diesel);
        this.text_state = findViewById(R.id.tv_state);
        recyclerView_most_trending = findViewById(R.id.most_trending);
        adContainerView = findViewById(R.id.ad_view_container);
        search_linear_module = findViewById(R.id.search_linear);
        ads_space = findViewById(R.id.bottom_space);
        clear = findViewById(R.id.clear);


        if (Common_Utils.search_Module) {
            search_linear_module.setVisibility(View.VISIBLE);
        } else {
            search_linear_module.setVisibility(View.GONE);
        }
        tv_number_vehicle = findViewById(R.id.edtSearchVehical);

        tv_number_vehicle.setOnTouchListener((view, motionEvent) -> {
            keyboard = true;
            return false;
        });

        tv_number_vehicle.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11), new InputFilter.AllCaps()});
        tv_number_vehicle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                clear.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        findViewById(R.id.btnsearch).setOnClickListener(view -> {
            Common_Utils.click_db = 2;
            String[] part;
            hideKeyboard(Main_Activity.this);
            if (isOnline()) {
                if (tv_number_vehicle.getText().toString().isEmpty()) {
                    tv_number_vehicle.setError("Please enter vehicle number");
                } else {

                    try {
                        part = tv_number_vehicle.getText().toString().split("(?<=\\D)(?=\\d)(?=\\d)");
                        vehicleNum_one = part[0] + part[1];
                        vehicleNum_two = String.format(Locale.US, "%04d", Integer.valueOf(part[2].replace(" ", "").trim()));

                        if (this.vehicleNum_two.length() == 4 && vehicleNum_one.length() == 5 || vehicleNum_one.length() == 6) {
                            try {
                                Intent intent = new Intent(Main_Activity.this, Owner_Vehicleinfo_Activity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                intent.putExtra("vehical_number", tv_number_vehicle.getText().toString().trim());
                                startActivity(intent);
                                try {
                                    if (mInterstitialAd != null && !isActivityLeft) {
                                        mInterstitialAd.show(Main_Activity.this);
                                        MyApplication.appOpenManager.isAdShow = true;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            invalid_daliog();
                        }

                    } catch (ArrayIndexOutOfBoundsException | NumberFormatException | NullPointerException e) {
                        invalid_daliog();
                        e.printStackTrace();
                    }

                }
            } else {
                checkConnectivity();
            }

        });


        tv_number_vehicle.setOnClickListener(v -> {
            tv_number_vehicle.setText("");
            clear.setVisibility(View.GONE);
        });
        findViewById(R.id.searchinfo).setOnClickListener(v -> info_daliog());

        findViewById(R.id.back_main).setOnClickListener(view -> onBackPressed());

        findViewById(R.id.rate_us_info).setOnClickListener(view -> {
            if (sharedPreferences.getBoolean("ratePref", true)) {
                rate();
            } else {
                Toast.makeText(Main_Activity.this, "You have already rate this application!!", Toast.LENGTH_SHORT).show();
            }
        });
        findViewById(R.id.other_info_btn).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(Main_Activity.this, OtherInformation_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                checkConnectivity();
            }
        });

        findViewById(R.id.dldetail_info).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(Main_Activity.this, Search_DrivingLicence_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                try {
                    if (mInterstitialAd != null && !isActivityLeft) {
                        mInterstitialAd.show(Main_Activity.this);
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                checkConnectivity();
            }

        });
        findViewById(R.id.history_info).setOnClickListener(view -> {
            Intent intent = new Intent(new Intent(Main_Activity.this, History_Vehicle_Activity.class));
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        });

        findViewById(R.id.Car_info_btn).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(new Intent(Main_Activity.this, CarInformation_Activity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                try {
                    if (mInterstitialAd != null && !isActivityLeft) {
                        mInterstitialAd.show(Main_Activity.this);
                        MyApplication.appOpenManager.isAdShow = true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                checkConnectivity();
            }

        });

        findViewById(R.id.Bike_info_btn).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(new Intent(Main_Activity.this, BikeInformation_Activity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                checkConnectivity();
            }

        });


        findViewById(R.id.RTO_exam_btn).setOnClickListener(view -> {
            Intent intent = new Intent(new Intent(Main_Activity.this, ExamRTO_Activity.class));
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        });


        findViewById(R.id.Rto_traffic_sign_btn).setOnClickListener(view -> {
            Intent intent = new Intent(new Intent(Main_Activity.this, SymbolRTO_Activity.class));
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        });

        findViewById(R.id.Rto_all_details_btn).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(new Intent(Main_Activity.this, AllRTODetails_Activity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                try {
                    if (mInterstitialAd != null && !isActivityLeft) {
                        mInterstitialAd.show(Main_Activity.this);
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                checkConnectivity();
            }
        });
        findViewById(R.id.suggestion_info).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(new Intent(Main_Activity.this, Suggestion_Activity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                checkConnectivity();
            }


        });

        findViewById(R.id.inforc).setOnClickListener(view -> {
            Intent intent = new Intent(Main_Activity.this, RC_Info_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        });

        findViewById(R.id.infodl).setOnClickListener(view -> {
            Intent intent = new Intent(Main_Activity.this, DL_Info_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        });

        findViewById(R.id.tv_location).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(new Intent(Main_Activity.this, Fuel_activity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                checkConnectivity();
            }
        });
        findViewById(R.id.change_city).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(new Intent(Main_Activity.this, Fuel_activity.class));
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                checkConnectivity();
            }
        });
    }

    private void sendAndRequestResponse() {

        this.mRequestQueue.add(new JsonObjectRequest(0, Common_Utils.url_ip_com, null, jSONObject -> {
            try {
                city_name = jSONObject.getString(City_fragment.title);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, volleyError -> {
        }));
    }

    public void fuel() {
        if (PrefManager_fuel.getCity(this).equals("")) {
            this.mRequestQueue.add(new StringRequest(0, Common_Utils.Url_api, str -> {
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    jSONObject.get(NotificationCompat.CATEGORY_STATUS);
                    city_name = jSONObject.get(City_fragment.title).toString();
                    PrefManager_fuel.savecity(Main_Activity.this, jSONObject.get(City_fragment.title).toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }, volleyError -> {
            }) {
                public String getBodyContentType() {
                    return "application/json";
                }

                @SuppressLint("RestrictedApi")
                public Map<String, String> getHeaders() {
                    HashMap hashMap = new HashMap();
                    hashMap.put(HttpHeaderParser.HEADER_CONTENT_TYPE, "application/json");
                    return hashMap;
                }
            });
        }
        if (!PrefManager_fuel.getPetrol(this).equals("") || !PrefManager_fuel.getDiesel(this).equals("")) {
            setPrice(PrefManager_fuel.getPetrol(this), PrefManager_fuel.getDiesel(this), PrefManager_fuel.getCity(this));
            return;
        }
        getPrice();
    }

    public void getPrice() {
        this.mRequestQueue.add(new StringRequest(0, Common_Utils.newapi_fuel, str -> {
            try {
                JSONArray jSONArray = new JSONArray(new JSONObject(new JSONObject(str).get("prices").toString()).getString("fuel_prices"));
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    Model_City modelCity = new Model_City();
                    modelCity.setCity(jSONObject2.getString("CityName"));
                    modelCity.setId(Integer.valueOf(jSONObject2.getString("id")));
                    modelCity.setPrice(jSONObject2.getString("Petrol"));
                    modelCity.setYPrice(jSONObject2.getString("Diesel"));
                    modelCity.setGas(jSONObject2.getString("CNG"));
                    modelCity.setStateName(jSONObject2.getString("StateName"));
                    modelCity.setAutogas(jSONObject2.getString("AutoGas"));
                    modelCity.setCreatedAt(jSONObject2.getString("created"));
                    if (jSONObject2.getString("CityName").equals(city_name)) {
                        PrefManager_fuel.saveDiesel(Main_Activity.this, jSONObject2.getString("Diesel"));
                        PrefManager_fuel.savePetrol(Main_Activity.this, jSONObject2.getString("Petrol"));
                    }
                    model_cities_array.add(modelCity);
                }

                PrefManager_fuel.saveStateList(Main_Activity.this, model_cities_array, "State");
                setPrice(PrefManager_fuel.getPetrol(Main_Activity.this), PrefManager_fuel.getDiesel(Main_Activity.this), PrefManager_fuel.getCity(Main_Activity.this));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }, Throwable::getMessage) {
            public String getBodyContentType() {
                return "application/json";
            }

            @SuppressLint("RestrictedApi")
            public Map<String, String> getHeaders() {
                HashMap hashMap = new HashMap();
                hashMap.put("AuthorizationKey", MyApplication.MYSECRET);
                return hashMap;
            }
        });
    }

    @SuppressLint("WrongConstant")
    public void setPrice(String str, String str2, String str3) {
        this.text_petrol.setText(str);
        this.text_diesel.setText(str2);
        this.text_state.setText(str3);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityLeft = false;
        keyboard = false;
        if (city) {
            setPrice(PrefManager_fuel.getPetrol(this), PrefManager_fuel.getDiesel(this), PrefManager_fuel.getCity(this));
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission") NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void rate() {
        final Dialog dialog = new Dialog(Main_Activity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rating_dialog);
        RatingBar ratingbar = dialog.findViewById(R.id.ratingbar);

        dialog.findViewById(R.id.txt_notnow).setOnClickListener(v -> dialog.dismiss());

        dialog.findViewById(R.id.txt_submit).setOnClickListener(v -> {
            dialog.dismiss();
            if (ratingbar.getRating() <= 3) {
                Intent intent = new Intent(Main_Activity.this, Suggestion_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
                SharedPreferences.Editor editor1;
                editor1 = sharedPreferences.edit();
                editor1.putBoolean("ratePref", false);
                editor1.apply();
            }
        });
        dialog.show();
    }

    private void Most_trending_get() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_Vehicles_rto)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_MostTrending> call = service.Most_Trending(MyApplication.MYSECRET);
        call.enqueue(new Callback<Model_MostTrending>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<Model_MostTrending> call, @NonNull retrofit2.Response<Model_MostTrending> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            for (int i = 0; i < response.body().getRto_All_most().size(); i++) {
                                Most_trading_array.add(response.body().getRto_All_most().get(i));
                            }
                            findViewById(R.id.most_lin).setVisibility(View.VISIBLE);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(Main_Activity.this, LinearLayoutManager.HORIZONTAL, false);
                            recyclerView_most_trending.setLayoutManager(layoutManager);
                            recyclerView_most_trending.setAdapter(new MostTrendingPerson_Adapter(Main_Activity.this, Most_trading_array));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_MostTrending> call, @NonNull Throwable t) {
                findViewById(R.id.most_lin).setVisibility(View.GONE);
            }
        });
    }

    public void checkConnectivity() {
        if (!isOnline()) {
            try {
                Dialog nointernetDialog = new Dialog(Main_Activity.this);
                nointernetDialog.requestWindowFeature(1);
                nointernetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                nointernetDialog.setContentView(R.layout.no_connection_dialog);
                nointernetDialog.setCancelable(false);
                Button tryagain = nointernetDialog.findViewById(R.id.tryagain);
                tryagain.setOnClickListener(v -> {
                    nointernetDialog.dismiss();
                    finish();
                    startActivity(getIntent());
                });

                nointernetDialog.show();
                DialogInterface.OnKeyListener dialogWelcomeNavigationOnKey = (dialog, keyCode, event) -> {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        nointernetDialog.dismiss();
                        return true;
                    }
                    return false;
                };
                nointernetDialog.setOnKeyListener(dialogWelcomeNavigationOnKey);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("SetTextI18n")
    public void invalid_daliog() {
        Dialog info = new Dialog(Main_Activity.this);
        info.requestWindowFeature(1);
        info.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        info.setContentView(R.layout.invalid_registartion_dialog);
        Button tryagain = info.findViewById(R.id.ok);
        TextView textView = info.findViewById(R.id.getno);
        textView.setText("" + tv_number_vehicle.getText().toString() + " is invalid registration number. Registration number format e.g. GJ05AA0001, GJ5AA0001, ABC0001, DL1AAA1, DL1AAA0001.");

        tryagain.setOnClickListener(v1 -> info.dismiss());

        info.show();
        DialogInterface.OnKeyListener dialogWelcomeNavigationOnKey = (dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                info.dismiss();
                return true;
            }
            return false;
        };
        info.setOnKeyListener(dialogWelcomeNavigationOnKey);
    }

    public void info_daliog() {
        Dialog info = new Dialog(Main_Activity.this);
        info.requestWindowFeature(1);
        info.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        info.setContentView(R.layout.searchinginfo_dialog);
        Button tryagain = info.findViewById(R.id.ok);

        tryagain.setOnClickListener(v1 -> info.dismiss());

        info.show();
        DialogInterface.OnKeyListener dialogWelcomeNavigationOnKey = (dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                info.dismiss();
                return true;
            }
            return false;
        };
        info.setOnKeyListener(dialogWelcomeNavigationOnKey);
    }

    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                        LoadAdInterstitial();
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;
            }
        });
    }

    public void Banner() {
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);
        loadBanner();
    }

    private void loadBanner() {
        AdRequest adRequest =
                new AdRequest.Builder().build();

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                ads_space.setVisibility(View.GONE);
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                ads_space.setVisibility(View.GONE);
            }
        });
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActivityLeft = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActivityLeft = true;
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        if (keyboard) {
            keyboard = false;
            hideKeyboard(Main_Activity.this);
        } else {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActivityLeft = true;
    }
}