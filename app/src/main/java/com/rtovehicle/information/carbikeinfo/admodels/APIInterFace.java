package com.rtovehicle.information.carbikeinfo.admodels;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIInterFace {
    String adsAPI = "https://stage-ads.punchapp.in/api/";

    @FormUrlEncoded
    @POST("get-ads-list")
    Call<AdsModel> getads(@Field("app_id") int IntVal);
}