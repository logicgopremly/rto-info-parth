package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_CarVariantDetails {

    @SerializedName("status")
    boolean status;

    @SerializedName("data")
    @Expose
    ArrayList<Variant_Data> data;

    public boolean isStatus() {
        return status;
    }

    public ArrayList<Variant_Data> getData() {
        return data;
    }

    public static class Variant_Data {
        @SerializedName("id")
        int id;

        @SerializedName("engine")
        @Expose
        Engine engine;

        @SerializedName("fuel")
        @Expose
        Fuel fuel;

        @SerializedName("suspension")
        @Expose
        Suspension suspension;

        @SerializedName("dimension")
        @Expose
        Dimension dimension;

        @SerializedName("otherfeature")
        @Expose
        OtherFeatures otherfeatures;

        public int getId() {
            return id;
        }

        public Engine getEngine() {
            return engine;
        }

        public Fuel getFuel() {
            return fuel;
        }

        public Suspension getSuspension() {
            return suspension;
        }

        public Dimension getDimension() {
            return dimension;
        }

        public OtherFeatures getOtherfeatures() {
            return otherfeatures;
        }

        public static class Engine {
            @SerializedName("Engine Type")
            String engineType;

            @SerializedName("Fast Charging")
            String fastCharging;

            @SerializedName("Displacement (cc)")
            String displacement;

            @SerializedName("Max Power")
            String maxPower;

            @SerializedName("Max Torque")
            String maxTorque;

            @SerializedName("No. of cylinder")
            String noOfCylinder;

            @SerializedName("Valves Per Cylinder")
            String valvesPerCylinder;

            @SerializedName("Valve Configuration")
            String valveConfiguration;

            @SerializedName("Fuel Supply System")
            String fuelSupplySystem;

            @SerializedName("Turbo Charger")
            String turboCharger;

            @SerializedName("Super Charge")
            String superCharge;

            @SerializedName("TransmissionType")
            String transmissionType;

            @SerializedName("Gear Box")
            String gearBox;

            @SerializedName("Mild Hybrid")
            String mildHybrid;

            @SerializedName("Drive Type")
            String driveType;

            public String getEngineType() {
                return engineType;
            }

            public String getFastCharging() {
                return fastCharging;
            }

            public String getDisplacement() {
                return displacement;
            }

            public String getMaxPower() {
                return maxPower;
            }

            public String getMaxTorque() {
                return maxTorque;
            }

            public String getNoOfCylinder() {
                return noOfCylinder;
            }

            public String getValvesPerCylinder() {
                return valvesPerCylinder;
            }

            public String getValveConfiguration() {
                return valveConfiguration;
            }

            public String getFuelSupplySystem() {
                return fuelSupplySystem;
            }

            public String getTurboCharger() {
                return turboCharger;
            }

            public String getSuperCharge() {
                return superCharge;
            }

            public String getTransmissionType() {
                return transmissionType;
            }

            public String getGearBox() {
                return gearBox;
            }

            public String getMildHybrid() {
                return mildHybrid;
            }

            public String getDriveType() {
                return driveType;
            }
        }

        public static class Fuel {
            @SerializedName("Fuel Type")
            String fuelType;

            @SerializedName("Mileage (ARAI)")
            String mileage;

            @SerializedName("Fuel Tank Capacity (Litres)")
            String fuelTankCapacity;

            @SerializedName("Emission Norm Compliance")
            String emissionNormCompliance;

            @SerializedName("Top Speed (Kmph)")
            String topSpeed;

            public String getFuelType() {
                return fuelType;
            }

            public String getMileage() {
                return mileage;
            }

            public String getFuelTankCapacity() {
                return fuelTankCapacity;
            }

            public String getEmissionNormCompliance() {
                return emissionNormCompliance;
            }

            public String getTopSpeed() {
                return topSpeed;
            }
        }

        public static class Suspension {
            @SerializedName("Front Suspension")
            String frontSuspension;

            @SerializedName("Rear Suspension")
            String rearSuspension;

            @SerializedName("Steering Type")
            String steeringType;

            @SerializedName("Steering Column")
            String steeringColumn;

            @SerializedName("Steering Gear Type")
            String steeringGearType;

            @SerializedName("Turning Radius (Metres)")
            String turningRadius;

            @SerializedName("Front Brake Type")
            String frontBrakeType;

            @SerializedName("Rear Brake Type")
            String rearBrakeType;

            @SerializedName("Acceleration")
            String acceleration;

            @SerializedName("0-100kmph")
            String zeroToHundredkmph;

            public String getFrontSuspension() {
                return frontSuspension;
            }

            public String getRearSuspension() {
                return rearSuspension;
            }

            public String getSteeringType() {
                return steeringType;
            }

            public String getSteeringColumn() {
                return steeringColumn;
            }

            public String getSteeringGearType() {
                return steeringGearType;
            }

            public String getTurningRadius() {
                return turningRadius;
            }

            public String getFrontBrakeType() {
                return frontBrakeType;
            }

            public String getRearBrakeType() {
                return rearBrakeType;
            }

            public String getAcceleration() {
                return acceleration;
            }

            public String getZeroToHundredkmph() {
                return zeroToHundredkmph;
            }
        }

        public static class Dimension {
            @SerializedName("Length (mm)")
            String length;

            @SerializedName("Width (mm)")
            String width;

            @SerializedName("Height (mm)")
            String height;

            @SerializedName("Boot Space (Litres)")
            String bootSpace;

            @SerializedName("Seating Capacity")
            String seatingCapacity;

            @SerializedName("Ground Clearance Unladen (mm)")
            String groundClearanceUnladen;

            @SerializedName("Wheel Base (mm)")
            String wheelBase;

            @SerializedName("Front Tread (mm)")
            String frontTread;

            @SerializedName("Rear Tread (mm)")
            String rearTread;

            @SerializedName("Kerb Weight (Kg)")
            String kerbWeight;

            @SerializedName("Gross Weight (Kg)")
            String grossWeight;

            @SerializedName("No of Doors")
            String noOfDoors;

            public String getLength() {
                return length;
            }

            public String getWidth() {
                return width;
            }

            public String getHeight() {
                return height;
            }

            public String getBootSpace() {
                return bootSpace;
            }

            public String getSeatingCapacity() {
                return seatingCapacity;
            }

            public String getGroundClearanceUnladen() {
                return groundClearanceUnladen;
            }

            public String getWheelBase() {
                return wheelBase;
            }

            public String getFrontTread() {
                return frontTread;
            }

            public String getRearTread() {
                return rearTread;
            }

            public String getKerbWeight() {
                return kerbWeight;
            }

            public String getNoOfDoors() {
                return noOfDoors;
            }
        }

        public static class OtherFeatures {
            @SerializedName("Parking Sensors")
            String parkingSensors;

            @SerializedName("Foldable Rear Seat")
            String foldablerearSeat;

            @SerializedName("Trunk Opener")
            String trunkOpener;

            @SerializedName("Tyre Size")
            String tyreSize;

            @SerializedName("Tyre Type")
            String tyreType;

            @SerializedName("Wheel Size")
            String wheelSize;

            @SerializedName("Additional Features")
            String additionalFeatures;

            @SerializedName("No of Airbags")
            String noOfAirbags;

            @SerializedName("Advance Safety Features")
            String advanceSafetyFeatures;

            @SerializedName("Anti-Pinch Power Windows")
            String antiPinchPowerWindows;

            public String getParkingSensors() {
                return parkingSensors;
            }

            public String getFoldablerearSeat() {
                return foldablerearSeat;
            }

            public String getTrunkOpener() {
                return trunkOpener;
            }

            public String getTyreSize() {
                return tyreSize;
            }

            public String getTyreType() {
                return tyreType;
            }

            public String getWheelSize() {
                return wheelSize;
            }

            public String getAdditionalFeatures() {
                return additionalFeatures;
            }

            public String getNoOfAirbags() {
                return noOfAirbags;
            }

            public String getAdvanceSafetyFeatures() {
                return advanceSafetyFeatures;
            }

            public String getAntiPinchPowerWindows() {
                return antiPinchPowerWindows;
            }
        }
    }

}
