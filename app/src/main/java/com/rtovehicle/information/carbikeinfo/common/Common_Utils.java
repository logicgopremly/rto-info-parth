package com.rtovehicle.information.carbikeinfo.common;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.util.Base64;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.model.Model_ExamHistory;
import com.rtovehicle.information.carbikeinfo.model.Model_SuperVehicle;
import com.rtovehicle.information.carbikeinfo.model.Model_UpcomingVehicle;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

public class Common_Utils {

    public static final String BASE_URL = "https://rto.punchapp.in/";
    public static final String BASE_Vehicles_rto = "https://androoceans.com/";
    public static final String newapi_fuel = "https://androoceans.com/fuel-price2";
    public static final String Url_api = "http://ip-api.com/json/?fields=status,message,city,query";
    public static final String url_ip_com = "http://ip-api.com/json";
    public static Boolean search_Module = true;

    public static final String[] dlinfoname = {"Learner's DL", "Permanent DL", "Renewal of DL", "Duplicate DL", "Addition of Class", "International driving permit", "License Related fees"};
    public static final int[] dlimage = {R.drawable.learner_dl__rto_new, R.drawable.permnt_dl__rto_new, R.drawable.renewal_dl__rto_new, R.drawable.duplicate_dl__rto_new, R.drawable.adddition_calss_rto_new, R.drawable.international_vehicle_rto_new, R.drawable.fees_rto_new};
    public static final int[] ids = {1, 2, 3, 4, 5, 6, 7};

    public static final String[] rcinfoname = {"Temporary Registration", "Permanent Registration", "Renewal of Registration", "Duplicate RC", "No Objection Certificate", "HP Endorsement", "HP Termination", "Address Change", "Reassignment of vehicle", "Trade Certificate", "Duplicate Trade certificate", "Ownership Transfer", "Diplomatic Vehicles", "Registration Display"};
    public static final int[] rcimage = {R.drawable.tem_regi_rto_new, R.drawable.perment_regi_rto_new, R.drawable.renewal_regi_rto_new, R.drawable.duplicate_rc_rto_new, R.drawable.no_objection_certificate_rto_new, R.drawable.hp_endoresement_rto_new, R.drawable.hp_trmination_rto_new, R.drawable.add_change_rto_new, R.drawable.regi_vehicle_rto_new, R.drawable.certificate_rto_new, R.drawable.trade_certificate_rto_new, R.drawable.ownership_transfer_rto_new, R.drawable.diplomatic_vehicle_rto_new, R.drawable.registration_display_rto_new};
    public static final int[] rcids = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};

    public static int click_db;
    public static final int[] Eng_Arr = {R.drawable.sign164, R.drawable.sign109, R.drawable.sign43, R.drawable.sign37, R.drawable.img43, R.drawable.sign35, R.drawable.sign34, R.drawable.sign5, R.drawable.sign33, R.drawable.sign7, R.drawable.sign6, R.drawable.sign8, R.drawable.sign13, R.drawable.sign72, R.drawable.sign27, R.drawable.sign28, R.drawable.sign26, R.drawable.sign25, R.drawable.sign67, R.drawable.sign3, R.drawable.sign18, R.drawable.sign17, R.drawable.sign11, R.drawable.sign10, R.drawable.sign22, R.drawable.sign12, R.drawable.sign2, R.drawable.sign30, R.drawable.sign41, R.drawable.sign32, R.drawable.sign29, R.drawable.sign75, R.drawable.sign69, R.drawable.sign44, R.drawable.sign45, R.drawable.sign65, R.drawable.sign14, R.drawable.sign66, R.drawable.sign31, R.drawable.sign129, R.drawable.sign70, R.drawable.sign46, R.drawable.sign9, R.drawable.sign165, R.drawable.sign166, R.drawable.sign167, R.drawable.sign24, R.drawable.sign4, R.drawable.sign1, R.drawable.sign19, R.drawable.sign74, R.drawable.sign73, R.drawable.sign40, R.drawable.sign15, R.drawable.sign69, R.drawable.sign42, R.drawable.sign50, R.drawable.sign49, R.drawable.sign36, R.drawable.sign16, R.drawable.sign64, R.drawable.sign48, R.drawable.sign51, R.drawable.sign47, R.drawable.sign39, R.drawable.sign52, R.drawable.sign56, R.drawable.sign55, R.drawable.sign54, R.drawable.sign53, R.drawable.sign40, R.drawable.sign21, R.drawable.sign20};
    public static final int[] Guj_Arr = {R.drawable.sign164, R.drawable.sign163, R.drawable.sign43, R.drawable.sign37, R.drawable.sign38, R.drawable.sign35, R.drawable.sign34, R.drawable.sign5, R.drawable.sign33, R.drawable.sign7, R.drawable.sign6, R.drawable.sign8, R.drawable.sign13, R.drawable.sign72, R.drawable.sign27, R.drawable.sign28, R.drawable.sign26, R.drawable.sign25, R.drawable.sign67, R.drawable.sign3, R.drawable.sign18, R.drawable.sign17, R.drawable.sign11, R.drawable.sign10, R.drawable.sign22, R.drawable.sign12, R.drawable.sign2, R.drawable.sign30, R.drawable.sign41, R.drawable.sign32, R.drawable.sign29, R.drawable.sign75, R.drawable.sign76, R.drawable.sign44, R.drawable.sign45, R.drawable.sign65, R.drawable.sign14, R.drawable.sign66, R.drawable.sign31, R.drawable.sign129, R.drawable.sign70, R.drawable.sign46, R.drawable.sign9, R.drawable.sign165, R.drawable.sign166, R.drawable.sign167, R.drawable.sign24, R.drawable.sign4, R.drawable.sign1, R.drawable.sign19, R.drawable.sign74, R.drawable.sign73, R.drawable.sign40, R.drawable.sign15, R.drawable.sign42, R.drawable.sign50, R.drawable.sign49, R.drawable.sign36, R.drawable.sign16, R.drawable.sign64, R.drawable.sign48, R.drawable.sign51, R.drawable.sign47, R.drawable.sign39, R.drawable.sign52, R.drawable.sign56, R.drawable.sign55, R.drawable.sign54, R.drawable.sign21, R.drawable.sign20, R.drawable.sign40, R.drawable.sign21, R.drawable.sign20};

    public static final int[] Hindi_Arr = {R.drawable.sign163, R.drawable.sign65, R.drawable.sign165, R.drawable.sign76, R.drawable.sign3, R.drawable.sign1, R.drawable.sign2, R.drawable.sign67, R.drawable.sign5, R.drawable.sign66, R.drawable.sign78, R.drawable.sign74, R.drawable.sign6, R.drawable.sign7, R.drawable.sign9, R.drawable.sign10, R.drawable.sign166, R.drawable.sign70, R.drawable.sign12, R.drawable.sign31, R.drawable.sign64, R.drawable.sign4, R.drawable.sign16, R.drawable.sign18, R.drawable.sign20, R.drawable.sign21, R.drawable.sign15, R.drawable.sign14, R.drawable.sign11, R.drawable.sign164, R.drawable.sign167, R.drawable.sign24, R.drawable.sign27, R.drawable.sign13, R.drawable.sign118, R.drawable.sign32, R.drawable.sign33, R.drawable.sign8, R.drawable.sign35, R.drawable.sign34, R.drawable.sign29, R.drawable.sign22, R.drawable.sign36, R.drawable.sign37, R.drawable.sign38, R.drawable.sign39, R.drawable.sign40, R.drawable.sign41, R.drawable.sign42, R.drawable.sign45, R.drawable.sign7, R.drawable.sign47, R.drawable.sign48, R.drawable.sign49, R.drawable.sign50, R.drawable.sign75, R.drawable.sign52, R.drawable.sign53};
    public static final int[] Marathi_Arr = {R.drawable.sign163, R.drawable.sign16, R.drawable.sign65, R.drawable.sign165, R.drawable.sign76, R.drawable.sign3, R.drawable.sign1, R.drawable.sign2, R.drawable.sign67, R.drawable.sign4, R.drawable.sign5, R.drawable.sign66, R.drawable.sign78, R.drawable.sign74, R.drawable.sign6, R.drawable.sign7, R.drawable.sign79, R.drawable.sign9, R.drawable.sign10, R.drawable.sign166, R.drawable.sign70, R.drawable.sign12, R.drawable.sign80, R.drawable.sign64, R.drawable.sign81, R.drawable.sign17, R.drawable.sign19, R.drawable.sign20, R.drawable.sign21, R.drawable.sign14, R.drawable.sign15, R.drawable.sign11, R.drawable.sign164, R.drawable.sign167, R.drawable.sign24, R.drawable.sign25, R.drawable.sign26, R.drawable.sign27, R.drawable.sign28, R.drawable.sign84, R.drawable.sign13, R.drawable.sign32, R.drawable.sign33, R.drawable.sign73, R.drawable.sign35, R.drawable.sign34, R.drawable.sign29, R.drawable.sign22, R.drawable.sign36, R.drawable.sign38, R.drawable.sign39, R.drawable.sign40, R.drawable.sign41, R.drawable.sign42, R.drawable.sign46, R.drawable.sign47, R.drawable.sign48, R.drawable.sign49, R.drawable.sign50, R.drawable.sign75, R.drawable.sign52, R.drawable.sign53, R.drawable.sign55, R.drawable.sign54, R.drawable.sign109, R.drawable.sign89, R.drawable.sign69, R.drawable.sign69, R.drawable.sign89, R.drawable.sign90, R.drawable.sign92, R.drawable.sign93, R.drawable.sign94, R.drawable.sign95, R.drawable.sign129, R.drawable.sign72, R.drawable.sign96, R.drawable.sign73, R.drawable.sign56, R.drawable.sign43};

    public static ArrayList<Model_ExamHistory> eng_history_list = new ArrayList<>();
    public static final String[] english_arr = {"Stop", "Give way", "Straight prohibited", "One way", "One way", "Vehicles prohibited in both directions", " Horn prohibited", "Pedestrians prohibited", "Cycles prohibited", "Right turn prohibited", "Left turn prohibited", " U-turn prohibited", "Overtaking prohibited", "Trucks prohibited", "Tonga prohibited", "All motor vehicles prohibited", "Hand cart prohibited", "Bullock and hand carts prohibited", "No parking", "No parking or stopping", " Speed limit", "Axle load limit", "Width limit", "Weight limit", "Length limit", "Load limit", "Restriction ends", "Turn left", "Turn Right", "Ahead or turn left", "Ahead or turn right", "Ahead", "Keep left", "Sound horn", "Buses only ", "Right hand curve", "Left hand curve", "Right hand pin bend", "Left hand pin bend", "Right reverse bend", "Narrow bridge", "Gap in median", "Cycle crossing", "Pedestrian crossing", "School ahead", "Men at work", "Roundabout", "Narrow road", "Road widens", "Side road left", "Side road right", "Major road", "Major road", "Staggered intersection", "Staggered intersection", "Y-intersection", "Y-intersection", "Y-intersection", "T-intersection", "Reduced carriageway", "Reduced carriageway", "Two way", "Cross road", "Truck lay bay", "Toll booth ahead", "Parking this side", "Parking both sides", "Scooter and motor cycle stand", "Taxi stand", " Auto-rickshaw stand", "Public telephone", "Filling station", "Hospital", "First aid post", "Eating place", "Light refreshment", "No through road", "No through side road", "Resting place", "Pedestrian subway", "Repair facility"};

    public static ArrayList<Model_ExamHistory> guj_history_list = new ArrayList<>();
    public static final String[] gujarati_arr = {"બંધ", "રસ્તો આપો", "સીધા પ્રતિબંધિત", "એક રસ્તો", "એક રસ્તો", "વાહન બંને દિશામાં પ્રતિબંધિત છે", "હોર્ન પ્રતિબંધિત", "પદયાત્રીઓ પ્રતિબંધિત છે", "સાયકલ્સ પ્રતિબંધિત છે", "જમણી વળાંક પ્રતિબંધિત", "ડાબી વળાંક પ્રતિબંધિત", "યુ-ટર્ન પ્રતિબંધિત છે", "ઓવરટેકિંગ પ્રતિબંધિત", "ટ્રક પ્રતિબંધિત", "ટોંગા પ્રતિબંધિત", "બધા મોટર વાહનો પ્રતિબંધિત છે", "હાથગાડી પ્રતિબંધિત", "બળદ અને હાથગાડી પ્રતિબંધિત", "પાર્કિંગ નથી", "પાર્કિંગ અથવા બંધ નથી", "ગતિ મર્યાદા", "ધરી ભાર સીમા", "પહોળાઈ સીમા", "વજન મર્યાદા", "લંબાઈ મર્યાદા", "ભાર મર્યાદા", "પ્રતિબંધ સમાપ્ત થાય છે", "ડાબે વળો", "જમણી બાજુ વળો", "આગળ અથવા ડાબે વળો", "આગળ અથવા જમણે વળો", "આગળ", "ડાબી રાખો", "ધ્વનિ શિંગડા", "ફક્ત બસો", "જમણા હાથ વળાંક", "ડાબા હાથ વળાંક", "જમણો હાથ પિન બેન્ડ", "ડાબા હાથની પિન બેન્ડ", "જમણો વિપરીત વળાંક", "સંક્ષિપ્ત પુલ", "મધ્યમાં ખાલી જગ્યા", "સાઇકલ ફાટક", "રાહદારી ફાટક", "આગળ શાળા", "કાર્ય ચાલુ છે", "ચાર રસ્તા", "સંક્ષિપ્ત માર્ગ", "રોડ પહોળું થાય છે", "સાઇડ રોડ ડાબે બાજુ", "સાઇડ રોડ જમણી બાજુ", "મુખ્ય રસ્તો", "મુખ્ય રસ્તો", "વિલંબિત આંતરછેદ", "વિલંબિત આંતરછેદ", "Y-આંતરછેદ", "Y-આંતરછેદ", "Y-આંતરછેદ", "T-આંતરછેદ", "ઘટાડાના વાહન", "ઘટાડાના વાહન", "બે દિશામાં જતું આવતું", "ક્રોસ રોડ", "ટ્રક ની જગ્યા", "આગળ ટૉલ બૂથ છે", "આ બાજુ પાર્કિંગ", "બંને પક્ષો પાર્કિંગ", "સ્કૂટર અને મોટર સાયકલ સ્ટેન્ડ", "ટેક્સી સ્ટેન્ડ", "ઓટો રીક્ષા સ્ટેન્ડ", "જાહેર ટેલિફોન", "ભરવાનું સ્થાન", "હોસ્પિટલ", "પ્રથમ સહાય પોસ્ટ", "ખાવા માટે ની જગ્યા", "સહેજ રિફ્રેશમેન્ટ", "માર્ગ દ્વારા નહીં", "બાજુ માર્ગ દ્વારા નહીં", "આરામ સ્થળ", "પેડેસ્ટ્રિયન સબવે", "સમારકામ સુવિધા"};
    public static final String[] hindi_arr = {"रुकें", "रास्ता दें", "सीधे प्रतिबंधित", "एक रास्ता", "एक रास्ता", "वाहन दोनों दिशाओं में निषिद्ध", "हॉर्न प्रतिबंधित", "पैदल चलने वालों ने निषिद्ध", "साइकिल निषिद्ध", "दाईं ओर निषिद्ध है", "बाईं ओर निषिद्ध है", "यू-टर्न निषिद्ध है", "अतिदेय निषिद्ध", "ट्रकों को निषिद्ध", "टोंगा निषिद्ध", "सभी मोटर वाहनों को निषिद्ध", "हाथ गाड़ी निषिद्ध", "बैल और हाथ की गाड़ियां निषिद्ध", "पार्किंग नहीं", "कोई पार्किंग या रोक नहीं है", "गति सीमा", "धुरा भार सीमा", "चौड़ाई सीमा", "वजन की सीमा", "लम्बाई की सीमा", "भार सीमा", "प्रतिबंध समाप्त होता है", "बाएं मुड़ें", "दायें मुड़ो", "आगे या बाएं मुड़ो  ", "आगे या दायें मुड़ो", "आगे", "बाए रखे", "ध्वनि सींग", "केवल बसें", "दायां हाथ वक्र", "बाएं हाथ वक्र", "दायां हाथ पिन मोड़", "बाएं हाथ पिन बेंड", "दाहिने उल्टा मोड़", "तंग पुल", "मध्य में अवकाश", "साइकिल चौराहा", "पैदल यात्री चौराहा", "आगे स्कूल", "काम चालू है", "चार सड़कों", "संकरी सड़क", "सड़क चौड़ा", "साइड रोड बाएं ओर ", "साइड रोड दाईं ओर", "मुख्य सड़क", "मुख्य सड़क", "स्थगित चौराहे", "स्थगित चौराहे", "Y-चौराहे", "Y-चौराहे", "Y-चौराहे", "T-चौराहे", "कम कैरेजवे", "कम कैरेजवे", "दो तरह से सड़क", "चौराहा", "ट्रक रखना बे", "टोल बूथ आगे", "इस तरफ पार्किंग करें", "दोनों तरफ पार्किंग", "स्कूटर और मोटर साइकिल स्टैंड", "टैक्सी स्टैंड", "ऑटो-रिक्शा स्टैंड", "सार्वजनिक टेलीफोन", "भरने का ठिकाना", "अस्पताल", "प्राथमिक चिकित्सा पद", "खाने की जगह", " हल्का जलपान", "सड़क के रास्ते नहीं", "किनारे से नहीं", "शांत स्थान", "पैदल यात्री सबवे", "मरम्मत सुविधा"};
    public static ArrayList<Model_ExamHistory> hindi_history_list = new ArrayList<>();

    public static final String key_eng_history_list = "key_eng_history_list";
    public static final String key_guj_history_list = "key_guj_history_list";
    public static final String key_hindi_history_list = "key_hindi_history_list";
    public static final String key_marathi_history_list = "key_marathi_history_list";
    public static final String[] marathi_arr = {"थांबवा", "मार्ग द्या", "सरळ प्रतिबंधित", "एकेरि मार्ग", "एकेरि मार्ग", "वाहने दोन्ही दिशानिर्देशांमध्ये प्रतिबंधित आहेत", "हॉर्नला मनाई आहे", "पादचार्यांसाठी प्रतिबंधित", "चक्र प्रतिबंधित", "उजवे वळण प्रतिबंधित", "डावीकडे वळण प्रतिबंधित केले", "यू टर्न प्रतिबंधित", "अतिप्रतिबंध प्रतिबंधित", "ट्रक प्रतिबंधित केले", "टोंगा प्रतिबंधित आहे", "सर्व मोटर वाहनांना निषिद्ध आहे", "हातांची बंदी घालण्यात आली", "बैल आणि हात गाड्या निषिद्ध", "गाडी उभी करण्यास मनाई आहे", "पार्किंग नाही किंवा थांबत नाही", "वेग मर्यादा", "अक्ष भार मर्यादा", "रुंदीची मर्यादा", "वजन मर्यादा", "लांबी मर्यादा", "भार मर्यादा", "प्रतिबंध समाप्त", "डावीकडे वळा", "उजवीकडे वळा", "पुढे किंवा डावीकडे वळा", "पुढे किंवा उजवीकडे वळवा", "पुढे", "डावीकडे ठेवा", "ध्वनी शिंग", "केवळ बस", "उजवा हात वक्र", "डावा हात कर्व्ह", "उजव्या हाताचा पिन बेंड", "डावा हात पिन बेंड", "उजवीकडे उलटा वाकणे", "अरुंद पूल", "असणारा अंतर", "सायकल ओलांडणे", "पादचारी ओलांडणे", "पुढे शाळा", "कार्य चालू आहे", "चौक", "संकुचित रस्ता", "रस्ता चौधरी", "बाजूला रस्ता डाव्या बाजूला", "साइड रस्ता उजव्या बाजूला", "मुख्य रस्ता", "मुख्य रस्ता", "विलंबित चौकट", "विलंबित चौकट", "Y-प्रतिच्छेदन", "Y-प्रतिच्छेदन", "Y-प्रतिच्छेदन", "T-प्रतिच्छेदन", "कमी वेरोपण", "कमी वेरोपण", "दोन मार्ग", "क्रॉस रोड", "ट्रकची जागा", "पुढे टोल बूथ", "या बाजूला पार्किंग करा", "पार्किंग दोन्ही बाजूंनी", "स्कूटर आणि मोटर सायकल स्टँड", "टॅक्सी स्टॅन्ड", "ऑटो रिक्शा स्टँड", "सार्वजनिक टेलिफोन", "स्टेशन भरणे", "रूग्णालय", "प्रथमोपचार पद", "स्थान खाणे", "हलका रिफ्रेशमेंट", "रस्त्यामार्गे नाही", "बाजूच्या रस्त्याद्वारे नाही", "आरामाची जागा", "पादचारी भुयारी मार्ग", "दुरुस्ती सुविधा"};
    public static ArrayList<Model_ExamHistory> marathi_history_list = new ArrayList<>();
    public static ArrayList<Model_UpcomingVehicle.Upcoming_Data> upcominglist = new ArrayList<>();
    public static ArrayList<Model_SuperVehicle.Super_Data> superlist = new ArrayList<>();

    public static final String DL_COV = "Class Of Vehicle";
    public static final String DL_COV_CAT = "COV Category";
    public static final String DL_COV_ISS = "COV Issue Date";
    public static final String DL_CUR_STA = "Current Status";
    public static final String DL_DOB = "Date of Birth";
    public static final String DL_DOI = "Date Of Issue";
    public static final String DL_HAZ_VAL = "Hazardous Valid Till";
    public static final String DL_HIL_VAL = "Hill Valid Till";
    public static final String DL_LAS_TRA = "Last Transaction At";
    public static final String DL_NAM = "Name";
    public static final String DL_NON_TRA = "Non-Transport Fr";
    public static final String DL_NON_TRA_TO = "Non-Transport To";
    public static final String DL_NUM = "DL Number";
    public static final String DL_TRA = "Transport From";
    public static final String DL_TRA_TO = "Transport To";
    public static String src1;
    public static String src2;
    public static String src3;
    public static String src4;
    public static String src5;
    public static String src6;
    public static String src7;
    public static String src8;
    public final Activity mActivity;
    private final Context mContext;

    public Common_Utils(Context context, Activity activity) {
        this.mContext = context;
        this.mActivity = activity;
        src1 = Bd(context.getString(R.string.test1));
        src2 = Bd(context.getString(R.string.test2));
        src3 = Bd(context.getString(R.string.test3));
        src4 = Bd(context.getString(R.string.test4));
        src5 = Bd(context.getString(R.string.test5));
        src6 = Bd(context.getString(R.string.test6));
        src7 = Bd(context.getString(R.string.test7));
        src8 = Bd(context.getString(R.string.test8));
    }

    public boolean isOnline() {
        @SuppressLint("WrongConstant") NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public void checkConnectivity() {
        if (!isOnline()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
            builder.setTitle("No Internet Connection");
            builder.setMessage("Please check your mobile internet connection and then try again.");
            builder.setPositiveButton("OK", (dialogInterface, i) -> mActivity.finish());
            builder.show();
        }
    }


    @SuppressLint("HardwareIds")
    public HashMap<String, List<String>> getInfo() {
        String str;
        String str2 = "FF";
        HashMap<String, List<String>> hashMap = new HashMap<>();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        hashMap.put("ti", Collections.singletonList(simpleDateFormat.format(new Date())));
        hashMap.put("pkg", Collections.singletonList(this.mContext.getPackageName()));
        try {
            str = Integer.toString(this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0).versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            str = str2;
        }
        hashMap.put("vc", Collections.singletonList(str));
        hashMap.put("did", Collections.singletonList(Settings.Secure.getString(this.mContext.getContentResolver(), "android_id")));
        hashMap.put("ui", Collections.singletonList("y"));
        try {
            str2 = this.mContext.getPackageManager().getPackageInfo(this.mContext.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        hashMap.put("vn", Collections.singletonList(str2));
        hashMap.put("kv", Collections.singletonList(Build.VERSION.RELEASE));
        hashMap.put("pm", Collections.singletonList(Build.MODEL));
        hashMap.put("mf", Collections.singletonList(Build.MANUFACTURER));
        hashMap.put("tz", Collections.singletonList(TimeZone.getDefault().getID()));
        hashMap.put("ph", Collections.singletonList("0"));
        hashMap.put("em", Collections.singletonList("-"));
        return hashMap;
    }

    public static String Bd(String str) {
        return new String(Base64.decode(str, 0));
    }

    public void Log(String str) {
        int i = 0;
        while (i < str.length()) {
            i = i + 2048;
        }
    }
}
