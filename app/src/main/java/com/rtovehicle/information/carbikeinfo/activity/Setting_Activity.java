package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.BuildConfig;
import com.rtovehicle.information.carbikeinfo.R;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public class Setting_Activity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    VersionChecker versionChecker;
    String latestVersion;
    ImageView update;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_setting);
        getWindow().setBackgroundDrawable(null);
        sharedPreferences = getSharedPreferences("rating", Context.MODE_PRIVATE);
        versionChecker = new VersionChecker();
        ((TextView) findViewById(R.id.version)).setText(BuildConfig.VERSION_NAME);
        update = findViewById(R.id.update_show);
        new Handler().postDelayed(() -> {
            try {
                latestVersion = versionChecker.execute().get();
                if (BuildConfig.VERSION_NAME.equals(latestVersion)) {
                    update.setImageResource(R.drawable.updated_yes);
                    ((TextView) findViewById(R.id.txt_no_update)).setText(getString(R.string.no_update_available));
                } else {
                    update.setImageResource(R.drawable.update_no);
                    ((TextView) findViewById(R.id.txt_no_update)).setText(getString(R.string.no_update_available1));
                }
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }, 10);
        click();
    }


    public void click() {
        findViewById(R.id.back_all).setOnClickListener(view -> Setting_Activity.this.onBackPressed());

        findViewById(R.id.how_to_use).setOnClickListener(view -> startActivity(new Intent(Setting_Activity.this, HowtoUse_Activity.class)));

        findViewById(R.id.update_setting).setOnClickListener(view -> {


            if (BuildConfig.VERSION_NAME.equals(latestVersion)) {
                Toast.makeText(Setting_Activity.this, "APP already updated", Toast.LENGTH_SHORT).show();
            } else {
                try {
                    Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (Exception ignored) {
                }
            }


        });


        findViewById(R.id.share_setting).setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.SEND");
            String sb = "" +
                    getString(R.string.app_name);
            intent.putExtra("android.intent.extra.SUBJECT", sb);
            String sb2 = "Its awesome,Check out this app! \n\n" +
                    getString(R.string.app_name) +
                    "\n\n Download it for free at \n\nhttps://play.google.com/store/apps/details?id=" +
                    getPackageName();
            intent.putExtra("android.intent.extra.TEXT", sb2);
            intent.setType("text/plain");
            startActivity(intent);
        });

        findViewById(R.id.feedback_setting).setOnClickListener(view -> {
            Intent intent = new Intent(Setting_Activity.this, Suggestion_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        });

        findViewById(R.id.rate_setting).setOnClickListener(view -> {

            if (sharedPreferences.getBoolean("ratePref", true)) {
                rate();
            } else {
                Toast.makeText(Setting_Activity.this, "You have already rate this application!!", Toast.LENGTH_SHORT).show();
            }

        });
        findViewById(R.id.privacypolicy_setting).setOnClickListener(view -> {
            if (isOnline()) {
                Intent intent = new Intent(Setting_Activity.this, Privacy_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                Toast.makeText(Setting_Activity.this, "You are not connected to Internet", Toast.LENGTH_SHORT).show();
            }
        });
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission") NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void rate() {
        final Dialog dialog = new Dialog(Setting_Activity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rating_dialog);
        RatingBar ratingbar = dialog.findViewById(R.id.ratingbar);

        dialog.findViewById(R.id.txt_notnow).setOnClickListener(v -> dialog.dismiss());

        dialog.findViewById(R.id.txt_submit).setOnClickListener(v -> {
            dialog.dismiss();
            if (ratingbar.getRating() <= 3) {
                Intent intent = new Intent(Setting_Activity.this, Suggestion_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
                SharedPreferences.Editor editor1;
                editor1 = sharedPreferences.edit();
                editor1.putBoolean("ratePref", false);
                editor1.apply();
            }
        });
        dialog.show();
    }

    @SuppressLint("StaticFieldLeak")
    public class VersionChecker extends AsyncTask<String, String, String> {

        private String newVersion;

        @Override
        protected String doInBackground(String... params) {

            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select(".hAyfc .htlgb")
                        .get(7)
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return newVersion;
        }
    }

    @SuppressLint("WrongConstant")
    public void onResume() {
        super.onResume();
    }

}
