package com.rtovehicle.information.carbikeinfo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.rtovehicle.information.carbikeinfo.R;

import java.util.Objects;

public class ViewImagePager_Adapter extends PagerAdapter {
    final Context context;
    final String[] carImages;
    final String image_url;
    final LayoutInflater mLayoutInflater;

    public ViewImagePager_Adapter(Context context, String[] carImages, String image_url) {
        this.context = context;
        this.carImages = carImages;
        this.image_url = image_url;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return carImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.item_car_image, container, false);

        ShapeableImageView imageView = itemView.findViewById(R.id.image_car_main);
        imageView.setShapeAppearanceModel(
                new ShapeAppearanceModel.Builder()
                        .setAllCorners(CornerFamily.ROUNDED, 8)
                        .build());

        Glide.with(context)
                .load(image_url + "/" + carImages[position])
                .centerCrop()
                .into(imageView);

        Objects.requireNonNull(container).addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {

        container.removeView((LinearLayout) object);
    }
}
