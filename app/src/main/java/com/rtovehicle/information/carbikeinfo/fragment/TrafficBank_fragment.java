package com.rtovehicle.information.carbikeinfo.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.QuestionRTOBank_Activity;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

public class TrafficBank_fragment extends Fragment {
    myAdapter adapter;
    ListView list;
    final int[] traffic_sign = {R.drawable.sign163, R.drawable.sign165, R.drawable.sign166, R.drawable.sign81, R.drawable.sign76, R.drawable.sign69, R.drawable.sign4, R.drawable.sign19, R.drawable.sign90, R.drawable.sign67, R.drawable.sign20, R.drawable.sign3, R.drawable.sign9, R.drawable.sign17, R.drawable.sign18, R.drawable.sign16, R.drawable.sign162, R.drawable.sign89, R.drawable.sign64, R.drawable.sign14, R.drawable.sign21, R.drawable.sign30, R.drawable.sign82, R.drawable.sign83, R.drawable.sign91, R.drawable.sign94, R.drawable.sign6, R.drawable.sign65, R.drawable.sign80, R.drawable.sign11, R.drawable.sign22, R.drawable.sign15, R.drawable.sign164, R.drawable.sign12, R.drawable.sign92, R.drawable.sign167, R.drawable.sign24, R.drawable.sign25, R.drawable.sign26, R.drawable.sign28, R.drawable.sign5, R.drawable.sign47, R.drawable.sign38, R.drawable.sign1, R.drawable.sign40, R.drawable.sign41, R.drawable.sign32, R.drawable.sign7, R.drawable.sign46, R.drawable.sign70, R.drawable.sign13, R.drawable.sign118, R.drawable.sign72, R.drawable.sign71, R.drawable.sign129, R.drawable.sign34, R.drawable.sign35, R.drawable.sign29, R.drawable.sign120, R.drawable.warning_road_narrowing_right, R.drawable.warning_road_narrowing_left, R.drawable.two_way_traffic_straight_ahead, R.drawable.sign10, R.drawable.truck_lay_bay, R.drawable.toll_boot_ahead, R.drawable.sign2, R.drawable.sign52, R.drawable.sign53, R.drawable.sign55, R.drawable.sign54, R.drawable.sign161, R.drawable.sign56, R.drawable.sign66, R.drawable.sign78, R.drawable.sign160, R.drawable.sign43, R.drawable.sign75, R.drawable.sign50, R.drawable.sign74, R.drawable.pedestrian_subway, R.drawable.repair_facility};

    class myAdapter extends BaseAdapter {
        final Context context;
        final String[] data;

        public long getItemId(int i) {
            return i;
        }

        public myAdapter(Context context2) {
            this.context = context2;
            if (QuestionRTOBank_Activity.language_str.equalsIgnoreCase("english")) {
                this.data = Common_Utils.english_arr;
            } else if (QuestionRTOBank_Activity.language_str.equalsIgnoreCase("gujarati")) {
                this.data = Common_Utils.gujarati_arr;
            } else if (QuestionRTOBank_Activity.language_str.equalsIgnoreCase("hindi")) {
                this.data = Common_Utils.hindi_arr;
            } else {
                this.data = Common_Utils.marathi_arr;
            }
        }

        public int getCount() {
            return TrafficBank_fragment.this.traffic_sign.length;
        }

        public Object getItem(int i) {
            return TrafficBank_fragment.this.traffic_sign[i];
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(this.context).inflate(R.layout.traffic_sign_item, viewGroup, false);
            }
            TextView textView = view.findViewById(R.id.count);
            ImageView imageView = view.findViewById(R.id.sign);
            ((TextView) view.findViewById(R.id.name)).setText(this.data[i]);
            imageView.setImageResource(TrafficBank_fragment.this.traffic_sign[i]);
            textView.setText(String.valueOf(i + 1));
            return view;
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.trafficbank_fragment, viewGroup, false);
        this.list = inflate.findViewById(R.id.listview);
        this.adapter = new myAdapter(requireContext());
        this.list.setAdapter(this.adapter);
        return inflate;
    }
}
