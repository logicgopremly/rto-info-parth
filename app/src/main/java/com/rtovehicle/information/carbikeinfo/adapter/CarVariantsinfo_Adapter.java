package com.rtovehicle.information.carbikeinfo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.CarVariantInformation_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_CarVariant;

import java.util.ArrayList;

public class CarVariantsinfo_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final Context context;
    final ArrayList<Model_CarVariant> varientsList;

    public static final String VARIANT_NAME = "variant_name";
    public static final String VARIANT_ID = "variant_id";

    public CarVariantsinfo_Adapter(Context context, ArrayList<Model_CarVariant> varientsList) {
        this.context = context;
        this.varientsList = varientsList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_varients_list_car, parent, false);
        return new VarientsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Model_CarVariant carVariant = varientsList.get(position);

        VarientsHolder varientsHolder = (VarientsHolder) holder;
        varientsHolder.model_name_textview.setText(carVariant.getVariant_name());
        varientsHolder.car_showroom_price_textview.setText(carVariant.getVariant_price());
        varientsHolder.car_transmission_textview.setText(carVariant.getTransmission_type());
        varientsHolder.car_fuel_type_textview.setText(carVariant.getFuel_type());
        varientsHolder.car_engine_disp_textview.setText(carVariant.getEngine_disp());

        varientsHolder.card_varient_textview.setOnClickListener(v -> {
            Intent intent = new Intent(context, CarVariantInformation_Activity.class);
            intent.putExtra(VARIANT_NAME, carVariant.getVariant_name());
            intent.putExtra(VARIANT_ID, carVariant.getId());
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return varientsList.size();
    }

    public static class VarientsHolder extends RecyclerView.ViewHolder {
        final TextView model_name_textview;
        final TextView car_showroom_price_textview;
        final TextView car_transmission_textview;
        final TextView car_fuel_type_textview;
        final TextView car_engine_disp_textview;
        final LinearLayout card_varient_textview;

        public VarientsHolder(@NonNull View itemView) {
            super(itemView);
            card_varient_textview = itemView.findViewById(R.id.card_varient);
            model_name_textview = itemView.findViewById(R.id.model_name);
            car_showroom_price_textview = itemView.findViewById(R.id.car_showroom_price);
            car_transmission_textview = itemView.findViewById(R.id.car_transmission);
            car_fuel_type_textview = itemView.findViewById(R.id.car_fuel_type);
            car_engine_disp_textview = itemView.findViewById(R.id.car_engine_disp);
        }
    }

}
