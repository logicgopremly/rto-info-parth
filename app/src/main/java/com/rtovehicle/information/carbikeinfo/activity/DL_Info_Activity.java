package com.rtovehicle.information.carbikeinfo.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.Dl_info_Adapter;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

public class DL_Info_Activity extends AppCompatActivity {
    RecyclerView information;
    Dl_info_Adapter deformationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dl_info);
        getWindow().setBackgroundDrawable(null);


        information = findViewById(R.id.dlinformation);
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        information.setLayoutManager(new GridLayoutManager(this, 3));
        deformationAdapter = new Dl_info_Adapter(this, Common_Utils.dlinfoname, Common_Utils.dlimage, (view, i) -> substring2(Common_Utils.ids[i], Common_Utils.dlinfoname[i]));
        this.information.setAdapter(deformationAdapter);
    }


    public void substring2(int a, String str) {
        Intent intent = new Intent(this, DL_InfoWEB_Activity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("number", a);
        intent.putExtra("name", str);
        startActivity(intent);
    }
}
