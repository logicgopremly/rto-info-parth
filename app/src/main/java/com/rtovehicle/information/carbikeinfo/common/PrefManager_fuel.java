package com.rtovehicle.information.carbikeinfo.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.rtovehicle.information.carbikeinfo.model.Model_City;
import com.mukesh.tinydb.TinyDB;

import java.util.ArrayList;
import java.util.List;

public class PrefManager_fuel {

    private static final String CITY = "city_id";
    private static final String DIESEL = "diesel_id";
    private static final String PETROL = "petrol_id";
    private static final String SHARED_PREFS_NAME = "pref_nofication";

    public static void savePetrol(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences(SHARED_PREFS_NAME, 0).edit();
        edit.putString(PETROL, str);
        edit.apply();
    }

    public static String getPetrol(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, 0).getString(PETROL, "");
    }

    public static void saveDiesel(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences(SHARED_PREFS_NAME, 0).edit();
        edit.putString(DIESEL, str);
        edit.apply();
    }

    public static String getDiesel(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, 0).getString(DIESEL, "");
    }

    public static void savecity(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences(SHARED_PREFS_NAME, 0).edit();
        edit.putString(CITY, str);
        edit.apply();
    }

    public static String getCity(Context context) {
        return context.getSharedPreferences(SHARED_PREFS_NAME, 0).getString(CITY, "");
    }


    public static void saveStateList(Context context, List<Model_City> list, String str) {
        ArrayList arrayList = new ArrayList<>();
        arrayList.addAll(list);
        new TinyDB(context).putListObject(str, arrayList);
    }

    public static List<Model_City> getSateLsit(Context context, String str) {
        ArrayList<Object> listObject = new TinyDB(context).getListObject(str, Model_City.class);
        ArrayList arrayList = new ArrayList<>();
        for (Object o : listObject) {
            arrayList.add(o);
        }
        return arrayList;
    }

}
