package com.rtovehicle.information.carbikeinfo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.model.Model_ColorVariant;

import java.util.ArrayList;

public class ColorVarients_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final Context context;
    final ArrayList<Model_ColorVariant> colorsList;

    public ColorVarients_Adapter(Context context, ArrayList<Model_ColorVariant> colorsList) {
        this.context = context;
        this.colorsList = colorsList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_color_list, parent, false);
        return new ColorsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Model_ColorVariant colorVarient = colorsList.get(position);
        ColorsHolder colorsHolder = (ColorsHolder) holder;
        try {
            try {
                colorsHolder.color_image.setBackgroundColor(Color.parseColor(colorVarient.getHexCode()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return colorsList.size();
    }

    public static class ColorsHolder extends RecyclerView.ViewHolder {
        final ImageView color_image;

        public ColorsHolder(@NonNull View itemView) {
            super(itemView);
            color_image = itemView.findViewById(R.id.color_image);
        }
    }
}
