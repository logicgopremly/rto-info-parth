package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;


public class DL_InfoWEB_Activity extends AppCompatActivity {
    int check;
    private FrameLayout adContainerView;
    NativeAd nativeAds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dl_infoweb);
        getWindow().setBackgroundDrawable(null);
        findid();
        NativeadsLoad();
        loadweb();
    }

    public void findid() {
        adContainerView = findViewById(R.id.adsframe);
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
    }

    public void loadweb() {
        check = getIntent().getIntExtra("number", 0);
        String name = getIntent().getStringExtra("name");
        TextView texts = findViewById(R.id.texts);
        texts.setText(name);

        WebView webView = findViewById(R.id.web_view);

        if (check == 1) {
            webView.loadUrl("file:///android_asset/dl1.html");
        }
        if (check == 2) {
            webView.loadUrl("file:///android_asset/dl2.html");

        }
        if (check == 3) {
            webView.loadUrl("file:///android_asset/dl3.html");
        }
        if (check == 4) {
            webView.loadUrl("file:///android_asset/dl4.html");
        }
        if (check == 5) {
            webView.loadUrl("file:///android_asset/dl5.html");
        }
        if (check == 6) {
            webView.loadUrl("file:///android_asset/dl6.html");
        }
        if (check == 7) {
            webView.loadUrl("file:///android_asset/dl7.html");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private void NativeadsLoad() {

        AdLoader.Builder builder = new AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
                .forNativeAd(nativeAd -> {

                    nativeAds = nativeAd;

                    @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) getLayoutInflater()
                            .inflate(R.layout.small_native, null);
                    populateUnifiedNativeAdView(nativeAd, adView);
                    adContainerView.removeAllViews();
                    adContainerView.addView(adView);

                });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull @NotNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();

        adLoader.loadAd(new AdRequest.Builder().build());


    }


    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (nativeAds != null) {
            nativeAds.destroy();
        }
    }
}
