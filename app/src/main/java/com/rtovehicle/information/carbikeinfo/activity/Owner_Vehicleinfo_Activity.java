package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.core.content.FileProvider;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rtovehicle.information.carbikeinfo.BuildConfig;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rtovehicle.information.carbikeinfo.common.DataBaseHelper;
import com.rtovehicle.information.carbikeinfo.common.LoadAnimation;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_Vehicle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Owner_Vehicleinfo_Activity extends AppCompatActivity {


    TextView no_data_one;
    TextView no_data_two;
    TextView text_Retry;
    LinearLayout linearLayout_main;
    LinearLayout linearLayout_progress;
    TextView text_rc_registered_at;
    TextView text_ChassisNoValue;
    TextView text_EngineNoValue;
    TextView text_FitnessUptoValue;
    TextView text_FuelTypeValue;
    TextView text_InsuranceUptoValue;
    TextView text_MakerModelValue;
    TextView text_MakerdescModelValue;
    TextView text_OwnerNameValue;
    TextView text_RegDateValue;
    TextView text_RegistrationNoValue;
    TextView text_VehicleClassValue;
    RelativeLayout relative_makerModel;
    RelativeLayout rate_us_relative;
    RelativeLayout relative_owner;
    RelativeLayout relative_no_bike;
    RelativeLayout relative_regdate;
    RelativeLayout relative_makerDescModel;
    RelativeLayout relative_engineNo;
    RelativeLayout relative_chassisNo;
    RelativeLayout relative_fueltype;
    RelativeLayout relative_vehicleClass;
    RelativeLayout relative_insuranceUpto;
    RelativeLayout star;

    View errorContainer;
    DataBaseHelper dataHelper;
    RatingBar ratingbar;
    SharedPreferences sharedPreferences;


    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_owner_vehicleinfo);
        getWindow().setBackgroundDrawable(null);

        String registrationNo = getIntent().getStringExtra("vehical_number");

        sharedPreferences = getSharedPreferences("rating", Context.MODE_PRIVATE);
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());

        dataHelper = new DataBaseHelper(this);
        linearLayout_main = findViewById(R.id.contentLayout);
        linearLayout_progress = findViewById(R.id.lyt_progress);
        text_OwnerNameValue = findViewById(R.id.ownerNameValue);
        text_MakerModelValue = findViewById(R.id.makerModelValue);
        text_MakerdescModelValue = findViewById(R.id.makerdescModelValue);
        text_RegDateValue = findViewById(R.id.regDateValue);
        text_FuelTypeValue = findViewById(R.id.fuelTypeValue);
        text_VehicleClassValue = findViewById(R.id.vehicleClassValue);
        this.errorContainer = findViewById(R.id.errorContainer);
        text_EngineNoValue = findViewById(R.id.engineNoValue);
        text_ChassisNoValue = findViewById(R.id.chassisNoValue);
        text_InsuranceUptoValue = findViewById(R.id.insuranceUptoValue);
        relative_makerModel = findViewById(R.id.rel_makerModel);
        relative_makerDescModel = findViewById(R.id.rel_makerDescModel);
        rate_us_relative = findViewById(R.id.rate_us_rel);
        star = findViewById(R.id.rate_us_rel1);
        text_Retry = findViewById(R.id.retry);
        no_data_one = findViewById(R.id.nodata1);
        no_data_two = findViewById(R.id.nodata2);
        text_FitnessUptoValue = findViewById(R.id.fitnessUptoValue);
        ratingbar = findViewById(R.id.ratingbar);
        text_RegistrationNoValue = findViewById(R.id.registrationNoValue);
        relative_owner = findViewById(R.id.rel_owner);
        relative_no_bike = findViewById(R.id.rel_no_bike);
        relative_regdate = findViewById(R.id.rel_regdate);
        relative_engineNo = findViewById(R.id.rel_engineNo);
        relative_chassisNo = findViewById(R.id.rel_chassisNo);
        relative_fueltype = findViewById(R.id.rel_fueltype);
        relative_vehicleClass = findViewById(R.id.rel_vehicleClass);
        relative_insuranceUpto = findViewById(R.id.rel_insuranceUpto);


        ratingbar.setOnRatingBarChangeListener((ratingBar, v, b) -> {
            if (ratingBar.getRating() <= 3) {
                Intent intent = new Intent(Owner_Vehicleinfo_Activity.this, Suggestion_Activity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)));
                SharedPreferences.Editor editor1;
                editor1 = sharedPreferences.edit();
                editor1.putBoolean("ratePref", false);
                editor1.apply();
                rate_us_relative.setVisibility(View.GONE);
            }

        });


        text_rc_registered_at = findViewById(R.id.rc_registered_at);
        findViewById(R.id.share_details).setOnClickListener(view -> {
            if (text_RegistrationNoValue.getText().toString().isEmpty()) {
                Toast.makeText(Owner_Vehicleinfo_Activity.this, "Details Not Found ", Toast.LENGTH_SHORT).show();
            } else {
                MenuBuilder menuBuilder = new MenuBuilder(this);
                MenuInflater inflater = new MenuInflater(this);
                inflater.inflate(R.menu.pop_menu, menuBuilder);
                MenuPopupHelper optionsMenu = new MenuPopupHelper(this, menuBuilder, view);
                optionsMenu.setForceShowIcon(true);
                menuBuilder.setCallback(new MenuBuilder.Callback() {
                    @SuppressLint("NonConstantResourceId")
                    @Override
                    public boolean onMenuItemSelected(@NonNull MenuBuilder menu, @NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.one:
                                function_shareData("Vehicle Registration Details: \n\n Registration No : " + (text_RegistrationNoValue.getText()) + "\n Owner Name : " + (text_OwnerNameValue.getText()) + "\n Maker Model : " + (text_MakerModelValue.getText()) + "\n Maker Desc : " + (text_MakerdescModelValue.getText()) + "\n Registration Date : " + (text_RegDateValue.getText()) + "\n Vehicle Class : " + (text_VehicleClassValue.getText()) + "\n Vehicle Insured Upto : " + (text_InsuranceUptoValue.getText()) + "\n Chassis No : " + (text_ChassisNoValue.getText()) + "\n Engine No : " + (text_EngineNoValue.getText()) + "\n Fuel Type : " + (text_FuelTypeValue.getText()) + "\n MoreDetails : https://play.google.com/store/apps/details?id=" + getPackageName());
                                return true;
                            case R.id.two:
                                Dexter.withContext(Owner_Vehicleinfo_Activity.this).withPermissions("android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE").withListener(new MultiplePermissionsListener() {
                                    public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                                        if (multiplePermissionsReport.areAllPermissionsGranted()) {
                                            File file = saveBitMap(Owner_Vehicleinfo_Activity.this, linearLayout_main);    //which view you want to pass that view as parameter
                                            if (file != null) {
                                                Uri uri = FileProvider.getUriForFile(Owner_Vehicleinfo_Activity.this, "com.rtovehicle.information.carbikeinfo.provider", file);
                                                Intent shareIntent = new Intent();
                                                shareIntent.setAction(Intent.ACTION_SEND);
                                                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                                                shareIntent.setType("image/jpeg");
                                                startActivity(Intent.createChooser(shareIntent, "Share"));
                                            } else {
                                                Toast.makeText(Owner_Vehicleinfo_Activity.this, "Details Not found", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                        if (multiplePermissionsReport.isAnyPermissionPermanentlyDenied()) {
                                            showSettingDialog(Owner_Vehicleinfo_Activity.this);
                                        }
                                    }

                                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {
                                        permissionToken.continuePermissionRequest();
                                    }
                                }).withErrorListener(dexterError -> Toast.makeText(Owner_Vehicleinfo_Activity.this, "Error occurred! ", Toast.LENGTH_SHORT).show()).onSameThread().check();


                                return true;
                            default:
                                return false;
                        }
                    }

                    @Override
                    public void onMenuModeChange(@NonNull MenuBuilder menu) {
                    }
                });
                optionsMenu.show();

            }
        });

        text_Retry.setOnClickListener(view -> onBackPressed());


        if (Common_Utils.click_db == 1) {
            if (!getIntent().getStringExtra("db_vehicle").equals("")) {
                relative_owner.setVisibility(View.VISIBLE);
                text_OwnerNameValue.setText(getIntent().getStringExtra("db_vehicle"));
            } else {
                relative_owner.setVisibility(View.GONE);
            }
            if (!getIntent().getStringExtra("db_vehicle1").equals("")) {
                relative_no_bike.setVisibility(View.VISIBLE);
                text_RegistrationNoValue.setText(getIntent().getStringExtra("db_vehicle1"));
            } else {
                relative_no_bike.setVisibility(View.GONE);
            }
            if (!getIntent().getStringExtra("db_vehicle2").equals("")) {
                relative_regdate.setVisibility(View.VISIBLE);
                text_RegDateValue.setText(getIntent().getStringExtra("db_vehicle2"));
            } else {
                relative_regdate.setVisibility(View.GONE);
            }
            if (!getIntent().getStringExtra("db_vehicle3").equals("")) {
                relative_makerDescModel.setVisibility(View.VISIBLE);
                text_MakerdescModelValue.setText(getIntent().getStringExtra("db_vehicle3"));
            } else {
                relative_makerDescModel.setVisibility(View.GONE);
            }
            if (!getIntent().getStringExtra("db_vehicle4").equals("")) {
                relative_makerModel.setVisibility(View.VISIBLE);
                text_MakerModelValue.setText(getIntent().getStringExtra("db_vehicle4"));
            } else {
                relative_makerModel.setVisibility(View.GONE);
            }
            if (!getIntent().getStringExtra("db_vehicle5").equals("")) {
                relative_engineNo.setVisibility(View.VISIBLE);
                text_EngineNoValue.setText(getIntent().getStringExtra("db_vehicle5"));
            } else {
                relative_engineNo.setVisibility(View.GONE);
            }

            if (!getIntent().getStringExtra("db_vehicle6").equals("")) {
                relative_chassisNo.setVisibility(View.VISIBLE);
                text_ChassisNoValue.setText(getIntent().getStringExtra("db_vehicle6"));
            } else {
                relative_chassisNo.setVisibility(View.GONE);
            }

            if (!getIntent().getStringExtra("db_vehicle7").equals("")) {
                relative_fueltype.setVisibility(View.VISIBLE);
                text_FuelTypeValue.setText(getIntent().getStringExtra("db_vehicle7"));
            } else {
                relative_fueltype.setVisibility(View.GONE);
            }
            if (!getIntent().getStringExtra("db_vehicle8").equals("")) {
                relative_vehicleClass.setVisibility(View.VISIBLE);
                text_VehicleClassValue.setText(getIntent().getStringExtra("db_vehicle8"));
            } else {
                relative_vehicleClass.setVisibility(View.GONE);
            }
            if (!getIntent().getStringExtra("db_vehicle9").equals("")) {
                relative_insuranceUpto.setVisibility(View.VISIBLE);
                text_InsuranceUptoValue.setText(getIntent().getStringExtra("db_vehicle9"));
            } else {
                relative_insuranceUpto.setVisibility(View.GONE);
            }
            text_FitnessUptoValue.setText(getIntent().getStringExtra("db_vehicle10"));
            new Handler().postDelayed(() -> LoadAnimation.fadeOut(linearLayout_progress), 200);
            linearLayout_main.setVisibility(View.VISIBLE);
            errorContainer.setVisibility(View.GONE);
        }

        if (Common_Utils.click_db == 2) {
            getVehicle_number(registrationNo);
        }

        if (Common_Utils.click_db == 3) {
            registrationNo = getIntent().getStringExtra("famous");
            getVehicle_number(registrationNo);
        }

    }

    private void getVehicle_number(String vehicle_number) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_Vehicles_rto)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_Vehicle> call = service.getVehicle_api(MyApplication.MYSECRET, vehicle_number);
        call.enqueue(new Callback<Model_Vehicle>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<Model_Vehicle> call, @NonNull retrofit2.Response<Model_Vehicle> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getBike_vehicle() != null) {
                                if (response.body().getBike_vehicle().getStatus() == 200) {
                                    if (response.body().getBike_vehicle().getRc_owner_name() != null) {
                                        relative_owner.setVisibility(View.VISIBLE);
                                        text_OwnerNameValue.setText(response.body().getBike_vehicle().getRc_owner_name());
                                    } else {
                                        relative_owner.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_regn_no() != null) {
                                        relative_no_bike.setVisibility(View.VISIBLE);
                                        text_RegistrationNoValue.setText(response.body().getBike_vehicle().getRc_regn_no());
                                    } else {
                                        relative_no_bike.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_regn_dt() != null) {
                                        relative_regdate.setVisibility(View.VISIBLE);
                                        text_RegDateValue.setText(response.body().getBike_vehicle().getRc_regn_dt());
                                    } else {
                                        relative_regdate.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_maker_desc() != null) {
                                        relative_makerDescModel.setVisibility(View.VISIBLE);
                                        text_MakerdescModelValue.setText(response.body().getBike_vehicle().getRc_maker_desc());
                                    } else {
                                        relative_makerDescModel.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_maker_model() != null) {
                                        relative_makerModel.setVisibility(View.VISIBLE);
                                        text_MakerModelValue.setText(response.body().getBike_vehicle().getRc_maker_model());
                                    } else {
                                        relative_makerModel.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_eng_no() != null) {
                                        relative_engineNo.setVisibility(View.VISIBLE);
                                        text_EngineNoValue.setText(response.body().getBike_vehicle().getRc_eng_no());
                                    } else {
                                        relative_engineNo.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_chasi_no() != null) {
                                        relative_chassisNo.setVisibility(View.VISIBLE);
                                        text_ChassisNoValue.setText(response.body().getBike_vehicle().getRc_chasi_no());
                                    } else {
                                        relative_chassisNo.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_fuel_desc() != null) {
                                        relative_fueltype.setVisibility(View.VISIBLE);
                                        text_FuelTypeValue.setText(response.body().getBike_vehicle().getRc_fuel_desc());
                                    } else {
                                        relative_fueltype.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_vh_class_desc() != null) {
                                        relative_vehicleClass.setVisibility(View.VISIBLE);
                                        text_VehicleClassValue.setText(response.body().getBike_vehicle().getRc_vh_class_desc());
                                    } else {
                                        relative_vehicleClass.setVisibility(View.GONE);
                                    }
                                    if (response.body().getBike_vehicle().getRc_insurance_upto() != null) {
                                        relative_insuranceUpto.setVisibility(View.VISIBLE);
                                        text_InsuranceUptoValue.setText(response.body().getBike_vehicle().getRc_insurance_upto());
                                    } else {
                                        relative_insuranceUpto.setVisibility(View.GONE);
                                    }
                                    text_FitnessUptoValue.setText(response.body().getBike_vehicle().getRc_fit_upto());
                                    text_rc_registered_at.setText(response.body().getBike_vehicle().getRc_registered_at());

                                    new Handler().postDelayed(() -> LoadAnimation.fadeOut(linearLayout_progress), 200);
                                    linearLayout_main.setVisibility(View.VISIBLE);
                                    star.setVisibility(View.VISIBLE);
                                    errorContainer.setVisibility(View.GONE);
                                    dataHelper.Insert_vehicle(response.body().getBike_vehicle().getDatafrom(), text_OwnerNameValue.getText().toString(), text_RegistrationNoValue.getText().toString(), text_MakerdescModelValue.getText().toString(), text_MakerModelValue.getText().toString(), text_RegDateValue.getText().toString(), text_FuelTypeValue.getText().toString(), text_VehicleClassValue.getText().toString(), text_rc_registered_at.getText().toString(), text_EngineNoValue.getText().toString(), text_ChassisNoValue.getText().toString(), text_InsuranceUptoValue.getText().toString(), text_FitnessUptoValue.getText().toString(), response.body().getBike_vehicle().getRc_maker_desc());
                                } else {
                                    new Handler().postDelayed(() -> LoadAnimation.fadeOut(linearLayout_progress), 200);
                                    errorContainer.setVisibility(View.VISIBLE);
                                    star.setVisibility(View.GONE);
                                    linearLayout_main.setVisibility(View.GONE);
                                    no_data_one.setText("We can not found vehicle number " + vehicle_number + " from the RTO. Because of :");
                                    no_data_two.setText("* You entered the number of the vehicle is Wrong. Check if " + vehicle_number + " is right, please.");
                                }
                            } else {
                                new Handler().postDelayed(() -> LoadAnimation.fadeOut(linearLayout_progress), 200);
                                errorContainer.setVisibility(View.VISIBLE);
                                linearLayout_main.setVisibility(View.GONE);
                                star.setVisibility(View.GONE);
                                no_data_one.setText("We can not found vehicle number " + vehicle_number + " from the RTO. Because of :");
                                no_data_two.setText("* You entered the number of the vehicle is Wrong. Check if " + vehicle_number + " is right, please.");
                            }
                        } else {
                            new Handler().postDelayed(() -> LoadAnimation.fadeOut(linearLayout_progress), 200);
                            errorContainer.setVisibility(View.VISIBLE);
                            linearLayout_main.setVisibility(View.GONE);
                            star.setVisibility(View.GONE);
                            no_data_one.setText("We can not found vehicle number " + vehicle_number + " from the RTO. Because of :");
                            no_data_two.setText("* You entered the number of the vehicle is Wrong. Check if " + vehicle_number + " is right, please.");
                        }

                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<Model_Vehicle> call, @NonNull Throwable t) {

            }
        });
    }

    @SuppressLint("WrongConstant")
    public void function_shareData(String shareBody) {
        Intent sharingIntent = new Intent("android.intent.action.SEND");
        sharingIntent.addFlags(335544320);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra("android.intent.extra.SUBJECT", "Vehicle Info");
        sharingIntent.putExtra("android.intent.extra.TEXT", shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share with:"));
    }


    private File saveBitMap(Context context, View drawView) {
        File pictureFileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), getString(R.string.app_name));
        if (!pictureFileDir.exists()) {
            boolean isDirectoryCreated = pictureFileDir.mkdirs();
            if (!isDirectoryCreated)
                return null;
        }
        String filename = pictureFileDir.getPath() + File.separator + System.currentTimeMillis() + ".jpg";
        File pictureFile = new File(filename);
        Bitmap bitmap = getBitmapFromView(drawView);
        try {
            pictureFile.createNewFile();
            FileOutputStream oStream = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
            oStream.flush();
            oStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        scanGallery(context, pictureFile.getAbsolutePath());
        return pictureFile;
    }

    private Bitmap getBitmapFromView(View view) {
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            bgDrawable.draw(canvas);
        } else {
            canvas.drawColor(Color.WHITE);
        }
        view.draw(canvas);
        return returnedBitmap;
    }

    private void scanGallery(Context cntx, String path) {
        try {
            MediaScannerConnection.scanFile(cntx, new String[]{path}, null, (path1, uri) -> {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedPreferences.getBoolean("ratePref", true)) {
            rate_us_relative.setVisibility(View.VISIBLE);
        } else {
            rate_us_relative.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public static void showSettingDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GO SETTINGS", (dialogInterface, i) -> {
            dialogInterface.cancel();
            openSettings(activity);
        });
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
        builder.show();
    }

    public static void openSettings(Activity activity) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", BuildConfig.APPLICATION_ID, null));
        activity.startActivityForResult(intent, 101);
    }
}
