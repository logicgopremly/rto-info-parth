package com.rtovehicle.information.carbikeinfo.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.rtovehicle.information.carbikeinfo.model.Model_ExamHistory;

import java.util.ArrayList;
import java.util.Arrays;

public class DBTiny {

    private final SharedPreferences preferences;

    public DBTiny(Context context) {
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }


    public ArrayList<String> getListString(String str) {
        return new ArrayList<>(Arrays.asList(TextUtils.split(this.preferences.getString(str, ""), "‚‗‚")));
    }


    public ArrayList<Model_ExamHistory> getAdListObject(String str) {
        Gson gson = new Gson();
        ArrayList listString = getListString(str);
        ArrayList<Model_ExamHistory> arrayList = new ArrayList<>();
        for (Object o : listString) {
            arrayList.add(gson.fromJson((String) o, Model_ExamHistory.class));
        }
        return arrayList;
    }


    public void putListString(String str, ArrayList<String> arrayList) {
        checkForNullKey(str);
        this.preferences.edit().putString(str, TextUtils.join("‚‗‚", arrayList.toArray(new String[0]))).apply();
    }


    public void putAdListObject(String str, ArrayList<Model_ExamHistory> arrayList) {
        checkForNullKey(str);
        Gson gson = new Gson();
        ArrayList arrayList2 = new ArrayList<>();
        for (Model_ExamHistory rto_examHistoryModel : arrayList) {
            arrayList2.add(gson.toJson(rto_examHistoryModel));
        }
        putListString(str, arrayList2);
    }


    public void checkForNullKey(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
    }

}
