package com.rtovehicle.information.carbikeinfo.activity;

import android.content.res.AssetManager;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener;
import com.google.android.material.tabs.TabLayout.Tab;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.fragment.QuestionsBank_fragment;
import com.rtovehicle.information.carbikeinfo.fragment.TrafficBank_fragment;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class QuestionRTOBank_Activity extends AppCompatActivity implements OnTabSelectedListener {
    public static String language_str;
    public static String questionbank_str;
    String[] head_array;
    TextView header;
    public TabLayout tabLayout;
    String[] tabtxt_array;
    private ViewPager viewPager;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_questionrto_bank);
        getWindow().setBackgroundDrawable(null);
        this.header = findViewById(R.id.id_header);
        findViewById(R.id.back_all).setOnClickListener(view -> QuestionRTOBank_Activity.this.onBackPressed());
        String str = "lang";
        String str2 = "english";
        if (getIntent().getStringExtra(str).equalsIgnoreCase(str2)) {
            questionbank_str = loadJSONFromAsset(str2);
            language_str = str2;
            this.tabtxt_array = getResources().getStringArray(R.array.tab_english);
            this.head_array = getResources().getStringArray(R.array.qustion_page_english);
        } else {
            String str3 = "gujarati";
            if (getIntent().getStringExtra(str).equalsIgnoreCase(str3)) {
                language_str = str3;
                questionbank_str = loadJSONFromAsset(str3);
                this.tabtxt_array = getResources().getStringArray(R.array.tab_gujarati);
                this.head_array = getResources().getStringArray(R.array.qustion_page_gujarati);
            } else {
                String stringExtra = getIntent().getStringExtra(str);
                String str4 = "hindi";
                if (stringExtra.equalsIgnoreCase(str4)) {
                    language_str = str4;
                    questionbank_str = loadJSONFromAsset(str4);
                    this.tabtxt_array = getResources().getStringArray(R.array.tab_hindi);
                    this.head_array = getResources().getStringArray(R.array.qustion_page_hindi);
                } else {
                    String str5 = "marathi";
                    language_str = str5;
                    questionbank_str = loadJSONFromAsset(str5);
                    this.tabtxt_array = getResources().getStringArray(R.array.tab_marathi);
                    this.head_array = getResources().getStringArray(R.array.qustion_page_marathi);
                }
            }
        }
        this.header.setText(this.head_array[0]);
        this.tabLayout = findViewById(R.id.tabLayout);
        TabLayout tabLayout2 = this.tabLayout;
        tabLayout2.addTab(tabLayout2.newTab().setText(this.tabtxt_array[0]));
        TabLayout tabLayout3 = this.tabLayout;
        tabLayout3.addTab(tabLayout3.newTab().setText(this.tabtxt_array[1]));
        this.tabLayout.setTabGravity(0);
        this.viewPager = findViewById(R.id.pager);
        this.viewPager.setAdapter(new TabsPagerAdapter(getSupportFragmentManager()));
        this.tabLayout.setOnTabSelectedListener(this);
        this.viewPager.setOnPageChangeListener(new OnPageChangeListener() {
            public void onPageScrollStateChanged(int i) {
            }

            public void onPageScrolled(int i, float f, int i2) {
            }

            public void onPageSelected(int i) {
                Objects.requireNonNull(QuestionRTOBank_Activity.this.tabLayout.getTabAt(i)).select();
            }
        });
    }

    public static class TabsPagerAdapter extends FragmentStatePagerAdapter {
        public int getCount() {
            return 2;
        }

        public TabsPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @NonNull
        public Fragment getItem(int i) {
            if (i == 0) {
                return new QuestionsBank_fragment();
            }
            if (i != 1) {
                return null;
            }
            return new TrafficBank_fragment();
        }
    }

    public void onTabReselected(Tab tab) {
    }

    public void onTabUnselected(Tab tab) {
    }


    public void onTabSelected(Tab tab) {
        this.viewPager.setCurrentItem(tab.getPosition());
    }

    public String loadJSONFromAsset(String str) {
        try {
            AssetManager assets = getAssets();
            String sb = str + ".json";
            InputStream open = assets.open(sb);
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            return new String(bArr, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void onResume() {
        super.onResume();

    }

    public void onPause() {
        super.onPause();
    }
}
