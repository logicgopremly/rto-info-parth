package com.rtovehicle.information.carbikeinfo.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.QuestionRTOBank_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_QuestionBank;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class QuestionsBank_fragment extends Fragment {
    myAdapter adapter_my;
    ListView list;
    final ArrayList<Model_QuestionBank> questionBankModels = new ArrayList<>();

    class myAdapter extends BaseAdapter {
        final String[] ans;
        final String ans_lang;
        final Context context;
        final String[] que;
        final String que_lang;
        final ArrayList<Model_QuestionBank> questionBankModels;
        final int[] signn;

        public long getItemId(int i) {
            return i;
        }

        public myAdapter(Context context2, ArrayList<Model_QuestionBank> arrayList) {
            this.context = context2;
            this.questionBankModels = arrayList;
            this.que = QuestionsBank_fragment.this.getResources().getStringArray(R.array.que);
            this.ans = QuestionsBank_fragment.this.getResources().getStringArray(R.array.ans);
            if (QuestionRTOBank_Activity.language_str.equalsIgnoreCase("english")) {
                this.signn = Common_Utils.Eng_Arr;
                this.ans_lang = this.ans[0];
                this.que_lang = this.que[0];
            } else if (QuestionRTOBank_Activity.language_str.equalsIgnoreCase("gujarati")) {
                this.signn = Common_Utils.Guj_Arr;
                this.ans_lang = this.ans[1];
                this.que_lang = this.que[1];
            } else if (QuestionRTOBank_Activity.language_str.equalsIgnoreCase("hindi")) {
                this.signn = Common_Utils.Hindi_Arr;
                this.ans_lang = this.ans[2];
                this.que_lang = this.que[2];
            } else {
                this.signn = Common_Utils.Marathi_Arr;
                this.ans_lang = this.ans[2];
                this.que_lang = this.que[2];
            }
        }

        public int getCount() {
            return this.questionBankModels.size();
        }

        public Object getItem(int i) {
            return this.questionBankModels.get(i);
        }

        @SuppressLint("WrongConstant")
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(this.context).inflate(R.layout.questionbank_item, viewGroup, false);
            }
            ImageView imageView = view.findViewById(R.id.sign);
            TextView textView = view.findViewById(R.id.que);
            TextView textView2 = view.findViewById(R.id.question);
            TextView textView3 = view.findViewById(R.id.ans);
            TextView textView4 = view.findViewById(R.id.answer);
            int i2 = i + 1;
            String sb = this.que_lang +
                    "" +
                    i2;
            textView.setText(sb);
            textView3.setText(this.ans_lang);
            Model_QuestionBank questionBankModel = this.questionBankModels.get(i);
            if (questionBankModel.getIsimage().equalsIgnoreCase("1")) {
                imageView.setVisibility(0);
                try {
                    imageView.setImageResource(this.signn[i]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                imageView.setVisibility(8);
            }
            textView2.setText(questionBankModel.getQuestion());
            textView4.setText(questionBankModel.getAnswer());
            return view;
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.questionsbank_fragment, viewGroup, false);
        this.list = inflate.findViewById(R.id.listview);
        try {
            JSONArray jSONArray = new JSONObject(QuestionRTOBank_Activity.questionbank_str).getJSONArray("data");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                Model_QuestionBank questionBankModel = new Model_QuestionBank();
                questionBankModel.setAnswer(jSONObject.getString("Answer"));
                questionBankModel.setQuestion(jSONObject.getString("Question"));
                questionBankModel.setIsimage(jSONObject.getString("Isimage"));
                this.questionBankModels.add(questionBankModel);
            }
            if (this.questionBankModels.size() != 0) {
                this.adapter_my = new myAdapter(requireContext(), this.questionBankModels);
                this.list.setAdapter(this.adapter_my);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return inflate;
    }
}
