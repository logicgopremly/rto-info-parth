package com.rtovehicle.information.carbikeinfo.model;

public class Model_ExamHistory {
    int attend_que;
    int correct_ans;
    String date;
    int pass;
    String reuslt;
    int wrong_ans;

    public int getPass() {
        return this.pass;
    }

    public void setPass(int i) {
        this.pass = i;
    }


    public String getDate() {
        return this.date;
    }

    public void setDate(String str) {
        this.date = str;
    }

    public String getReuslt() {
        return this.reuslt;
    }

    public void setReuslt(String str) {
        this.reuslt = str;
    }

    public int getCorrect_ans() {
        return this.correct_ans;
    }

    public void setCorrect_ans(int i) {
        this.correct_ans = i;
    }

    public int getWrong_ans() {
        return this.wrong_ans;
    }

    public void setWrong_ans(int i) {
        this.wrong_ans = i;
    }

    public int getAttend_que() {
        return this.attend_que;
    }

    public void setAttend_que(int i) {
        this.attend_que = i;
    }
}
