package com.rtovehicle.information.carbikeinfo.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.Pager_Adapter;
import com.rtovehicle.information.carbikeinfo.fragment.Super_vehicles_fragment;
import com.rtovehicle.information.carbikeinfo.fragment.Upcoming_vehicles_fragment;


public class OtherInformation_Activity extends AppCompatActivity {

    LinearLayout Super_lin;
    LinearLayout upcoming_lin;
    TextView text_super;
    TextView text_upcoming;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otherinformation);
        getWindow().setBackgroundDrawable(null);
        findviewId();
    }

    public void findviewId() {
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        upcoming_lin = findViewById(R.id.upcoming_linear);
        Super_lin = findViewById(R.id.super_linear);
        text_upcoming = findViewById(R.id.upcoming_text);
        text_super = findViewById(R.id.super_code_text);
        viewPager = findViewById(R.id.vpPager);
        final Pager_Adapter slidingTabManager = new Pager_Adapter(getSupportFragmentManager());
        slidingTabManager.addFragment(new Super_vehicles_fragment());
        slidingTabManager.addFragment(new Upcoming_vehicles_fragment());
        viewPager.setAdapter(slidingTabManager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    text_super.setTextColor(Color.parseColor("#000000"));
                    text_upcoming.setTextColor(Color.parseColor("#A1A1A1"));
                }
                if (position == 1) {
                    text_upcoming.setTextColor(Color.parseColor("#000000"));
                    text_super.setTextColor(Color.parseColor("#A1A1A1"));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        Super_lin.setOnClickListener(view -> viewPager.setCurrentItem(0));
        upcoming_lin.setOnClickListener(view -> viewPager.setCurrentItem(1));
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}