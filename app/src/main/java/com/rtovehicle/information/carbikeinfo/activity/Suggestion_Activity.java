package com.rtovehicle.information.carbikeinfo.activity;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_Feedback;
import com.rtovehicle.information.carbikeinfo.common.Client_Api;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Suggestion_Activity extends AppCompatActivity {
    ImageView send_suggestion;
    EditText TextDescription;
    EditText TextTitle;
    boolean isFatching = false;
    LinearLayout dialog_ll;

    public static class ToastMessage {
        public static final String DESCRIPTION_TEXT_SET_PRE_CONDITION = "Please enter a description.";
        public static final String TITLE_TEXT_SET_PRE_CONDITION = "Please enter a title.";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggestion);
        getWindow().setBackgroundDrawable(null);
        initUI();
        clickListener();
        removeFocus();
        closeKeyboard();
    }

    private void initUI() {
        TextTitle = findViewById(R.id.TextTitle);
        TextDescription = findViewById(R.id.TextDescription);
        send_suggestion = findViewById(R.id.SendEmail);
        dialog_ll = findViewById(R.id.dialog_ll);
        findViewById(R.id.imageViewBack).setOnClickListener(v -> onBackPressed());
    }

    private void clickListener() {
        this.send_suggestion.setOnClickListener(v -> {
            if (TextTitle.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(Suggestion_Activity.this, ToastMessage.TITLE_TEXT_SET_PRE_CONDITION, Toast.LENGTH_SHORT).show();
            } else if (TextDescription.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(Suggestion_Activity.this, ToastMessage.DESCRIPTION_TEXT_SET_PRE_CONDITION, Toast.LENGTH_SHORT).show();
            } else {
                PackageInfo packageInfo = null;
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                String appverson = Objects.requireNonNull(packageInfo).versionName;
                String appname = getResources().getString(R.string.app_name);
                int appversoncode = Build.VERSION.SDK_INT;
                String mobilemodel = Build.MODEL;
                String packagename = getPackageName();
                PostFeedback(appverson, appname, String.valueOf(appversoncode), mobilemodel, packagename, TextTitle.getText().toString(), TextDescription.getText().toString());
            }
        });

    }

    private void removeFocus() {
        getWindow().setSoftInputMode(3);
    }

    private void closeKeyboard() {
        try {
            View view = getCurrentFocus();
            if (view != null) {
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void PostFeedback(String appverson, String appname, String appversoncode, String mobilemodel, String packagename, String title, String description) {

        if (!isFatching) {
            dialog_ll.setVisibility(View.VISIBLE);
            isFatching = true;

            Base_interface apiinterface = Client_Api.getClient().create(Base_interface.class);
            apiinterface.sendfeedback(appname, packagename, title, description, mobilemodel, appversoncode, appverson).enqueue(new Callback<Model_Feedback>() {
                @Override
                public void onResponse(@NonNull Call<Model_Feedback> call, @NonNull Response<Model_Feedback> response) {
                    isFatching = false;

                    if (response.code() == 200) {
                        if (response.body() != null) {
                            if (response.body().getStatus()) {
                                Toast.makeText(Suggestion_Activity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                TextDescription.setText("");
                                TextTitle.setText("");
                                dialog_ll.setVisibility(View.GONE);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Model_Feedback> call, @NonNull Throwable t) {
                    dialog_ll.setVisibility(View.GONE);
                    Toast.makeText(Suggestion_Activity.this, "Please send feedback in playstore", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}