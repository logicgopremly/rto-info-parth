package com.rtovehicle.information.carbikeinfo.model;

public class Model_HistoryVehicle {

    int id;
    String datafrom;
    String rc_owner_name;
    String rc_regn_no;
    String rc_maker_desc;
    String rc_maker_model;
    String rc_regn_dt;
    String rc_fuel_desc;
    String rc_vh_class_desc;
    String rc_registered_at;
    String rc_eng_no;
    String rc_chasi_no;
    String rc_insurance_upto;
    String rc_fit_upto;
    String rc_norms_desc;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDatafrom(String datafrom) {
        this.datafrom = datafrom;
    }

    public String getRc_owner_name() {
        return rc_owner_name;
    }

    public void setRc_owner_name(String rc_owner_name) {
        this.rc_owner_name = rc_owner_name;
    }

    public String getRc_regn_no() {
        return rc_regn_no;
    }

    public void setRc_regn_no(String rc_regn_no) {
        this.rc_regn_no = rc_regn_no;
    }

    public String getRc_maker_desc() {
        return rc_maker_desc;
    }

    public void setRc_maker_desc(String rc_maker_desc) {
        this.rc_maker_desc = rc_maker_desc;
    }

    public String getRc_maker_model() {
        return rc_maker_model;
    }

    public void setRc_maker_model(String rc_maker_model) {
        this.rc_maker_model = rc_maker_model;
    }

    public String getRc_regn_dt() {
        return rc_regn_dt;
    }

    public void setRc_regn_dt(String rc_regn_dt) {
        this.rc_regn_dt = rc_regn_dt;
    }

    public String getRc_fuel_desc() {
        return rc_fuel_desc;
    }

    public void setRc_fuel_desc(String rc_fuel_desc) {
        this.rc_fuel_desc = rc_fuel_desc;
    }

    public String getRc_vh_class_desc() {
        return rc_vh_class_desc;
    }

    public void setRc_vh_class_desc(String rc_vh_class_desc) {
        this.rc_vh_class_desc = rc_vh_class_desc;
    }

    public void setRc_registered_at(String rc_registered_at) {
        this.rc_registered_at = rc_registered_at;
    }

    public String getRc_eng_no() {
        return rc_eng_no;
    }

    public void setRc_eng_no(String rc_eng_no) {
        this.rc_eng_no = rc_eng_no;
    }

    public String getRc_chasi_no() {
        return rc_chasi_no;
    }

    public void setRc_chasi_no(String rc_chasi_no) {
        this.rc_chasi_no = rc_chasi_no;
    }

    public String getRc_insurance_upto() {
        return rc_insurance_upto;
    }

    public void setRc_insurance_upto(String rc_insurance_upto) {
        this.rc_insurance_upto = rc_insurance_upto;
    }

    public String getRc_fit_upto() {
        return rc_fit_upto;
    }

    public void setRc_fit_upto(String rc_fit_upto) {
        this.rc_fit_upto = rc_fit_upto;
    }


    public void setRc_norms_desc(String rc_norms_desc) {
        this.rc_norms_desc = rc_norms_desc;
    }
}
