package com.rtovehicle.information.carbikeinfo.model;

public class Model_QuestionBank {
    String Answer;
    String Isimage;
    String Question;
    String correctAnswer;
    int imdID;
    String[] option;

    public int getImdID() {
        return this.imdID;
    }

    public void setImdID(int i) {
        this.imdID = i;
    }

    public String getIsimage() {
        return this.Isimage;
    }

    public void setIsimage(String str) {
        this.Isimage = str;
    }

    public String getQuestion() {
        return this.Question;
    }

    public void setQuestion(String str) {
        this.Question = str;
    }

    public String getAnswer() {
        return this.Answer;
    }

    public void setAnswer(String str) {
        this.Answer = str;
    }

    public String getCorrectAnswer() {
        return this.correctAnswer;
    }

    public void setCorrectAnswer(String str) {
        this.correctAnswer = str;
    }

    public String[] getOption() {
        return this.option;
    }

    public void setOption(String[] strArr) {
        this.option = strArr;
    }
}
