package com.rtovehicle.information.carbikeinfo.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.fragment.City_fragment;
import com.rtovehicle.information.carbikeinfo.fragment.State_fragment;

public class Fuel_activity extends AppCompatActivity {
    private Fragment currentFragment;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setBackgroundDrawable(null);
        setContentView(R.layout.activity_fuel);
        setFragment(new State_fragment(), "State");

    }

    public void setFragment(Fragment fragment, String str) {
        if (fragment != null) {
            this.currentFragment = fragment;
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, str).commit();
        }
    }

    public void onBackPressed() {
        Fragment fragment = this.currentFragment;
        if (fragment instanceof City_fragment) {
            setFragment(new State_fragment(), "State");
        } else {
            finish();
        }
    }
}
