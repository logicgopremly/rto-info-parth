package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.BikeinfoVariants_Adapter;
import com.rtovehicle.information.carbikeinfo.adapter.CarListinfo_Adapter;
import com.rtovehicle.information.carbikeinfo.adapter.CarVariantsinfo_Adapter;
import com.rtovehicle.information.carbikeinfo.adapter.ColorVarients_Adapter;
import com.rtovehicle.information.carbikeinfo.adapter.CompanyInfo_Adapter;
import com.rtovehicle.information.carbikeinfo.adapter.ViewImagePager_Adapter;
import com.rtovehicle.information.carbikeinfo.common.BaseRecyclerView;
import com.rtovehicle.information.carbikeinfo.common.GridMarginDecoration;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_BikeVariant;
import com.rtovehicle.information.carbikeinfo.model.Model_BikeVariantDetails;
import com.rtovehicle.information.carbikeinfo.model.Model_CarVariant;
import com.rtovehicle.information.carbikeinfo.model.Model_CarVariantDetails;
import com.rtovehicle.information.carbikeinfo.model.Model_ColorVariant;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rd.PageIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CarFullDetails_Activity extends AppCompatActivity {

    String name, price, car_variants, image_url;
    String[] images;
    int car_id;
    ArrayList<Model_ColorVariant> colorsList_array;
    ArrayList<Model_CarVariant> variantsListCar_array;
    ArrayList<Model_BikeVariant> variantListBike_array;
    BaseRecyclerView rv_car_color_list, rv_car_variant_list;
    Dialog dialog;
    ViewPager viewPager;
    ViewImagePager_Adapter viewImagePager_adapter;
    TextView textFuelType, textSuspensionCC, textMileage;
    String vehicle_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carfulldetails);
        getWindow().setBackgroundDrawable(null);
        vehicle_type = getIntent().getStringExtra(CompanyInfo_Adapter.VEHICLE_TYPE);
        init();
    }

    private void init() {
        colorsList_array = new ArrayList<>();
        variantsListCar_array = new ArrayList<>();
        variantListBike_array = new ArrayList<>();


        viewPager = findViewById(R.id.viewPager);
        rv_car_color_list = findViewById(R.id.rv_car_color_list);
        rv_car_variant_list = findViewById(R.id.rv_car_varients_list);
        textFuelType = findViewById(R.id.txtFuelType);
        textSuspensionCC = findViewById(R.id.txtSuspensionCC);
        textMileage = findViewById(R.id.txtMileage);

        rv_car_variant_list.setLayoutManager(new LinearLayoutManager(this));
        rv_car_variant_list.setNestedScrollingEnabled(false);
        rv_car_color_list.addOuterGridSpacing(8);
        rv_car_color_list.addItemDecoration(new GridMarginDecoration(16));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        rv_car_color_list.setLayoutManager(linearLayoutManager);
        float albumGridSpacing = 16;
        rv_car_color_list.addOuterGridSpacing((int) (albumGridSpacing / 2));
        rv_car_color_list.addItemDecoration(new GridMarginDecoration((int) albumGridSpacing));

        car_id = getIntent().getIntExtra(CarListinfo_Adapter.CAR_ID, -1);
        images = getIntent().getStringArrayExtra(CarListinfo_Adapter.CAR_IMAGES);
        image_url = getIntent().getStringExtra(CarListinfo_Adapter.IMAGE_URL);
        name = getIntent().getStringExtra(CarListinfo_Adapter.CAR_NAME);
        price = getIntent().getStringExtra(CarListinfo_Adapter.CAR_PRICE);
        car_variants = getIntent().getStringExtra(CarListinfo_Adapter.CAR_VARIENTS);

        ((TextView) findViewById(R.id.car_name)).setText(name);
        ((TextView) findViewById(R.id.car_showroom_price)).setText(price);
        ((TextView) findViewById(R.id.app_title)).setText(name);
        findViewById(R.id.back_all).setOnClickListener(v -> onBackPressed());

        String[] colorsVarientsList = getIntent().getStringArrayExtra(CarListinfo_Adapter.CAR_COLOR_VARIENTS);
        for (String s : colorsVarientsList) {
            Model_ColorVariant colorVarient = new Model_ColorVariant();
            colorVarient.setHexCode(s);
            colorsList_array.add(colorVarient);
        }

        viewImagePager_adapter = new ViewImagePager_Adapter(this, images, image_url);
        PageIndicatorView pageIndicatorView = findViewById(R.id.pageIndicatorView);
        pageIndicatorView.setCount(images.length);
        pageIndicatorView.setSelection(0);
        viewPager.setAdapter(viewImagePager_adapter);
        setVarientsColors(colorsList_array);
        if (Objects.equals(vehicle_type, "car")) {
            setVarientsDetailsCar(car_variants);
        } else {
            setVarientsDetailsBike(car_variants);
        }
        if (Objects.equals(vehicle_type, "car")){
            getCarVariantDetails(car_id);
        } else {
            getBikeVariantDetails(car_id);
        }
    }

    private void setVarientsColors(ArrayList<Model_ColorVariant> colorsList) {
        ColorVarients_Adapter colorVarientsAdapter = new ColorVarients_Adapter(this, colorsList);
        rv_car_color_list.setAdapter(colorVarientsAdapter);
    }

    private void getCarVariantDetails(int varient_id) {
        ShowDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_CarVariantDetails> call = service.getVariantDetailsCar(MyApplication.MYSECRET, varient_id);

        call.enqueue(new Callback<Model_CarVariantDetails>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<Model_CarVariantDetails> call, @NonNull Response<Model_CarVariantDetails> response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            try {
                                Model_CarVariantDetails.Variant_Data.Engine engine = response.body().getData().get(0).getEngine();
                                Model_CarVariantDetails.Variant_Data.Fuel fuel = response.body().getData().get(0).getFuel();
                                textFuelType.setText(fuel.getFuelType());
                                textSuspensionCC.setText(engine.getTransmissionType() + ", " + engine.getDisplacement() + " cc");
                                textMileage.setText(fuel.getMileage() + "KM/L");
                            } catch (Exception ignored) {

                            }
                        }
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_CarVariantDetails> call, @NonNull Throwable t) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
    }

    private void getBikeVariantDetails(int varient_id) {
        ShowDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_BikeVariantDetails> call = service.getVariantDetailsBike(MyApplication.MYSECRET, varient_id);

        call.enqueue(new Callback<Model_BikeVariantDetails>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<Model_BikeVariantDetails> call, @NonNull Response<Model_BikeVariantDetails> response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            try {
                                Model_BikeVariantDetails.Variant_Data.Engine engine = response.body().getData().get(0).getEngine();
                                Model_BikeVariantDetails.Variant_Data.Milege milege = response.body().getData().get(0).getMilege();
                                textFuelType.setVisibility(View.GONE);
                                textSuspensionCC.setText(engine.getDisplacement() + " cc");
                                textMileage.setText(milege.getCityMileage());
                            } catch (Exception ignored) {
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_BikeVariantDetails> call, @NonNull Throwable t) {
                if (dialog.isShowing())
                    dialog.dismiss();

            }
        });
    }

    private void setVarientsDetailsCar(String car_varients) {
        try {
            JSONArray obj = new JSONArray(car_varients);
            for (int i = 0; i < obj.length(); i++) {
                JSONObject object = obj.getJSONObject(i);
                int id = object.getInt("id");
                String variant_name = object.getString("variant_name");
                String variant_price = object.getString("variant_price");
                String transmission_type = object.getString("transmission_type");
                String fuel_type = object.getString("fuel_type");
                String engine_disp = object.getString("engine_disp");
                String milege = object.getString("milege");

                Model_CarVariant carVariant = new Model_CarVariant();
                carVariant.setId(id);
                carVariant.setVariant_name(variant_name);
                carVariant.setVariant_price(variant_price);
                carVariant.setTransmission_type(transmission_type);
                carVariant.setFuel_type(fuel_type);
                carVariant.setEngine_disp(engine_disp);
                carVariant.setMilege(milege);
                variantsListCar_array.add(carVariant);

                CarVariantsinfo_Adapter carVariantsAdapter = new CarVariantsinfo_Adapter(this, variantsListCar_array);
                rv_car_variant_list.setAdapter(carVariantsAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setVarientsDetailsBike(String car_varients) {
        try {
            JSONArray obj = new JSONArray(car_varients);
            for (int i = 0; i < obj.length(); i++) {
                JSONObject object = obj.getJSONObject(i);
                int id = object.getInt("id");
                String variant_name = object.getString("variant_name");
                String variant_price = object.getString("variant_price");
                String cc = object.getString("cc");

                Model_BikeVariant bikeVariant = new Model_BikeVariant();
                bikeVariant.setId(id);
                bikeVariant.setVariant_name(variant_name);
                bikeVariant.setVariant_price(variant_price);
                bikeVariant.setCc(cc);
                variantListBike_array.add(bikeVariant);

                BikeinfoVariants_Adapter carVariantsAdapter = new BikeinfoVariants_Adapter(this, variantListBike_array);
                rv_car_variant_list.setAdapter(carVariantsAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void ShowDialog() {
        // TODO Auto-generated method stub
        dialog = new Dialog(CarFullDetails_Activity.this,
                android.R.style.Theme_Material_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_progress);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
        dialog.show();
    }
}