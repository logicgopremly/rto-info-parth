package com.rtovehicle.information.carbikeinfo.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.model.Model_UpcomingVehicle;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import java.util.ArrayList;

public class UpcomingVehicleShow_Activity extends AppCompatActivity {
    ArrayList<Model_UpcomingVehicle.Upcoming_Data> whatsNewModelArrayList;
    InterstitialAd mInterstitialAd;
    boolean isActivityLeft;
    LinearLayout banner;
    TextView ads_space;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcomingvehicalshow);
        getWindow().setBackgroundDrawable(null);
        banner();
        isActivityLeft = false;
        LoadAdInterstitial();
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        int i = getIntent().getIntExtra("abc", 1);
        whatsNewModelArrayList = new ArrayList<>();
        whatsNewModelArrayList = Common_Utils.upcominglist;

        if (whatsNewModelArrayList != null) {
            Model_UpcomingVehicle.Upcoming_Data upcomingModel = whatsNewModelArrayList.get(i);

            ((TextView) findViewById(R.id.vehicalname)).setText(upcomingModel.getName());
            ((TextView) findViewById(R.id.vehicalmaker)).setText(upcomingModel.getMaker());
            ((TextView) findViewById(R.id.vehicalprice)).setText(upcomingModel.getExpected_price());
            ((TextView) findViewById(R.id.vehicalMileage)).setText(upcomingModel.getMileage());
            ((TextView) findViewById(R.id.vehicalCapacity)).setText(upcomingModel.getCapicity());
            ((TextView) findViewById(R.id.vehicaltopspeed)).setText(upcomingModel.getTop_speed());
            ((TextView) findViewById(R.id.vehicalPower)).setText(upcomingModel.getPower());
            ((TextView) findViewById(R.id.vehicallaunchdate)).setText(upcomingModel.getExpected_launch());
            ((TextView) findViewById(R.id.vehicaldescription)).setText(upcomingModel.getDescription1());

            Glide.with(this).load(upcomingModel.getImage()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).into((ImageView) findViewById(R.id.vehicalimage));
        }
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }


    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });

            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;
            }
        });
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    public void onBackPressed() {
        try {
            if (mInterstitialAd != null && !isActivityLeft) {
                mInterstitialAd.show(UpcomingVehicleShow_Activity.this);
                MyApplication.appOpenManager.isAdShow = true;
            }

        } catch (Exception e) {
            e.printStackTrace();

        }
        finish();
    }

    public void banner() {
        banner = findViewById(R.id.adaptive);
        ads_space = findViewById(R.id.albumArtImage);
        AdView adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        banner.removeAllViews();
        banner.addView(adView);
        loadBanner(adView);
    }


    private void loadBanner(AdView adView) {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                ads_space.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError adError) {
                ads_space.setVisibility(View.GONE);
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdClicked() {
            }

            @Override
            public void onAdClosed() {
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.isActivityLeft = true;
    }
}
