package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.AllRTODetails_Adapter;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_RtoALLOffice;
import com.rtovehicle.information.carbikeinfo.common.LoadAnimation;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AllRTODetails_Activity extends AppCompatActivity {

    AllRTODetails_Adapter allRTODetails_adapter;
    ArrayList<Model_RtoALLOffice.DATA_ALL> rtoALLOffices_arraylist;
    RecyclerView recyclerView;
    LinearLayout rto_all_lin;
    LinearLayout lyt_progress;
    EditText edit_search;


    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_allrto_details);
        rtoALLOffices_arraylist = new ArrayList<>();
        findID();
        searchFromEditText();
        ALlRTO_INFO();
    }

    public void findID() {
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        recyclerView = findViewById(R.id.rr_rtoails);
        lyt_progress = findViewById(R.id.lyt_progress);
        rto_all_lin = findViewById(R.id.all_rto_list);
        edit_search = findViewById(R.id.edit_search);
    }

    private void searchFromEditText() {

        edit_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty()) {
                    filter(editable.toString());
                }
            }
        });

    }

    private void ALlRTO_INFO() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_Vehicles_rto)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_RtoALLOffice> call = service.RTOALL(MyApplication.MYSECRET);
        call.enqueue(new Callback<Model_RtoALLOffice>() {
            @SuppressLint({"SetTextI18n", "WrongConstant"})
            @Override
            public void onResponse(@NonNull Call<Model_RtoALLOffice> call, @NonNull retrofit2.Response<Model_RtoALLOffice> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            for (int i = 0; i < response.body().getRto_All().size(); i++) {
                                rtoALLOffices_arraylist.add(response.body().getRto_All().get(i));
                            }
                            if (rtoALLOffices_arraylist != null) {
                                allRTODetails_adapter = new AllRTODetails_Adapter(AllRTODetails_Activity.this, rtoALLOffices_arraylist);
                                recyclerView.setLayoutManager(new LinearLayoutManager(AllRTODetails_Activity.this, 1, false));
                                recyclerView.setAdapter(allRTODetails_adapter);
                                new Handler().postDelayed(() -> LoadAnimation.fadeOut(lyt_progress), 200);
                                rto_all_lin.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_RtoALLOffice> call, @NonNull Throwable t) {
            }
        });
    }


    private void filter(String text) {
        ArrayList<Model_RtoALLOffice.DATA_ALL> filter_Names = new ArrayList<>();
        for (Model_RtoALLOffice.DATA_ALL s : rtoALLOffices_arraylist) {
            if (s.getName().toLowerCase().contains(text.toLowerCase())) {
                filter_Names.add(s);
            }
        }
        allRTODetails_adapter.filterList(filter_Names);
    }

    public void onBackPressed() {
        finish();
    }
}
