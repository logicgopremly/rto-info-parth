package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.model.Model_QuestionBank;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ExamStart_Activity extends AppCompatActivity {
    String RTO_JSON;
    int ans;
    int attend = 0;
    String corr_string;
    Animation fadefast_animation;
    Animation fadeinfor_animation;
    Animation fadeoutfor_animation;
    final Handler h_timer = new Handler();
    final Handler handler = new Handler();
    Dialog loaddialog;
    final Runnable hold = () -> {
        if (ExamStart_Activity.this.sec == 0) {
            if (ExamStart_Activity.this.que_id != 16) {
                ExamStart_Activity.this.mainLayout.startAnimation(ExamStart_Activity.this.fadeoutfor_animation);
            } else if (!ExamStart_Activity.this.result) {
                ExamStart_Activity startExamActivity = ExamStart_Activity.this;
                startExamActivity.result = true;
                Intent intent = new Intent(startExamActivity, Exam_Result_Activity.class);
                String str = "lang";
                intent.putExtra(str, ExamStart_Activity.this.getIntent().getStringExtra(str));
                intent.putExtra("right", ExamStart_Activity.this.right_ans);
                intent.putExtra("wrong", ExamStart_Activity.this.wong_ans);
                ExamStart_Activity.this.integers.clear();
                String str2 = "result";
                intent.putExtra(str2, ExamStart_Activity.this.right_ans >= 11);
                intent.putExtra("attend", ExamStart_Activity.this.attend);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                ExamStart_Activity.this.startActivity(intent);
                ExamStart_Activity.this.finish();
            }
            ExamStart_Activity.this.handler.removeCallbacks(ExamStart_Activity.this.hold);
            return;
        }
        ExamStart_Activity.this.sec--;
        ExamStart_Activity.this.handler.postDelayed(ExamStart_Activity.this.hold, 1000);
    };
    TextView id_correct_ans;
    TextView id_result;
    TextView id_timer;
    TextView id_wrong_ans;
    LinearLayout imgLayout;
    int[] img_Arr;
    final List<Integer> integers = new ArrayList<>();
    LinearLayout layoutopt1;
    LinearLayout layoutopt2;
    LinearLayout layoutopt3;
    LinearLayout mainLayout;
    int num;
    TextView option1;
    TextView option2;
    TextView option3;
    TextView que;
    int que_id = 1;
    int que_idex = 0;
    String que_string;
    TextView question;
    final ArrayList<Model_QuestionBank> questionBankModels = new ArrayList<>();
    boolean result = false;
    int result_data = 0;
    String result_string;
    int right_ans = 0;
    int sec = 2;
    ImageView sign;
    int time = 30;
    String time_string;
    final Runnable timer = () -> {
        String str = "";
        if (ExamStart_Activity.this.time == 0) {
            ExamStart_Activity.this.que_idex++;
            ExamStart_Activity.this.que_id++;
            String str2 = "attend";
            String str3 = "wrong";
            String str4 = "right";
            String str5 = "result";
            String str6 = "lang";
            if (ExamStart_Activity.this.wong_ans >= 4) {
                if (!ExamStart_Activity.this.result) {
                    ExamStart_Activity startExamActivity = ExamStart_Activity.this;
                    startExamActivity.result = true;
                    Intent intent = new Intent(startExamActivity, Exam_Result_Activity.class);
                    intent.putExtra(str6, ExamStart_Activity.this.getIntent().getStringExtra(str6));
                    intent.putExtra(str4, ExamStart_Activity.this.right_ans);
                    intent.putExtra(str3, ExamStart_Activity.this.wong_ans);
                    ExamStart_Activity.this.integers.clear();
                    intent.putExtra(str5, ExamStart_Activity.this.right_ans >= 11);
                    intent.putExtra(str2, ExamStart_Activity.this.attend);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    ExamStart_Activity.this.startActivity(intent);
                    ExamStart_Activity.this.finish();
                }
            } else if (ExamStart_Activity.this.que_id != 16) {
                ExamStart_Activity.this.wong_ans++;
                TextView textView = ExamStart_Activity.this.id_timer;
                String sb = str +
                        ExamStart_Activity.this.time;
                textView.setText(sb);
                ExamStart_Activity.this.mainLayout.startAnimation(ExamStart_Activity.this.fadeoutfor_animation);
                ExamStart_Activity startExamActivity2 = ExamStart_Activity.this;
                startExamActivity2.time = 30;
                TextView textView2 = startExamActivity2.id_wrong_ans;
                String sb2 = str +
                        ExamStart_Activity.this.wong_ans;
                textView2.setText(sb2);
            } else if (!ExamStart_Activity.this.result) {
                ExamStart_Activity startExamActivity3 = ExamStart_Activity.this;
                startExamActivity3.result = true;
                Intent intent2 = new Intent(startExamActivity3, Exam_Result_Activity.class);
                intent2.putExtra(str6, ExamStart_Activity.this.getIntent().getStringExtra(str6));
                intent2.putExtra(str4, ExamStart_Activity.this.right_ans);
                intent2.putExtra(str3, ExamStart_Activity.this.wong_ans);
                ExamStart_Activity.this.integers.clear();
                intent2.putExtra(str5, ExamStart_Activity.this.right_ans >= 11);
                intent2.putExtra(str2, ExamStart_Activity.this.attend);
                intent2.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                ExamStart_Activity.this.startActivity(intent2);
                ExamStart_Activity.this.finish();
            }
            ExamStart_Activity.this.h_timer.removeCallbacks(ExamStart_Activity.this.timer);
            return;
        }
        TextView textView3 = ExamStart_Activity.this.id_timer;
        String sb3 = str +
                ExamStart_Activity.this.time;
        textView3.setText(sb3);
        ExamStart_Activity.this.time--;
        ExamStart_Activity.this.h_timer.postDelayed(ExamStart_Activity.this.timer, 1000);
    };
    int wong_ans = 0;
    String wong_string;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_exam_start);
        getWindow().setBackgroundDrawable(null);

        this.id_wrong_ans = findViewById(R.id.id_wrong_ans);
        TextView textView = this.id_wrong_ans;
        String str = "";
        String sb = str +
                this.wong_ans;
        textView.setText(sb);
        this.id_correct_ans = findViewById(R.id.id_correct_ans);
        TextView textView2 = this.id_correct_ans;
        String sb2 = str +
                this.right_ans;
        textView2.setText(sb2);
        this.id_result = findViewById(R.id.id_result);
        TextView textView3 = this.id_result;
        String sb3 = this.result_data +
                "/15";
        textView3.setText(sb3);
        this.id_timer = findViewById(R.id.id_timer);
        TextView textView4 = this.id_timer;
        String sb4 = str +
                this.time;
        textView4.setText(sb4);

        this.h_timer.post(this.timer);
        findViewById(R.id.back_all).setOnClickListener(view -> ExamStart_Activity.this.onBackPressed());

        this.mainLayout = findViewById(R.id.mainLayout);
        this.layoutopt1 = findViewById(R.id.layoutopt1);
        this.layoutopt2 = findViewById(R.id.layoutopt2);
        this.layoutopt3 = findViewById(R.id.layoutopt3);
        this.imgLayout = findViewById(R.id.imgLayout);
        this.que = findViewById(R.id.que);
        this.question = findViewById(R.id.question);
        this.option1 = findViewById(R.id.option1);
        this.option2 = findViewById(R.id.option2);
        this.option3 = findViewById(R.id.option3);
        this.sign = findViewById(R.id.sign);
        this.fadeinfor_animation = AnimationUtils.loadAnimation(this, R.anim.fadeinfor);
        this.fadeoutfor_animation = AnimationUtils.loadAnimation(this, R.anim.fadeoutfor);
        this.fadefast_animation = AnimationUtils.loadAnimation(this, R.anim.fadeinforfast);
        ShowDialog();
        String str2 = "lang";
        String str3 = "english";
        if (!getIntent().getStringExtra(str2).equalsIgnoreCase(str3)) {
            String str4 = "gujarati";
            if (!getIntent().getStringExtra(str2).equalsIgnoreCase(str4)) {
                String str5 = "hindi";
                if (!getIntent().getStringExtra(str2).equalsIgnoreCase(str5)) {
                    this.que_string = getResources().getStringArray(R.array.que)[3];
                    this.img_Arr = Common_Utils.Marathi_Arr;
                    this.corr_string = getResources().getStringArray(R.array.corret_que)[3];
                    this.wong_string = getResources().getStringArray(R.array.wrong_que)[3];
                    this.result_string = getResources().getStringArray(R.array.result_que)[3];
                    this.time_string = getResources().getStringArray(R.array.time_que)[3];
                    this.RTO_JSON = loadJSONFromAsset("marathi");
                } else {
                    this.img_Arr = Common_Utils.Hindi_Arr;
                    this.corr_string = getResources().getStringArray(R.array.corret_que)[2];
                    this.wong_string = getResources().getStringArray(R.array.wrong_que)[2];
                    this.result_string = getResources().getStringArray(R.array.result_que)[2];
                    this.time_string = getResources().getStringArray(R.array.time_que)[2];
                    this.que_string = getResources().getStringArray(R.array.que)[2];
                    this.RTO_JSON = loadJSONFromAsset(str5);
                }
            } else {
                this.img_Arr = Common_Utils.Guj_Arr;
                this.corr_string = getResources().getStringArray(R.array.corret_que)[1];
                this.wong_string = getResources().getStringArray(R.array.wrong_que)[1];
                this.result_string = getResources().getStringArray(R.array.result_que)[1];
                this.time_string = getResources().getStringArray(R.array.time_que)[1];
                this.que_string = getResources().getStringArray(R.array.que)[1];
                this.RTO_JSON = loadJSONFromAsset(str4);
            }
        } else {
            this.RTO_JSON = loadJSONFromAsset(str3);
            this.que_string = getResources().getStringArray(R.array.que)[0];
            this.img_Arr = Common_Utils.Eng_Arr;
            this.corr_string = getResources().getStringArray(R.array.corret_que)[0];
            this.wong_string = getResources().getStringArray(R.array.wrong_que)[0];
            this.result_string = getResources().getStringArray(R.array.result_que)[0];
            this.time_string = getResources().getStringArray(R.array.time_que)[0];
        }
        new LoadData().execute();

        this.layoutopt1.setOnClickListener(view -> {
            ExamStart_Activity.this.h_timer.removeCallbacks(ExamStart_Activity.this.timer);
            ExamStart_Activity startExamActivity = ExamStart_Activity.this;
            startExamActivity.time = 30;
            startExamActivity.attend++;
            ExamStart_Activity.this.que_idex++;
            ExamStart_Activity.this.que_id++;
            ExamStart_Activity startExamActivity2 = ExamStart_Activity.this;
            startExamActivity2.sec = 2;
            startExamActivity2.handler.removeCallbacks(ExamStart_Activity.this.hold);
            ExamStart_Activity.this.handler.post(ExamStart_Activity.this.hold);
            ExamStart_Activity.this.layoutopt1.setClickable(false);
            ExamStart_Activity.this.layoutopt2.setClickable(false);
            ExamStart_Activity.this.layoutopt3.setClickable(false);
            String str1 = "";
            if (ExamStart_Activity.this.ans == 0) {
                ExamStart_Activity.this.layoutopt1.setBackgroundResource(R.drawable.right_ans);
                option1.setTextColor(Color.WHITE);


                ExamStart_Activity.this.layoutopt1.startAnimation(ExamStart_Activity.this.fadefast_animation);
                ExamStart_Activity.this.right_ans++;
                ExamStart_Activity.this.result_data++;
                TextView textView1 = ExamStart_Activity.this.id_result;
                String sb1 = ExamStart_Activity.this.result_data +
                        "/15";
                textView1.setText(sb1);
                TextView textView21 = ExamStart_Activity.this.id_correct_ans;
                String sb21 = str1 +
                        ExamStart_Activity.this.right_ans;
                textView21.setText(sb21);
            } else {
                ExamStart_Activity.this.layoutopt1.setBackgroundResource(R.drawable.wrong_ans);
                option1.setTextColor(Color.WHITE);


                ExamStart_Activity.this.layoutopt1.startAnimation(ExamStart_Activity.this.fadefast_animation);
                ExamStart_Activity.this.wong_ans++;
                TextView textView31 = ExamStart_Activity.this.id_wrong_ans;
                String sb31 = str1 +
                        ExamStart_Activity.this.wong_ans;
                textView31.setText(sb31);
            }
            if (ExamStart_Activity.this.ans == 1) {
                ExamStart_Activity.this.layoutopt2.setBackgroundResource(R.drawable.right_ans);
                option2.setTextColor(Color.WHITE);

                ExamStart_Activity.this.layoutopt2.startAnimation(ExamStart_Activity.this.fadefast_animation);
            }
            if (ExamStart_Activity.this.ans == 2) {
                ExamStart_Activity.this.layoutopt3.setBackgroundResource(R.drawable.right_ans);
                option3.setTextColor(Color.WHITE);

                ExamStart_Activity.this.layoutopt3.startAnimation(ExamStart_Activity.this.fadefast_animation);
            }
        });
        this.layoutopt2.setOnClickListener(view -> {
            ExamStart_Activity.this.que_idex++;
            ExamStart_Activity.this.que_id++;
            ExamStart_Activity.this.attend++;
            ExamStart_Activity.this.h_timer.removeCallbacks(ExamStart_Activity.this.timer);
            ExamStart_Activity startExamActivity = ExamStart_Activity.this;
            startExamActivity.time = 30;
            startExamActivity.sec = 2;
            startExamActivity.handler.removeCallbacks(ExamStart_Activity.this.hold);
            ExamStart_Activity.this.handler.post(ExamStart_Activity.this.hold);
            ExamStart_Activity.this.layoutopt1.setClickable(false);
            ExamStart_Activity.this.layoutopt2.setClickable(false);
            ExamStart_Activity.this.layoutopt3.setClickable(false);
            if (ExamStart_Activity.this.ans == 0) {
                ExamStart_Activity.this.layoutopt1.setBackgroundResource(R.drawable.right_ans);
                ExamStart_Activity.this.layoutopt1.startAnimation(ExamStart_Activity.this.fadefast_animation);
                option1.setTextColor(Color.WHITE);

            }
            String str12 = "";
            if (ExamStart_Activity.this.ans == 1) {
                ExamStart_Activity.this.layoutopt2.setBackgroundResource(R.drawable.right_ans);
                option2.setTextColor(Color.WHITE);

                ExamStart_Activity.this.layoutopt2.startAnimation(ExamStart_Activity.this.fadefast_animation);
                ExamStart_Activity.this.right_ans++;
                TextView textView12 = ExamStart_Activity.this.id_correct_ans;
                String sb12 = str12 +
                        ExamStart_Activity.this.right_ans;
                textView12.setText(sb12);
                ExamStart_Activity.this.result_data++;
                TextView textView212 = ExamStart_Activity.this.id_result;
                String sb212 = ExamStart_Activity.this.result_data +
                        "/15";
                textView212.setText(sb212);
            } else {
                ExamStart_Activity.this.layoutopt2.setBackgroundResource(R.drawable.wrong_ans);
                option2.setTextColor(Color.WHITE);

                ExamStart_Activity.this.layoutopt2.startAnimation(ExamStart_Activity.this.fadefast_animation);
                ExamStart_Activity.this.wong_ans++;
                TextView textView312 = ExamStart_Activity.this.id_wrong_ans;
                String sb312 = str12 +
                        ExamStart_Activity.this.wong_ans;
                textView312.setText(sb312);
            }
            if (ExamStart_Activity.this.ans == 2) {
                ExamStart_Activity.this.layoutopt3.setBackgroundResource(R.drawable.right_ans);
                option3.setTextColor(Color.WHITE);

                ExamStart_Activity.this.layoutopt3.startAnimation(ExamStart_Activity.this.fadefast_animation);
            }
        });
        this.layoutopt3.setOnClickListener(view -> {
            ExamStart_Activity.this.attend++;
            ExamStart_Activity.this.que_idex++;
            ExamStart_Activity.this.que_id++;
            ExamStart_Activity.this.h_timer.removeCallbacks(ExamStart_Activity.this.timer);
            ExamStart_Activity startExamActivity = ExamStart_Activity.this;
            startExamActivity.time = 30;
            startExamActivity.sec = 2;
            startExamActivity.handler.removeCallbacks(ExamStart_Activity.this.hold);
            ExamStart_Activity.this.handler.post(ExamStart_Activity.this.hold);
            if (ExamStart_Activity.this.ans == 0) {
                ExamStart_Activity.this.layoutopt1.setBackgroundResource(R.drawable.right_ans);
                option1.setTextColor(Color.WHITE);

                ExamStart_Activity.this.layoutopt1.startAnimation(ExamStart_Activity.this.fadefast_animation);
            }
            if (ExamStart_Activity.this.ans == 1) {
                ExamStart_Activity.this.layoutopt2.setBackgroundResource(R.drawable.right_ans);
                option2.setTextColor(Color.WHITE);

                ExamStart_Activity.this.layoutopt2.startAnimation(ExamStart_Activity.this.fadefast_animation);
            }
            String str13 = "";
            if (ExamStart_Activity.this.ans == 2) {
                ExamStart_Activity.this.layoutopt3.setBackgroundResource(R.drawable.right_ans);
                option3.setTextColor(Color.WHITE);
                ExamStart_Activity.this.layoutopt3.startAnimation(ExamStart_Activity.this.fadefast_animation);
                ExamStart_Activity.this.right_ans++;
                TextView textView13 = ExamStart_Activity.this.id_correct_ans;
                String sb13 = str13 +
                        ExamStart_Activity.this.right_ans;
                textView13.setText(sb13);
                ExamStart_Activity.this.result_data++;
                TextView textView213 = ExamStart_Activity.this.id_result;
                String sb213 = ExamStart_Activity.this.result_data +
                        "/15";
                textView213.setText(sb213);
                return;
            }
            ExamStart_Activity.this.layoutopt3.setBackgroundResource(R.drawable.wrong_ans);
            option3.setTextColor(Color.WHITE);

            ExamStart_Activity.this.layoutopt3.startAnimation(ExamStart_Activity.this.fadefast_animation);
            ExamStart_Activity.this.wong_ans++;
            TextView textView313 = ExamStart_Activity.this.id_wrong_ans;
            String sb313 = str13 +
                    ExamStart_Activity.this.wong_ans;
            textView313.setText(sb313);
        });


        this.fadefast_animation.setAnimationListener(new AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                ExamStart_Activity.this.layoutopt1.setClickable(false);
                ExamStart_Activity.this.layoutopt2.setClickable(false);
                ExamStart_Activity.this.layoutopt3.setClickable(false);
            }

            public void onAnimationEnd(Animation animation) {
                if (ExamStart_Activity.this.wong_ans >= 5 && !ExamStart_Activity.this.result) {
                    ExamStart_Activity startExamActivity = ExamStart_Activity.this;
                    startExamActivity.result = true;
                    Intent intent = new Intent(startExamActivity, Exam_Result_Activity.class);
                    String str = "lang";
                    intent.putExtra(str, ExamStart_Activity.this.getIntent().getStringExtra(str));
                    intent.putExtra("right", ExamStart_Activity.this.right_ans);
                    intent.putExtra("wrong", ExamStart_Activity.this.wong_ans);
                    ExamStart_Activity.this.integers.clear();
                    String str2 = "result";
                    intent.putExtra(str2, ExamStart_Activity.this.right_ans >= 11);
                    intent.putExtra("attend", ExamStart_Activity.this.attend);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    ExamStart_Activity.this.startActivity(intent);
                    ExamStart_Activity.this.finish();
                }
            }
        });
        this.fadeinfor_animation.setAnimationListener(new AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                ExamStart_Activity.this.layoutopt1.setClickable(false);
                ExamStart_Activity.this.layoutopt2.setClickable(false);
                ExamStart_Activity.this.layoutopt3.setClickable(false);
                ExamStart_Activity.this.layoutopt1.setBackgroundResource(R.drawable.ans_bg);
                ExamStart_Activity.this.layoutopt2.setBackgroundResource(R.drawable.ans_bg);
                ExamStart_Activity.this.layoutopt3.setBackgroundResource(R.drawable.ans_bg);
            }

            public void onAnimationEnd(Animation animation) {
                option3.setTextColor(Color.BLACK);
                option2.setTextColor(Color.BLACK);
                option1.setTextColor(Color.BLACK);
                ExamStart_Activity.this.layoutopt1.setClickable(true);
                ExamStart_Activity.this.layoutopt2.setClickable(true);
                ExamStart_Activity.this.layoutopt3.setClickable(true);

            }
        });
        this.fadeoutfor_animation.setAnimationListener(new AnimationListener() {
            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            @SuppressLint({"WrongConstant", "ResourceAsColor"})
            public void onAnimationEnd(Animation animation) {
                ExamStart_Activity startExamActivity = ExamStart_Activity.this;
                startExamActivity.time = 30;
                startExamActivity.h_timer.removeCallbacks(ExamStart_Activity.this.timer);
                ExamStart_Activity.this.h_timer.post(ExamStart_Activity.this.timer);
                if (ExamStart_Activity.this.questionBankModels.size() != 0) {
                    ExamStart_Activity startExamActivity2 = ExamStart_Activity.this;
                    startExamActivity2.num = startExamActivity2.ramnumber(startExamActivity2.questionBankModels.size() - 1);
                    Model_QuestionBank questionBankModel = ExamStart_Activity.this.questionBankModels.get(ExamStart_Activity.this.num);
                    TextView textView = ExamStart_Activity.this.que;
                    String sb = ExamStart_Activity.this.que_string +
                            " " +
                            ExamStart_Activity.this.que_id;
                    textView.setText(sb);
                    ExamStart_Activity.this.question.setText(questionBankModel.getQuestion());
                    String str = "1";
                    if (!questionBankModel.getIsimage().equalsIgnoreCase(str)) {
                        ExamStart_Activity.this.imgLayout.setVisibility(8);
                    } else {
                        ExamStart_Activity.this.imgLayout.setVisibility(0);
                        ExamStart_Activity.this.sign.setImageResource(questionBankModel.getImdID());
                    }
                    TextView textView2 = ExamStart_Activity.this.option1;
                    String sb2 = "A. " +
                            questionBankModel.getOption()[0];
                    textView2.setText(sb2);
                    TextView textView3 = ExamStart_Activity.this.option2;
                    String sb3 = "B. " +
                            questionBankModel.getOption()[1];
                    textView3.setText(sb3);
                    TextView textView4 = ExamStart_Activity.this.option3;
                    String sb4 = "C. " +
                            questionBankModel.getOption()[2];
                    textView4.setText(sb4);
                    if (!questionBankModel.getCorrectAnswer().equalsIgnoreCase("0")) {
                        ExamStart_Activity.this.layoutopt1.setBackgroundResource(R.drawable.ans_bg);
                    } else {
                        ExamStart_Activity.this.ans = 0;
                    }
                    if (questionBankModel.getCorrectAnswer().equalsIgnoreCase(str)) {
                        ExamStart_Activity.this.ans = 1;
                    }
                    if (questionBankModel.getCorrectAnswer().equalsIgnoreCase("2")) {

                        ExamStart_Activity.this.ans = 2;
                    }
                    ExamStart_Activity.this.mainLayout.startAnimation(ExamStart_Activity.this.fadeinfor_animation);
                    ExamStart_Activity.this.questionBankModels.remove(ExamStart_Activity.this.num);
                }
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private class LoadData extends AsyncTask<Void, Void, Void> {
        private LoadData() {
        }


        public Void doInBackground(Void... voidArr) {
            String str = "Isimage";
            try {
                JSONArray jSONArray = new JSONObject(ExamStart_Activity.this.RTO_JSON).getJSONArray("data");
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = (JSONObject) jSONArray.get(i);
                    Model_QuestionBank questionBankModel = new Model_QuestionBank();
                    questionBankModel.setAnswer(jSONObject.getString("Answer"));
                    questionBankModel.setQuestion(jSONObject.getString("Question"));
                    questionBankModel.setIsimage(jSONObject.getString(str));
                    questionBankModel.setCorrectAnswer(jSONObject.getString("correctAnswer"));
                    JSONArray jSONArray2 = jSONObject.getJSONArray("option");
                    String[] strArr = new String[jSONArray2.length()];
                    for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                        strArr[i2] = (String) jSONArray2.get(i2);
                    }
                    questionBankModel.setOption(strArr);
                    if (jSONObject.getString(str).equalsIgnoreCase("1")) {
                        questionBankModel.setImdID(ExamStart_Activity.this.img_Arr[i]);
                    }
                    ExamStart_Activity.this.questionBankModels.add(questionBankModel);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                StringBuilder sb = new StringBuilder();
                sb.append("");
                sb.append(e);
            }
            return null;
        }


        @SuppressLint({"WrongConstant", "ResourceAsColor"})
        public void onPostExecute(Void voidR) {
            super.onPostExecute(voidR);
            if (loaddialog.isShowing()) {
                loaddialog.dismiss();
            }
            if (ExamStart_Activity.this.questionBankModels.size() != 0) {
                ExamStart_Activity startExamActivity = ExamStart_Activity.this;
                startExamActivity.num = startExamActivity.ramnumber(startExamActivity.questionBankModels.size() - 1);
                Model_QuestionBank questionBankModel = ExamStart_Activity.this.questionBankModels.get(ExamStart_Activity.this.num);
                TextView textView = ExamStart_Activity.this.que;
                String sb = ExamStart_Activity.this.que_string +
                        " " +
                        ExamStart_Activity.this.que_id;
                textView.setText(sb);
                ExamStart_Activity.this.question.setText(questionBankModel.getQuestion());
                String str = "1";
                if (!questionBankModel.getIsimage().equalsIgnoreCase(str)) {
                    ExamStart_Activity.this.imgLayout.setVisibility(8);
                } else {
                    ExamStart_Activity.this.imgLayout.setVisibility(0);
                    ExamStart_Activity.this.sign.setImageResource(questionBankModel.getImdID());
                }
                TextView textView2 = ExamStart_Activity.this.option1;
                String sb2 = "A. " +
                        questionBankModel.getOption()[0];
                textView2.setText(sb2);
                TextView textView3 = ExamStart_Activity.this.option2;
                String sb3 = "B. " +
                        questionBankModel.getOption()[1];
                textView3.setText(sb3);
                TextView textView4 = ExamStart_Activity.this.option3;
                String sb4 = "C. " +
                        questionBankModel.getOption()[2];
                textView4.setText(sb4);
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase("0")) {

                    ExamStart_Activity.this.ans = 0;
                }
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase(str)) {
                    ExamStart_Activity.this.ans = 1;
                }
                if (questionBankModel.getCorrectAnswer().equalsIgnoreCase("2")) {

                    ExamStart_Activity.this.ans = 2;
                }
                ExamStart_Activity.this.questionBankModels.remove(ExamStart_Activity.this.num);
            }
        }
    }


    public String loadJSONFromAsset(String str) {
        try {
            AssetManager assets = getAssets();
            String sb = str +
                    ".json";
            InputStream open = assets.open(sb);
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            return new String(bArr, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    public int ramnumber(int i) {
        int nextInt = new Random().nextInt(i);
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(nextInt);
        return nextInt;
    }


    @SuppressLint("WrongConstant")
    public void onPause() {
        super.onPause();

        this.h_timer.removeCallbacks(this.timer);
    }


    @SuppressLint("WrongConstant")
    public void onResume() {
        super.onResume();


        this.h_timer.removeCallbacks(this.timer);
        this.h_timer.post(this.timer);
    }

    private void ShowDialog() {
        // TODO Auto-generated method stub
        loaddialog = new Dialog(ExamStart_Activity.this,
                android.R.style.Theme_Material_Dialog);
        loaddialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loaddialog.setContentView(R.layout.loading_progress);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(loaddialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        loaddialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        loaddialog.getWindow().setAttributes(lp);
        loaddialog.setCancelable(false);
        loaddialog.show();
    }
}
