package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.internal.view.SupportMenu;
import androidx.core.view.ViewCompat;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rtovehicle.information.carbikeinfo.common.DBTiny;
import com.rtovehicle.information.carbikeinfo.model.Model_ExamHistory;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Exam_Result_Activity extends AppCompatActivity {
    TextView header;
    TextView id_attend_que;
    TextView id_right_ans;
    TextView id_wrongs;
    String[] list_item_array;
    Boolean result;
    DBTiny tinyDB;
    TextView text_cong_msg;
    TextView text_msg;
    TextView text_result;
    TextView text_you_scored;


    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_exam_result);
        getWindow().setBackgroundDrawable(null);
        this.tinyDB = new DBTiny(this);
        findid();
        result();
    }


    public void findid() {
        this.text_you_scored = findViewById(R.id.txt_you_scored);
        this.text_result = findViewById(R.id.txt_result);
        this.text_cong_msg = findViewById(R.id.txt_cong_msg);
        this.text_msg = findViewById(R.id.txt_msg);
        this.id_attend_que = findViewById(R.id.id_attend_que);
        this.id_right_ans = findViewById(R.id.id_right_ans);
        this.id_wrongs = findViewById(R.id.id_wrongans);
        this.header = findViewById(R.id.id_header);
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
    }

    @SuppressLint("SetTextI18n")
    public void result() {
        String str = "right";
        getIntent().getIntExtra(str, 0);
        String str2 = "wrong";
        getIntent().getIntExtra(str2, 0);
        String str3 = "result";
        this.result = getIntent().getBooleanExtra(str3, false);
        @SuppressLint("SimpleDateFormat") String format = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        String str4 = "lang";
        if (getIntent().getStringExtra(str4).equalsIgnoreCase("english")) {
            this.list_item_array = getResources().getStringArray(R.array.qustion_page_english);
            Common_Utils.eng_history_list = this.tinyDB.getAdListObject(Common_Utils.key_eng_history_list);
            String str5 = "Wrong Answer: ";
            String str6 = "Right Answer: ";
            String str7 = "Questions attended: ";
            String str8 = "You Scored";
            if (this.result) {
                this.text_you_scored.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_result.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_cong_msg.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_msg.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_attend_que.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_right_ans.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_wrongs.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                int intExtra = getIntent().getIntExtra(str, 0) + getIntent().getIntExtra(str2, 0);
                this.text_you_scored.setText(str8);
                this.text_result.setText(getIntent().getStringExtra(str3));
                this.text_cong_msg.setText("Congratulations!!");
                this.text_msg.setText("You've just cleared driving licence test");
                TextView textView = this.id_attend_que;
                String sb = str7 +
                        intExtra;
                textView.setText(sb);
                TextView textView2 = this.id_right_ans;
                String sb2 = str6 +
                        getIntent().getIntExtra(str, 0);
                textView2.setText(sb2);
                TextView textView3 = this.id_wrongs;
                String sb3 = str5 +
                        getIntent().getIntExtra(str2, 0);
                textView3.setText(sb3);
                Model_ExamHistory examHistoryModel = new Model_ExamHistory();
                examHistoryModel.setPass(1);
                examHistoryModel.setAttend_que(intExtra);
                examHistoryModel.setCorrect_ans(getIntent().getIntExtra(str, 0));
                examHistoryModel.setDate(format);
                examHistoryModel.setReuslt("Pass");
                examHistoryModel.setWrong_ans(getIntent().getIntExtra(str2, 0));
                Common_Utils.eng_history_list.add(examHistoryModel);
            } else {
                this.text_you_scored.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_result.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_cong_msg.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_msg.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_attend_que.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_right_ans.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_wrongs.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_you_scored.setText(str8);
                int intExtra2 = getIntent().getIntExtra(str, 0) + getIntent().getIntExtra(str2, 0);
                this.text_result.setText(getIntent().getStringExtra(str3));
                this.text_cong_msg.setText("Sorry! You failed the test");
                this.text_msg.setText("Sorry! You have failed in driving licence test");
                TextView textView4 = this.id_attend_que;
                String sb4 = str7 +
                        intExtra2;
                textView4.setText(sb4);
                TextView textView5 = this.id_right_ans;
                String sb5 = str6 +
                        getIntent().getIntExtra(str, 0);
                textView5.setText(sb5);
                TextView textView6 = this.id_wrongs;
                String sb6 = str5 +
                        getIntent().getIntExtra(str2, 0);
                textView6.setText(sb6);
                Model_ExamHistory examHistoryModel2 = new Model_ExamHistory();
                examHistoryModel2.setPass(0);
                examHistoryModel2.setAttend_que(intExtra2);
                examHistoryModel2.setCorrect_ans(getIntent().getIntExtra(str, 0));
                examHistoryModel2.setDate(format);
                examHistoryModel2.setReuslt("Fail");
                examHistoryModel2.setWrong_ans(getIntent().getIntExtra(str2, 0));
                Common_Utils.eng_history_list.add(examHistoryModel2);
            }
            this.tinyDB.putAdListObject(Common_Utils.key_eng_history_list, Common_Utils.eng_history_list);
        } else if (getIntent().getStringExtra(str4).equalsIgnoreCase("gujarati")) {
            this.list_item_array = getResources().getStringArray(R.array.qustion_page_gujarati);
            Common_Utils.guj_history_list = this.tinyDB.getAdListObject(Common_Utils.key_guj_history_list);
            String str9 = "સાચા જવાબ: ";
            String str10 = "પ્રશ્નો હાજરી: ";
            String str11 = "તમે સ્કોર બનાવ્યો";
            if (this.result) {
                this.text_you_scored.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_result.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_cong_msg.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_msg.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_attend_que.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_right_ans.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_wrongs.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_you_scored.setText(str11);
                int intExtra3 = getIntent().getIntExtra(str, 0) + getIntent().getIntExtra(str2, 0);
                this.text_result.setText(getIntent().getStringExtra(str3));
                this.text_cong_msg.setText("અભિનંદન");
                this.text_msg.setText("તમે ડ્રાઇવિંગ લાઇસેંસ પરીક્ષણ પાસ કર્યું છે");
                TextView textView7 = this.id_attend_que;
                String sb7 = str10 +
                        intExtra3;
                textView7.setText(sb7);
                TextView textView8 = this.id_right_ans;
                String sb8 = str9 +
                        getIntent().getIntExtra(str, 0);
                textView8.setText(sb8);
                TextView textView9 = this.id_wrongs;
                String sb9 = "ખોટા જવાબ: " +
                        getIntent().getIntExtra(str2, 0);
                textView9.setText(sb9);
                Model_ExamHistory examHistoryModel3 = new Model_ExamHistory();
                examHistoryModel3.setPass(1);
                examHistoryModel3.setAttend_que(intExtra3);
                examHistoryModel3.setCorrect_ans(getIntent().getIntExtra(str, 0));
                examHistoryModel3.setDate(format);
                examHistoryModel3.setReuslt("પાસ");
                examHistoryModel3.setWrong_ans(getIntent().getIntExtra(str2, 0));
                Common_Utils.guj_history_list.add(examHistoryModel3);
            } else {
                int intExtra4 = getIntent().getIntExtra(str, 0) + getIntent().getIntExtra(str2, 0);
                Model_ExamHistory examHistoryModel4 = new Model_ExamHistory();
                examHistoryModel4.setAttend_que(intExtra4);
                examHistoryModel4.setCorrect_ans(getIntent().getIntExtra(str, 0));
                examHistoryModel4.setDate(format);
                examHistoryModel4.setReuslt("નિષ્ફળ");
                examHistoryModel4.setWrong_ans(getIntent().getIntExtra(str2, 0));
                Common_Utils.guj_history_list.add(examHistoryModel4);
                this.text_you_scored.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_result.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_cong_msg.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_msg.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_attend_que.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_right_ans.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_wrongs.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_you_scored.setText(str11);
                examHistoryModel4.setPass(0);
                this.text_result.setText(getIntent().getStringExtra(str3));
                this.text_cong_msg.setText("માફ કરશો! તમે પરીક્ષણમાં નિષ્ફળ ગયા છો");
                this.text_msg.setText("માફ કરશો! તમે ડ્રાઇવિંગ લાઇસેંસ પરીક્ષણમાં નિષ્ફળ ગયા છો");
                TextView textView10 = this.id_attend_que;
                String sb10 = str10 +
                        intExtra4;
                textView10.setText(sb10);
                TextView textView11 = this.id_right_ans;
                String sb11 = str9 +
                        getIntent().getIntExtra(str, 0);
                textView11.setText(sb11);
                TextView textView12 = this.id_wrongs;
                String sb12 = "ખોટા જવાબ: " +
                        getIntent().getIntExtra(str2, 0);
                textView12.setText(sb12);
            }
            this.tinyDB.putAdListObject(Common_Utils.key_guj_history_list, Common_Utils.guj_history_list);
        } else if (getIntent().getStringExtra(str4).equalsIgnoreCase("hindi")) {
            this.list_item_array = getResources().getStringArray(R.array.qustion_page_hindi);
            Common_Utils.hindi_history_list = this.tinyDB.getAdListObject(Common_Utils.key_hindi_history_list);
            if (this.result) {
                this.text_you_scored.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_result.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_cong_msg.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_msg.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_attend_que.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_right_ans.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_wrongs.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                Model_ExamHistory examHistoryModel5 = new Model_ExamHistory();
                int intExtra5 = getIntent().getIntExtra(str, 0) + getIntent().getIntExtra(str2, 0);
                examHistoryModel5.setAttend_que(intExtra5);
                examHistoryModel5.setCorrect_ans(getIntent().getIntExtra(str, 0));
                examHistoryModel5.setDate(format);
                examHistoryModel5.setReuslt("पास");
                examHistoryModel5.setWrong_ans(getIntent().getIntExtra(str2, 0));
                examHistoryModel5.setPass(1);
                Common_Utils.hindi_history_list.add(examHistoryModel5);
                this.text_you_scored.setText("आपने स्कोर किया");
                this.text_result.setText(getIntent().getStringExtra(str3));
                this.text_cong_msg.setText("अभिनंदन");
                this.text_msg.setText("आपने ड्राइविंग लाइसेंस परीक्षा उत्तीर्ण की है");
                TextView textView13 = this.id_attend_que;
                String sb13 = "सवाल उपस्थित: " +
                        intExtra5;
                textView13.setText(sb13);
                TextView textView14 = this.id_right_ans;
                String sb14 = "सही उत्तर: " +
                        getIntent().getIntExtra(str, 0);
                textView14.setText(sb14);
                TextView textView15 = this.id_wrongs;
                String sb15 = "गलत जवाब: " +
                        getIntent().getIntExtra(str2, 0);
                textView15.setText(sb15);
            } else {
                int intExtra6 = getIntent().getIntExtra(str, 0) + getIntent().getIntExtra(str2, 0);
                Model_ExamHistory examHistoryModel6 = new Model_ExamHistory();
                examHistoryModel6.setAttend_que(intExtra6);
                examHistoryModel6.setCorrect_ans(getIntent().getIntExtra(str, 0));
                examHistoryModel6.setDate(format);
                examHistoryModel6.setReuslt("असफल");
                examHistoryModel6.setWrong_ans(getIntent().getIntExtra(str2, 0));
                Common_Utils.hindi_history_list.add(examHistoryModel6);
                this.text_you_scored.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_result.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_cong_msg.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_msg.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_attend_que.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_right_ans.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_wrongs.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_you_scored.setText("आपने स्कोर किया");
                examHistoryModel6.setPass(0);
                this.text_result.setText(getIntent().getStringExtra(str3));
                this.text_cong_msg.setText("माफ़ कीजिये! आप परीक्षा में असफल रहे हैं");
                this.text_msg.setText("माफ़ कीजिये! आप ड्राइविंग लाइसेंस परीक्षण में विफल रहे हैं");
                TextView textView16 = this.id_attend_que;
                String sb16 = "सवाल उपस्थित: " +
                        intExtra6;
                textView16.setText(sb16);
                TextView textView17 = this.id_right_ans;
                String sb17 = "सही उत्तर: " +
                        getIntent().getIntExtra(str, 0);
                textView17.setText(sb17);
                TextView textView18 = this.id_wrongs;
                String sb18 = "गलत जवाब: " +
                        getIntent().getIntExtra(str2, 0);
                textView18.setText(sb18);
            }
            this.tinyDB.putAdListObject(Common_Utils.key_hindi_history_list, Common_Utils.hindi_history_list);
        } else {
            this.list_item_array = getResources().getStringArray(R.array.qustion_page_marathi);
            Common_Utils.marathi_history_list = this.tinyDB.getAdListObject(Common_Utils.key_marathi_history_list);
            if (this.result) {
                Model_ExamHistory examHistoryModel7 = new Model_ExamHistory();
                int intExtra7 = getIntent().getIntExtra(str, 0) + getIntent().getIntExtra(str2, 0);
                examHistoryModel7.setAttend_que(intExtra7);
                examHistoryModel7.setCorrect_ans(getIntent().getIntExtra(str, 0));
                examHistoryModel7.setDate(format);
                examHistoryModel7.setReuslt("पास");
                examHistoryModel7.setPass(1);
                examHistoryModel7.setWrong_ans(getIntent().getIntExtra(str2, 0));
                Common_Utils.marathi_history_list.add(examHistoryModel7);
                this.text_you_scored.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_result.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_cong_msg.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_msg.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_attend_que.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_right_ans.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.id_wrongs.setTextColor(ViewCompat.MEASURED_STATE_MASK);
                this.text_you_scored.setText("आपण स्कोअर के");
                this.text_result.setText(getIntent().getStringExtra(str3));
                this.text_cong_msg.setText("अभिनंदन");
                this.text_msg.setText("आपण वाहनचालक परवाना चाचणी उत्तीर्ण केली आहे");
                TextView textView19 = this.id_attend_que;
                String sb19 = "प्रश्न उपस्थित: " +
                        intExtra7;
                textView19.setText(sb19);
                TextView textView20 = this.id_right_ans;
                String sb20 = "खरे उत्तर: " +
                        getIntent().getIntExtra(str, 0);
                textView20.setText(sb20);
                TextView textView21 = this.id_wrongs;
                String sb21 = "खोटे उत्तर: " +
                        getIntent().getIntExtra(str2, 0);
                textView21.setText(sb21);
            } else {
                Model_ExamHistory examHistoryModel8 = new Model_ExamHistory();
                int intExtra8 = getIntent().getIntExtra(str, 0) + getIntent().getIntExtra(str2, 0);
                examHistoryModel8.setAttend_que(intExtra8);
                examHistoryModel8.setCorrect_ans(getIntent().getIntExtra(str, 0));
                examHistoryModel8.setDate(format);
                examHistoryModel8.setReuslt("असफल");
                examHistoryModel8.setWrong_ans(getIntent().getIntExtra(str2, 0));
                examHistoryModel8.setPass(0);
                Common_Utils.marathi_history_list.add(examHistoryModel8);
                this.text_you_scored.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_result.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_cong_msg.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_msg.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_attend_que.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_right_ans.setTextColor(SupportMenu.CATEGORY_MASK);
                this.id_wrongs.setTextColor(SupportMenu.CATEGORY_MASK);
                this.text_you_scored.setText("आपण स्कोअर के");
                this.text_result.setText(getIntent().getStringExtra(str3));
                this.text_cong_msg.setText("क्षमस्व! आपण चाचणी अयशस्वी");
                this.text_msg.setText("क्षमस्व! ड्रायव्हिंग लायसन्स चाचणीत आपण अयशस्वी झाला आहात");
                TextView textView22 = this.id_attend_que;
                String sb22 = "प्रश्न उपस्थित: " +
                        intExtra8;
                textView22.setText(sb22);
                TextView textView23 = this.id_right_ans;
                String sb23 = "खरे उत्तर: " +
                        getIntent().getIntExtra(str, 0);
                textView23.setText(sb23);
                TextView textView24 = this.id_wrongs;
                String sb24 = "खोटे उत्तर: " +
                        getIntent().getIntExtra(str2, 0);
                textView24.setText(sb24);
            }
            this.tinyDB.putAdListObject(Common_Utils.key_marathi_history_list, Common_Utils.marathi_history_list);
        }
        this.header.setText(this.list_item_array[3]);
        TextView textView25 = this.text_result;
        String sb25 = getIntent().getIntExtra(str, 0) +
                "/15";
        textView25.setText(sb25);
    }


    @Override
    public void onBackPressed() {
        finish();
    }


}
