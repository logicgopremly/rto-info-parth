package com.rtovehicle.information.carbikeinfo.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.mikelau.views.shimmer.ShimmerRecyclerViewX;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.UpcomingVehicleShow_Activity;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_UpcomingVehicle;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Upcoming_vehicles_fragment extends Fragment {

    ShimmerRecyclerViewX shimmerRecyclerViewX;
    MyRecyclerAdapter myAdapter;
    final ArrayList<Model_UpcomingVehicle.Upcoming_Data> upcoming_data = new ArrayList<>();

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.rto_upcoming_vehical_frgament, viewGroup, false);

        shimmerRecyclerViewX = inflate.findViewById(R.id.upcoming_recyclervidhew);
        getupcomingvehicel();
        return inflate;
    }


    public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {
        final Context context;
        private final ArrayList<Model_UpcomingVehicle.Upcoming_Data> whatsNewModelArrayList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public final TextView text_name;
            public final ImageView thumb_image;
            final LinearLayout layout;


            public MyViewHolder(View view) {
                super(view);
                this.thumb_image = view.findViewById(R.id.thumb_image);
                this.text_name = view.findViewById(R.id.text_name);
                layout = view.findViewById(R.id.click_lin);
            }
        }

        public MyRecyclerAdapter(Context context2, ArrayList<Model_UpcomingVehicle.Upcoming_Data> arrayList) {
            context = context2;
            whatsNewModelArrayList = arrayList;
        }

        @NonNull
        public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.others_new_item, viewGroup, false));
        }

        public void onBindViewHolder(final MyViewHolder myViewHolder, @SuppressLint("RecyclerView") final int i) {
            myViewHolder.setIsRecyclable(false);

            final Model_UpcomingVehicle.Upcoming_Data upcomingModel = this.whatsNewModelArrayList.get(i);


            Glide.with(this.context).load(upcomingModel.getImage()).placeholder(R.drawable.placeholder).into(myViewHolder.thumb_image);

            myViewHolder.text_name.setText(upcomingModel.getName());
            myViewHolder.text_name.setSelected(true);
            myViewHolder.layout.setOnClickListener(v -> {

                Intent intent = new Intent(getContext(), UpcomingVehicleShow_Activity.class);
                intent.putExtra("abc", i);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            });


        }

        public int getItemCount() {
            return this.whatsNewModelArrayList.size();
        }
    }

    private void getupcomingvehicel() {
        shimmerRecyclerViewX.showShimmerAdapter();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_Vehicles_rto)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_UpcomingVehicle> call = service.RTO_Upcoming_vehicle(MyApplication.MYSECRET);
        call.enqueue(new Callback<Model_UpcomingVehicle>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<Model_UpcomingVehicle> call, @NonNull retrofit2.Response<Model_UpcomingVehicle> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getUpcoming_Data_array() != null) {
                                for (int i = 0; i < response.body().getUpcoming_Data_array().size(); i++) {
                                    upcoming_data.add(response.body().getUpcoming_Data_array().get(i));
                                }
                                shimmerRecyclerViewX.setLayoutManager(new GridLayoutManager(getContext(), 2));
                                myAdapter = new MyRecyclerAdapter(getContext(), upcoming_data);
                                Common_Utils.upcominglist = upcoming_data;
                                shimmerRecyclerViewX.setAdapter(myAdapter);
                                shimmerRecyclerViewX.hideShimmerAdapter();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_UpcomingVehicle> call, @NonNull Throwable t) {
            }
        });
    }

}
