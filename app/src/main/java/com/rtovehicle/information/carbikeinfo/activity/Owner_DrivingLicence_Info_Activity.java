package com.rtovehicle.information.carbikeinfo.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

import java.util.ArrayList;
import java.util.List;

import de.codecrafters.tableview.TableDataAdapter;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.model.TableColumnWeightModel;

public class Owner_DrivingLicence_Info_Activity extends AppCompatActivity {

  public void onCreate(@Nullable Bundle bundle) {
    super.onCreate(bundle);
    setContentView(R.layout.activity_owner_licence_info);
    getWindow().setBackgroundDrawable(null);
    findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
    Intent intent = getIntent();
    ArrayList arrayList = new ArrayList<>();
    arrayList.add(new String[]{Common_Utils.DL_NUM, intent.getStringExtra(Common_Utils.DL_NUM)});
    arrayList.add(new String[]{Common_Utils.DL_DOB, intent.getStringExtra(Common_Utils.DL_DOB)});
    arrayList.add(new String[]{Common_Utils.DL_CUR_STA, intent.getStringExtra(Common_Utils.DL_CUR_STA)});
    arrayList.add(new String[]{Common_Utils.DL_NAM, intent.getStringExtra(Common_Utils.DL_NAM)});
    arrayList.add(new String[]{Common_Utils.DL_DOI, intent.getStringExtra(Common_Utils.DL_DOI)});
    arrayList.add(new String[]{Common_Utils.DL_LAS_TRA, intent.getStringExtra(Common_Utils.DL_LAS_TRA)});
    arrayList.add(new String[]{Common_Utils.DL_NON_TRA, intent.getStringExtra(Common_Utils.DL_NON_TRA)});
    arrayList.add(new String[]{Common_Utils.DL_NON_TRA_TO, intent.getStringExtra(Common_Utils.DL_NON_TRA_TO)});
    arrayList.add(new String[]{Common_Utils.DL_TRA, intent.getStringExtra(Common_Utils.DL_TRA)});
    arrayList.add(new String[]{Common_Utils.DL_TRA_TO, intent.getStringExtra(Common_Utils.DL_TRA_TO)});
    arrayList.add(new String[]{Common_Utils.DL_HAZ_VAL, intent.getStringExtra(Common_Utils.DL_HAZ_VAL)});
    arrayList.add(new String[]{Common_Utils.DL_HIL_VAL, intent.getStringExtra(Common_Utils.DL_HIL_VAL)});
    arrayList.add(new String[]{Common_Utils.DL_COV_CAT, intent.getStringExtra(Common_Utils.DL_COV_CAT)});
    arrayList.add(new String[]{Common_Utils.DL_COV, intent.getStringExtra(Common_Utils.DL_COV)});
    arrayList.add(new String[]{Common_Utils.DL_COV_ISS, intent.getStringExtra(Common_Utils.DL_COV_ISS)});

    TableView tableView = findViewById(R.id.tableView);
    LocalSimpleTableDataAdapter localSimpleTableDataAdapter = new LocalSimpleTableDataAdapter(this, (List<String[]>) arrayList);
    localSimpleTableDataAdapter.setTextSize(18);
    tableView.setDataAdapter(localSimpleTableDataAdapter);
    tableView.setHeaderVisible(false);
    TableColumnWeightModel tableColumnWeightModel = new TableColumnWeightModel(2);
    tableColumnWeightModel.setColumnWeight(0, 1);
    tableColumnWeightModel.setColumnWeight(1, 2);
    tableView.setColumnModel(tableColumnWeightModel);
  }

  private static final class LocalSimpleTableDataAdapter extends TableDataAdapter<String[]> {
    private int textSize = 18;


    public LocalSimpleTableDataAdapter(Context context, List<String[]> list) {
      super(context, list);
    }

    public View getCellView(int i, int i2, ViewGroup viewGroup) {
      TextView textView = new TextView(getContext());
      int paddingBottom = 15;
      int paddingLeft = 20;
      int paddingRight = 20;
      int paddingTop = 15;
      textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
      textView.setTextSize((float) this.textSize);
      textView.setTextColor(getResources().getColor(R.color.black));
      try {
        textView.setText(((String[]) getItem(i))[i2]);
      } catch (IndexOutOfBoundsException e) {
        StringBuilder sb = new StringBuilder();
        sb.append("No Sting given for row ");
        sb.append(i);
        sb.append(", column ");
        sb.append(i2);
        sb.append(". Caught exception: ");
        sb.append(e.toString());
      }
      return textView;
    }

    public void setTextSize(int i) {
      this.textSize = i;
    }
  }


  public boolean onOptionsItemSelected(MenuItem menuItem) {
    if (menuItem.getItemId() != 16908332) {
      return super.onOptionsItemSelected(menuItem);
    }
    onBackPressed();
    return true;
  }

  public void onBackPressed() {
    finish();
  }

}
