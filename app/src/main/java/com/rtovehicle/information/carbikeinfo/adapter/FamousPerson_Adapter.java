package com.rtovehicle.information.carbikeinfo.adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.Owner_Vehicleinfo_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_MostTrending;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

import java.util.ArrayList;


public class FamousPerson_Adapter extends RecyclerView.Adapter<FamousPerson_Adapter.myh> {
    final Context context;
    final ArrayList<Model_MostTrending.DATA_ALL_Most.vehicle_details_most> details_mostArrayList;

    public FamousPerson_Adapter(Context context, ArrayList<Model_MostTrending.DATA_ALL_Most.vehicle_details_most> favorite_arr) {
        this.context = context;
        this.details_mostArrayList = favorite_arr;
    }

    @NonNull
    @Override
    public myh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.famous_adpater, parent, false);
        return new myh(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull myh holder, @SuppressLint("RecyclerView") final int position) {

        holder.ownername_text.setText(details_mostArrayList.get(position).getName());
        holder.number_vehicle_text.setText(details_mostArrayList.get(position).getVehicle_number());

        holder.next_lin.setOnClickListener(view -> {
            Common_Utils.click_db = 3;
            Intent intent = new Intent(context, Owner_Vehicleinfo_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("famous", details_mostArrayList.get(position).getVehicle_number());
            context.startActivity(intent);
        });

    }


    @Override
    public int getItemCount() {
        return details_mostArrayList.size();
    }

    public static class myh extends RecyclerView.ViewHolder {

        final LinearLayout next_lin;
        final TextView ownername_text;
        final TextView number_vehicle_text;


        public myh(@NonNull View itemView) {
            super(itemView);
            ownername_text = itemView.findViewById(R.id.txvName);
            number_vehicle_text = itemView.findViewById(R.id.txvRegNo);
            next_lin = itemView.findViewById(R.id.next_lin);
        }
    }

}
