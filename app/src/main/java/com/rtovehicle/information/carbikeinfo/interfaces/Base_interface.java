package com.rtovehicle.information.carbikeinfo.interfaces;

import com.rtovehicle.information.carbikeinfo.model.Model_Bike;
import com.rtovehicle.information.carbikeinfo.model.Model_BikeVariantDetails;
import com.rtovehicle.information.carbikeinfo.model.Model_Btn;
import com.rtovehicle.information.carbikeinfo.model.Model_Car;
import com.rtovehicle.information.carbikeinfo.model.Model_CarVariantDetails;
import com.rtovehicle.information.carbikeinfo.model.Model_Company;
import com.rtovehicle.information.carbikeinfo.model.Model_Feedback;
import com.rtovehicle.information.carbikeinfo.model.Model_MostTrending;
import com.rtovehicle.information.carbikeinfo.model.Model_SuperVehicle;
import com.rtovehicle.information.carbikeinfo.model.Model_UpcomingVehicle;
import com.rtovehicle.information.carbikeinfo.model.Model_Vehicle;
import com.rtovehicle.information.carbikeinfo.model.Model_RtoALLOffice;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Base_interface {

    @GET("api/v2/company-list")
    Call<Model_Company> getCompanyistCar(
            @Header("AuthorizationKey") String authorizationKey
    );

    @GET("api/bike-v2/company-list")
    Call<Model_Company> getCompanyistBike(
            @Header("AuthorizationKey") String authorizationKey
    );

    @FormUrlEncoded
    @POST("api/v2/car-model-list")
    Call<Model_Car> getCarList(
            @Header("AuthorizationKey") String authorizationKey,
            @Field("company_id") int company_id
    );

    @FormUrlEncoded
    @POST("api/bike-v2/bike-model-list")
    Call<Model_Bike> getBikeList(
            @Header("AuthorizationKey") String authorizationKey,
            @Field("company_id") int company_id
    );

    @FormUrlEncoded
    @POST("api/v2/car-variant-detail")
    Call<Model_CarVariantDetails> getVariantDetailsCar(
            @Header("AuthorizationKey") String AuthorizationKey,
            @Field("variant_id") int variant_id
    );

    @FormUrlEncoded
    @POST("api/bike-v2/bike-variant-detail")
    Call<Model_BikeVariantDetails> getVariantDetailsBike(
            @Header("AuthorizationKey") String AuthorizationKey,
            @Field("variant_id") int variant_id
    );

    @POST("vehical-info2")
    Call<Model_Vehicle> getVehicle_api(
            @Header("AuthorizationKey") String AuthorizationKey,
            @Query("vehical_number") String vehical_number);

    @FormUrlEncoded
    @POST("feedback")
    Call<Model_Feedback> sendfeedback(
            @Field("app_name") String app_name,
            @Field("package_name") String package_name,
            @Field("title") String title,
            @Field("description") String description,
            @Field("device_name") String device_name,
            @Field("android_version") String android_version,
            @Field("version") String version
    );


    @GET("get-selfie-data")
    Call<Model_Btn> getBtnAd();

    @GET("/rto-info2")
    Call<Model_RtoALLOffice> RTOALL(
            @Header("AuthorizationKey") String AuthorizationKey);

    @GET("/super-vehical2")
    Call<Model_SuperVehicle> RTO_Super_vehicle(
            @Header("AuthorizationKey") String AuthorizationKey);

    @GET("/upcoming-vehical2")
    Call<Model_UpcomingVehicle> RTO_Upcoming_vehicle(
            @Header("AuthorizationKey") String AuthorizationKey);

    @GET("/celebrity-cars2")
    Call<Model_MostTrending> Most_Trending(
            @Header("AuthorizationKey") String AuthorizationKey);
}
