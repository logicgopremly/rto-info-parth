package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.R;

public class Privacy_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);
        getWindow().setBackgroundDrawable(null);
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        WebView privacypolicy = findViewById(R.id.privacypolicy);
        startWebView(privacypolicy, getString(R.string.policy));
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void startWebView(WebView webView, String url) {
        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            public void onLoadResource(WebView view, String url) {

                if (progressDialog == null) {
                    progressDialog = new ProgressDialog(Privacy_Activity.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }

            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }

}
