package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


public class SymbolsRTO_info_Activity extends AppCompatActivity {
    RecyclerView recyclerView;
    String string;
    MyAdapter myAdapter;
    HashMap<String, Integer> hashMapp;
    List<String> namelist_array;
    List<Integer> imagelist_array;
    private FrameLayout adContainerView;
    NativeAd nativeAds;


    public void onBackPressed() {
        finish();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_symbolsrto_info);
        getWindow().setBackgroundDrawable(null);
        adContainerView = findViewById(R.id.adsframe);
        NativeadsLoad();
        this.recyclerView = findViewById(R.id.recyclerViewRTOSymbolInformation);
        this.string = getIntent().getStringExtra("symbols");
        hashMapp = new HashMap<>();
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());

        if (this.string.equals("Mandatory")) {
            {
                hashMapp.put("Speed Limit", R.drawable.mandatorypic1);
                hashMapp.put("No Entry", R.drawable.mandatorypic2);
                hashMapp.put("One Way Sign, Entry Allowed", R.drawable.mandatorypic3);
                hashMapp.put("Right Turn Prohibited", R.drawable.mandatorypic4);
                hashMapp.put("Left Turn Prohibited", R.drawable.mandatorypic5);
                hashMapp.put("One Way, Entry Prohibited", R.drawable.mandatorypic6);
                hashMapp.put("Load Limit", R.drawable.mandatorypic7);
                hashMapp.put("Vehicle Prohibited in Both Directions", R.drawable.mandatorypic8);
                hashMapp.put("Horn Prohibited", R.drawable.mandatorypic9);
                hashMapp.put("U-Turn Prohibited", R.drawable.mandatorypic10);
                hashMapp.put("Overtaking Prohibited", R.drawable.mandatorypic11);
                hashMapp.put("No Parking", R.drawable.mandatorypic12);
                hashMapp.put("Width Limit", R.drawable.mandatorypic13);
                hashMapp.put("Height Limit", R.drawable.mandatorypic14);
                hashMapp.put("No Stopping or Standing", R.drawable.mandatorypic15);
                hashMapp.put("Restriction Ends", R.drawable.mandatorypic16);
                hashMapp.put("Stop", R.drawable.mandatorypic17);
                hashMapp.put("Compulsory Turn Right", R.drawable.mandatorypic18);
                hashMapp.put("Compulsory Turn Left", R.drawable.mandatorypic19);
                hashMapp.put("Compulsory Ahead Only", R.drawable.mandatorypic20);
                hashMapp.put("Compulsory Keep Left", R.drawable.mandatorypic21);
                hashMapp.put("Compulsory Ahead or Turn Left", R.drawable.mandatorypic22);
                hashMapp.put("Compulsory Ahead or Turn Right ", R.drawable.mandatorypic23);
                hashMapp.put("Compulsory Sound Horn", R.drawable.mandatorypic24);
                hashMapp.put("Give Way", R.drawable.mandatorypic25);
            }

            namelist_array = new ArrayList<>(hashMapp.keySet());
            imagelist_array = new ArrayList<>(hashMapp.values());
            this.recyclerView.setHasFixedSize(true);
            this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            myAdapter = new MyAdapter();
            this.recyclerView.setAdapter(myAdapter);
            return;
        }

        switch (this.string) {
            case "Cautionary": {
                hashMapp.put("Right Hair Pin Bend", R.drawable.cautionarypic1);
                hashMapp.put("Left Hair Pin Bend", R.drawable.cautionarypic2);
                hashMapp.put("Right Hand Curve", R.drawable.cautionarypic3);
                hashMapp.put("Left Hand Curve", R.drawable.cautionarypic4);
                hashMapp.put("Left Reverse Bend", R.drawable.cautionarypic5);
                hashMapp.put("Right Reverse Bend", R.drawable.cautionarypic6);
                hashMapp.put("Side Road Left", R.drawable.cautionarypic7);
                hashMapp.put("Side Road Right", R.drawable.cautionarypic8);
                hashMapp.put("T-intersection", R.drawable.cautionarypic9);
                hashMapp.put("Major Road Ahead", R.drawable.cautionarypic10);
                hashMapp.put("Staggered Intersection", R.drawable.cautionarypic11);
                hashMapp.put("Staggered Intersection", R.drawable.cautionarypic12);
                hashMapp.put("Cross Road Ahead", R.drawable.cautionarypic13);
                hashMapp.put("Right Y-Intersection", R.drawable.cautionarypic14);
                hashMapp.put("Left Y-Intersection", R.drawable.cautionarypic15);
                hashMapp.put("Two Way Traffic", R.drawable.cautionarypic16);
                hashMapp.put("Y-Intersection", R.drawable.cautionarypic17);
                hashMapp.put("Roundabout Ahead", R.drawable.cautionarypic18);
                hashMapp.put("Gap In Median", R.drawable.cautionarypic19);
                hashMapp.put("Pedestrian Crossing", R.drawable.cautionarypic20);
                hashMapp.put("Narrow Bridge Ahead", R.drawable.cautionarypic21);
            }

            namelist_array = new ArrayList<>(hashMapp.keySet());
            imagelist_array = new ArrayList<>(hashMapp.values());
            this.recyclerView.setHasFixedSize(true);
            this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            myAdapter = new MyAdapter();
            this.recyclerView.setAdapter(myAdapter);

            break;
            case "Informatory": {
                hashMapp.put("Public telephone", R.drawable.info1);
                hashMapp.put("Petrol pump", R.drawable.info2);
                hashMapp.put("Hospital", R.drawable.info3);
                hashMapp.put("Eating Place", R.drawable.info4);
                hashMapp.put("Light Refreshment", R.drawable.info5);
                hashMapp.put("No Through Road", R.drawable.info6);
                hashMapp.put("No Through Side Road", R.drawable.info7);
                hashMapp.put("First Aid Post", R.drawable.info8);
                hashMapp.put("Park This Side", R.drawable.info9);
                hashMapp.put("Parking Both Sides", R.drawable.info10);
                hashMapp.put("Parking Lot Bikes", R.drawable.info11);
                hashMapp.put("Parking Lot Cycles", R.drawable.info12);
                hashMapp.put("Parking Lot Taxis", R.drawable.info13);
                hashMapp.put("Parking Lot Auto", R.drawable.info14);
            }


            namelist_array = new ArrayList<>(hashMapp.keySet());
            imagelist_array = new ArrayList<>(hashMapp.values());
            this.recyclerView.setHasFixedSize(true);
            this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            myAdapter = new MyAdapter();
            this.recyclerView.setAdapter(myAdapter);

            break;
            case "Road AND Signals": {
                hashMapp.put("Centre Line Marking For A Two Lane Road", R.drawable.roadtype1);
                hashMapp.put("Lane Line And Broken Centre Line", R.drawable.roadtype2);
                hashMapp.put("Centre Barrier Line Marking For A Four Lane Road", R.drawable.roadtype3);
                hashMapp.put("Centre Barrier Line Marking For A Six Lane Road", R.drawable.roadtype4);
                hashMapp.put("Double White/Yellow Lines: Used where visibility is restricted in both directions.", R.drawable.roadtype5);
                hashMapp.put("Combination Of Solid And Broken Lines:If the line on your side is broken,you may cross or straddle it.OverTake - but only if it is safe to do so. If the line on your side is continious you must not cross or straddle it.", R.drawable.roadtype6);
                hashMapp.put("Stop Line: A stop line is as single solid transverse line painted before the intersecting edge of the road junction/intersection.", R.drawable.roadtype7);
                hashMapp.put("Give Way Line: The give way line is usually a double dotted line marked transversely at junctions.", R.drawable.roadtype8);
                hashMapp.put("Border or Edge Lines: These are continuous lines at the edge of the carriageway and mark the limits of the main carriageway upto which a driver can safely venture.", R.drawable.roadtype9);
                hashMapp.put("Parking prohibited Lines:A Solid continuous yellow line painted on the kerb or edge of the carriageway along with a No-parking sign indicates the extent of no-parking area.", R.drawable.roadtype10);
                hashMapp.put("Yellow Box Junctions or Keep Clear:These are yellow crossed diagonal lines within the box.The vehicles should cross it only if they have a clear space available ahead of the yellow box. In this marked area vehicles must not stop even briefly.", R.drawable.roadtype11);
                hashMapp.put("Pedestrian Crossings These are alternate black and white stripes painted parallel to the road generally known as zebra crossing.Pedestrians must cross only at the point where these lines are provided and when the signal is in thier favour at controlled crossing.You must stop and give way to pedestrians at these crossing.Pedestrians crossing are marked to facilitate of way to pedestrians.", R.drawable.roadtype12);
            }


            namelist_array = new ArrayList<>(hashMapp.keySet());
            imagelist_array = new ArrayList<>(hashMapp.values());
            this.recyclerView.setHasFixedSize(true);
            this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            myAdapter = new MyAdapter();
            this.recyclerView.setAdapter(myAdapter);

            break;
            case "Driving Rules": {
                hashMapp.put("I intend to move in to the left or turn left.", R.drawable.rules1);
                hashMapp.put("I intend to move out of the right or changing the lane or turn right.", R.drawable.rules2);
                hashMapp.put("I intend to stop.", R.drawable.rules3);
                hashMapp.put("I intend to slow down.", R.drawable.rules4);
                hashMapp.put("Indicating the car following you to over take.", R.drawable.rules5);
            }


            namelist_array = new ArrayList<>(hashMapp.keySet());
            imagelist_array = new ArrayList<>(hashMapp.values());
            this.recyclerView.setHasFixedSize(true);
            this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            myAdapter = new MyAdapter();
            this.recyclerView.setAdapter(myAdapter);

            break;
            case "Traffic Police Signals": {
                hashMapp.put("To stop vehicles approaching simultaneously from front and behind", R.drawable.police1);
                hashMapp.put("To allow vehicles coming from right and turing right by stopping traffic approaching from the left", R.drawable.police2);
                hashMapp.put("To beckon the vehicles approaching from right", R.drawable.police3);
                hashMapp.put("To beckon the vehicles approaching from left", R.drawable.police4);
                hashMapp.put("To stop vehicles aproaching from left and waiting to turn right", R.drawable.police5);
                hashMapp.put("To stop vehicles coming from front", R.drawable.police6);
                hashMapp.put("To stop vehicles approaching from behind", R.drawable.police7);
                hashMapp.put("To stop vehicles approaching from right to allow vehicles from the left to turn right", R.drawable.police8);
                hashMapp.put("Warning signal closing all vehicles", R.drawable.police9);
            }


            namelist_array = new ArrayList<>(hashMapp.keySet());
            imagelist_array = new ArrayList<>(hashMapp.values());
            this.recyclerView.setHasFixedSize(true);
            this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
            myAdapter = new MyAdapter();
            this.recyclerView.setAdapter(myAdapter);

            break;
            default:
        }
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public final TextView textView;
            public final ImageView imageView;

            public MyViewHolder(View v) {
                super(v);
                textView = v.findViewById(R.id.name);
                imageView = v.findViewById(R.id.sign);
            }
        }

        public MyAdapter() {

        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.symbol_item_layout, parent, false);
            return new MyViewHolder(v);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.textView.setText(namelist_array.get(position));
            holder.imageView.setImageResource(imagelist_array.get(position));
        }

        @Override
        public int getItemCount() {
            return hashMapp.size();
        }
    }

    private void NativeadsLoad() {

        AdLoader.Builder builder = new AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
                .forNativeAd(nativeAd -> {

                    nativeAds = nativeAd;

                    @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) getLayoutInflater()
                            .inflate(R.layout.small_native, null);
                    populateUnifiedNativeAdView(nativeAd, adView);
                    adContainerView.removeAllViews();
                    adContainerView.addView(adView);

                });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull @NotNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();

        adLoader.loadAd(new AdRequest.Builder().build());

    }


    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (nativeAds != null) {
            nativeAds.destroy();
        }
    }
}
