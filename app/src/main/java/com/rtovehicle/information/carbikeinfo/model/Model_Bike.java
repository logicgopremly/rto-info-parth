package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_Bike {
    @SerializedName("status")
    boolean status;

    @SerializedName("data")
    ArrayList<Bike_Data> data;

    @SerializedName("url")
    String url;

    public static class Bike_Data {
        @SerializedName("id")
        int id;

        @SerializedName("model_name")
        String model_name;

        @SerializedName("model_price")
        String model_price;

        @SerializedName("model_image")
        String model_image;

        @SerializedName("model_color")
        String[] model_color;

        @SerializedName("model_images")
        String[] model_images;

        @SerializedName("model_variants")
        String model_variants;

        public int getId() {
            return id;
        }

        public String getModel_name() {
            return model_name;
        }

        public String getModel_price() {
            return model_price;
        }

        public String getModel_image() {
            return model_image;
        }

        public String[] getModel_color() {
            return model_color;
        }

        public String[] getModel_images() {
            return model_images;
        }

        public String getModel_variants() {
            return model_variants;
        }
    }

    public boolean isStatus() {
        return status;
    }

    public ArrayList<Bike_Data> getData() {
        return data;
    }

    public String getUrl() {
        return url;
    }
}
