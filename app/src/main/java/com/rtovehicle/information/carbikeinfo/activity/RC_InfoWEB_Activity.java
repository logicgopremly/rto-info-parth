package com.rtovehicle.information.carbikeinfo.activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

public class RC_InfoWEB_Activity extends AppCompatActivity {

    int check;
    private FrameLayout adContainerView;
    private AdView adView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rc_infoweb);
        getWindow().setBackgroundDrawable(null);
        adContainerView = findViewById(R.id.adsframe);
        WebView webView = findViewById(R.id.web_view);

        Banner();
        check = getIntent().getIntExtra("number", 0);
        findViewById(R.id.back).setOnClickListener(v -> onBackPressed());
        String name = getIntent().getStringExtra("name");
        ((TextView) findViewById(R.id.texts)).setText(name);


        if (check == 1) {
            webView.loadUrl("file:///android_asset/rc1.html");
        }
        if (check == 2) {
            webView.loadUrl("file:///android_asset/rc2.html");

        }
        if (check == 3) {
            webView.loadUrl("file:///android_asset/rc3.html");

        }
        if (check == 4) {
            webView.loadUrl("file:///android_asset/rc4.html");

        }
        if (check == 5) {
            webView.loadUrl("file:///android_asset/rc5.html");

        }
        if (check == 6) {
            webView.loadUrl("file:///android_asset/rc6.html");

        }
        if (check == 7) {
            webView.loadUrl("file:///android_asset/rc7.html");

        }
        if (check == 8) {
            webView.loadUrl("file:///android_asset/rc8.html");

        }
        if (check == 9) {
            webView.loadUrl("file:///android_asset/rc9.html");

        }
        if (check == 10) {
            webView.loadUrl("file:///android_asset/rc10.html");

        }
        if (check == 11) {
            webView.loadUrl("file:///android_asset/rc11.html");

        }
        if (check == 12) {
            webView.loadUrl("file:///android_asset/rc12.html");

        }
        if (check == 13) {
            webView.loadUrl("file:///android_asset/rc13.html");

        }
        if (check == 14) {
            webView.loadUrl("file:///android_asset/rc14.html");

        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    public void Banner() {
        adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        adContainerView.addView(adView);
        loadBanner();
    }

    private void loadBanner() {

        AdRequest adRequest =
                new AdRequest.Builder()
                        .build();

        AdSize adSize = getAdSize();
        adView.setAdSize(adSize);
        adView.loadAd(adRequest);
    }

    private AdSize getAdSize() {

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }


}
