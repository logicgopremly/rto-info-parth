package com.rtovehicle.information.carbikeinfo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.Fuel_activity;
import com.rtovehicle.information.carbikeinfo.model.Model_City;

import java.util.ArrayList;
import java.util.List;

public class City_Adapter extends RecyclerView.Adapter<City_Adapter.Holder> {
    private final Context context;
    public final List<Model_City> listState;
    public OnClickedState onClickedState;
    private final List<Model_City> tempList;

    public interface OnClickedState {
        void callBack(Model_City modelCity);
    }

    static class Holder extends RecyclerView.ViewHolder {

        public final RelativeLayout rl_state;
        public final TextView textView;

        public Holder(View view) {
            super(view);
            this.textView = view.findViewById(R.id.tv_state);
            this.rl_state = view.findViewById(R.id.rl_state);
        }
    }

    public City_Adapter(Context context2, List<Model_City> list, Fuel_activity activityBase) {
        ArrayList arrayList = new ArrayList<>();
        this.tempList = arrayList;
        this.context = context2;
        this.listState = list;
        arrayList.addAll(list);
    }

    public void calBack(OnClickedState onClickedState2) {
        this.onClickedState = onClickedState2;
    }

    @NonNull
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(this.context).inflate(R.layout.state_item_layout, viewGroup, false));
    }

    public void onBindViewHolder(Holder holder, int i) {
        holder.textView.setText(this.listState.get(i).getCity());
        final Model_City modelCity = this.listState.get(i);
        holder.rl_state.setOnClickListener(view -> City_Adapter.this.onClickedState.callBack(modelCity));
    }

    public int getItemCount() {
        return this.listState.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void filter(CharSequence charSequence) {
        ArrayList arrayList = new ArrayList<>();
        if (TextUtils.isEmpty(charSequence)) {
            arrayList.addAll(this.tempList);
        } else {
            for (Model_City next : this.listState) {
                if (next.getCity().toLowerCase().contains(charSequence)) {
                    arrayList.add(next);
                }
            }
        }
        this.listState.clear();
        this.listState.addAll(arrayList);
        notifyDataSetChanged();
        arrayList.clear();
    }
}
