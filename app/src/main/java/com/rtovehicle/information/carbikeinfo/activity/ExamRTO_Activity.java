package com.rtovehicle.information.carbikeinfo.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

public class ExamRTO_Activity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_exam_rto);
        getWindow().setBackgroundDrawable(null);
        LoadAdInterstitial();
        findViewById(R.id.back_all).setOnClickListener(view -> ExamRTO_Activity.this.onBackPressed());

        final Intent intent = new Intent(this, ExamOption_Activity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        findViewById(R.id.ll_Lang_english).setOnClickListener(view -> {
            intent.putExtra("lang", "english");
            ExamRTO_Activity.this.startActivity(intent);
            try {
                if (mInterstitialAd != null) {
                    mInterstitialAd.show(ExamRTO_Activity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.ll_Lang_hindi).setOnClickListener(view -> {
            intent.putExtra("lang", "hindi");
            ExamRTO_Activity.this.startActivity(intent);
            try {
                if (mInterstitialAd != null) {
                    MyApplication.appOpenManager.isAdShow = true;
                    mInterstitialAd.show(ExamRTO_Activity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.ll_Lang_marathi).setOnClickListener(view -> {
            intent.putExtra("lang", "marathi");
            ExamRTO_Activity.this.startActivity(intent);
            try {
                if (mInterstitialAd != null) {
                    MyApplication.appOpenManager.isAdShow = true;
                    mInterstitialAd.show(ExamRTO_Activity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        findViewById(R.id.ll_Lang_gujarati).setOnClickListener(view -> {
            intent.putExtra("lang", "gujarati");
            ExamRTO_Activity.this.startActivity(intent);
            try {
                if (mInterstitialAd != null) {
                    MyApplication.appOpenManager.isAdShow = true;
                    mInterstitialAd.show(ExamRTO_Activity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, MyApplication.get_Admob_interstitial_Id(), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;

                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;

            }
        });
    }


}
