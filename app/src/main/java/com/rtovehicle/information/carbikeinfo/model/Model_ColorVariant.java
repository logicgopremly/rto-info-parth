package com.rtovehicle.information.carbikeinfo.model;

public class Model_ColorVariant {
    String hexCode;

    public String getHexCode() {
        return hexCode;
    }

    public void setHexCode(String hexCode) {
        this.hexCode = hexCode;
    }
}
