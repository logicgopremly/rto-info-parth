package com.rtovehicle.information.carbikeinfo.activity;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.CarVariantsinfo_Adapter;
import com.rtovehicle.information.carbikeinfo.interfaces.Base_interface;
import com.rtovehicle.information.carbikeinfo.model.Model_CarVariantDetails;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CarVariantInformation_Activity extends AppCompatActivity {

    Toolbar herder;
    String text_name;
    int variants_id;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carvariantinformation);
        getWindow().setBackgroundDrawable(null);
        init();
        getVariantDetails(variants_id);
    }

    private void init() {
        herder = findViewById(R.id.toolbar);
        text_name = getIntent().getStringExtra(CarVariantsinfo_Adapter.VARIANT_NAME);
        variants_id = getIntent().getIntExtra(CarVariantsinfo_Adapter.VARIANT_ID, -1);
        herder.setNavigationIcon(ContextCompat.getDrawable(this, R.drawable.back_rto_new_1));
        ((TextView) herder.findViewById(R.id.app_title)).setText(text_name);
        setSupportActionBar(herder);
        herder.setNavigationOnClickListener(v -> onBackPressed());
    }


    private void getVariantDetails(int varient_id) {
        ShowDialog();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Common_Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Base_interface service = retrofit.create(Base_interface.class);
        Call<Model_CarVariantDetails> call = service.getVariantDetailsCar(MyApplication.MYSECRET, varient_id);

        call.enqueue(new Callback<Model_CarVariantDetails>() {
            @Override
            public void onResponse(@NonNull Call<Model_CarVariantDetails> call, @NonNull Response<Model_CarVariantDetails> response) {
                if (dialog.isShowing())
                    dialog.dismiss();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            Model_CarVariantDetails.Variant_Data.Engine engine = response.body().getData().get(0).getEngine();
                            setEngineDetails(engine);
                            Model_CarVariantDetails.Variant_Data.Fuel fuel = response.body().getData().get(0).getFuel();
                            setFuelDetails(fuel);
                            Model_CarVariantDetails.Variant_Data.Suspension suspension = response.body().getData().get(0).getSuspension();
                            setSuspensionDetails(suspension);
                            Model_CarVariantDetails.Variant_Data.Dimension dimension = response.body().getData().get(0).getDimension();
                            setDimensionDetails(dimension);
                            Model_CarVariantDetails.Variant_Data.OtherFeatures otherFeatures = response.body().getData().get(0).getOtherfeatures();
                            setOtherFeaturesDetails(otherFeatures);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Model_CarVariantDetails> call, @NonNull Throwable t) {
                if (dialog.isShowing())
                    dialog.dismiss();
            }
        });
    }

    private void setEngineDetails(Model_CarVariantDetails.Variant_Data.Engine engine) {
        ((TextView) findViewById(R.id.txtEngineType)).setText(engine.getEngineType());
        ((TextView) findViewById(R.id.txtFastCharging)).setText(engine.getFastCharging());
        ((TextView) findViewById(R.id.txtDisplacement)).setText(engine.getDisplacement());
        ((TextView) findViewById(R.id.txtMaxPower)).setText(engine.getMaxPower());
        ((TextView) findViewById(R.id.txtMaxTorque)).setText(engine.getMaxTorque());
        ((TextView) findViewById(R.id.txtNoOfCylinder)).setText(engine.getNoOfCylinder());
        ((TextView) findViewById(R.id.txtValvesPerCylinder)).setText(engine.getValvesPerCylinder());
        ((TextView) findViewById(R.id.txtValveConfiguration)).setText(engine.getValveConfiguration());
        ((TextView) findViewById(R.id.txtFuelSupplySystem)).setText(engine.getFuelSupplySystem());
        ((TextView) findViewById(R.id.txtTurboCharger)).setText(engine.getTurboCharger());
        ((TextView) findViewById(R.id.txtSuperCharge)).setText(engine.getSuperCharge());
        ((TextView) findViewById(R.id.txtTransmissionType)).setText(engine.getTransmissionType());
        ((TextView) findViewById(R.id.txtGearBox)).setText(engine.getGearBox());
        ((TextView) findViewById(R.id.txtMildHybrid)).setText(engine.getMildHybrid());
        ((TextView) findViewById(R.id.driveType)).setText(engine.getDriveType());
    }

    private void setFuelDetails(Model_CarVariantDetails.Variant_Data.Fuel fuel) {
        ((TextView) findViewById(R.id.txtFuelType)).setText(fuel.getFuelType());
        ((TextView) findViewById(R.id.txtMileage)).setText(fuel.getMileage());
        ((TextView) findViewById(R.id.txtFuelTankCapacity)).setText(fuel.getFuelTankCapacity());
        ((TextView) findViewById(R.id.txtEmissionNormCompliance)).setText(fuel.getEmissionNormCompliance());
        ((TextView) findViewById(R.id.txtTopSpeed)).setText(fuel.getTopSpeed());
    }

    private void setSuspensionDetails(Model_CarVariantDetails.Variant_Data.Suspension suspension) {
        ((TextView) findViewById(R.id.txtFrontSuspension)).setText(suspension.getFrontSuspension());
        ((TextView) findViewById(R.id.txtRearSuspension)).setText(suspension.getRearSuspension());
        ((TextView) findViewById(R.id.txtSteeringType)).setText(suspension.getSteeringType());
        ((TextView) findViewById(R.id.txtSteeringColumn)).setText(suspension.getSteeringColumn());
        ((TextView) findViewById(R.id.txtSteeringGearType)).setText(suspension.getSteeringGearType());
        ((TextView) findViewById(R.id.txtTurningRadius)).setText(suspension.getTurningRadius());
        ((TextView) findViewById(R.id.txtFrontBrakeType)).setText(suspension.getFrontBrakeType());
        ((TextView) findViewById(R.id.txtRearBrakeType)).setText(suspension.getRearBrakeType());
        ((TextView) findViewById(R.id.txtAcceleration)).setText(suspension.getAcceleration());
        ((TextView) findViewById(R.id.txtZeroToHundredkmph)).setText(suspension.getZeroToHundredkmph());
    }

    private void setDimensionDetails(Model_CarVariantDetails.Variant_Data.Dimension dimension) {
        ((TextView) findViewById(R.id.txtLength)).setText(dimension.getLength());
        ((TextView) findViewById(R.id.txtWidth)).setText(dimension.getWidth());
        ((TextView) findViewById(R.id.txtHeight)).setText(dimension.getHeight());
        ((TextView) findViewById(R.id.txtBootSpace)).setText(dimension.getBootSpace());
        ((TextView) findViewById(R.id.txtSeatingCapacity)).setText(dimension.getSeatingCapacity());
        ((TextView) findViewById(R.id.txtGroundClearanceUnladen)).setText(dimension.getGroundClearanceUnladen());
        ((TextView) findViewById(R.id.txtWheelBase)).setText(dimension.getWheelBase());
        ((TextView) findViewById(R.id.txtFrontTread)).setText(dimension.getFrontTread());
        ((TextView) findViewById(R.id.txtRearTread)).setText(dimension.getRearTread());
        ((TextView) findViewById(R.id.txtKerbWeight)).setText(dimension.getKerbWeight());
        ((TextView) findViewById(R.id.txtNoOfDoors)).setText(dimension.getNoOfDoors());
    }

    private void setOtherFeaturesDetails(Model_CarVariantDetails.Variant_Data.OtherFeatures otherFeatures) {
        ((TextView) findViewById(R.id.txtParkingSensors)).setText(otherFeatures.getParkingSensors());
        ((TextView) findViewById(R.id.txtFoldablerearSeat)).setText(otherFeatures.getFoldablerearSeat());
        ((TextView) findViewById(R.id.txtTrunkOpener)).setText(otherFeatures.getTrunkOpener());
        ((TextView) findViewById(R.id.txtTyreSize)).setText(otherFeatures.getTyreSize());
        ((TextView) findViewById(R.id.txtTyreType)).setText(otherFeatures.getTyreType());
        ((TextView) findViewById(R.id.txtWheelSize)).setText(otherFeatures.getWheelSize());
        ((TextView) findViewById(R.id.txtAdditionalFeatures)).setText(otherFeatures.getAdditionalFeatures());
        ((TextView) findViewById(R.id.txtNoOfAirbags)).setText(otherFeatures.getNoOfAirbags());
        ((TextView) findViewById(R.id.txtAdvanceSafetyFeatures)).setText(otherFeatures.getAdvanceSafetyFeatures());
        ((TextView) findViewById(R.id.txtAntiPinchPowerWindows)).setText(otherFeatures.getAntiPinchPowerWindows());
    }

    private void ShowDialog() {
        // TODO Auto-generated method stub
        dialog = new Dialog(CarVariantInformation_Activity.this,
                android.R.style.Theme_Material_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_progress);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
        dialog.show();
    }
}