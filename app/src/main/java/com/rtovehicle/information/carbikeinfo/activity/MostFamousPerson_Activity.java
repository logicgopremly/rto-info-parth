package com.rtovehicle.information.carbikeinfo.activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.FamousPerson_Adapter;
import com.rtovehicle.information.carbikeinfo.adapter.MostTrendingPerson_Adapter;
import com.rtovehicle.information.carbikeinfo.model.Model_MostTrending;

import java.util.ArrayList;


public class MostFamousPerson_Activity extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<Model_MostTrending.DATA_ALL_Most.vehicle_details_most> detailsMostArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rto_mostfamous_person);
        getWindow().setBackgroundDrawable(null);
        findviewId();
    }

    public void findviewId() {
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        ((TextView) findViewById(R.id.first)).setText(getIntent().getStringExtra("txt_name"));
        recyclerView = findViewById(R.id.most_trending);
        detailsMostArrayList = MostTrendingPerson_Adapter.v_array;
        recyclerView.setLayoutManager(new LinearLayoutManager(MostFamousPerson_Activity.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new FamousPerson_Adapter(MostFamousPerson_Activity.this, detailsMostArrayList));
    }

    @Override
    public void onBackPressed() {
        detailsMostArrayList.clear();
        MostTrendingPerson_Adapter.v_array.clear();
        finish();
    }
}