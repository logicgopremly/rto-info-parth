package com.rtovehicle.information.carbikeinfo.model;

public class Model_Feedback {

    boolean status;
    String message;

    public String getMessage() {
        return message;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
