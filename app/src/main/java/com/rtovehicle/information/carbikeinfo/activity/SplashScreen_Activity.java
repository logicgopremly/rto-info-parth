package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.ThreeBounce;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.admodels.APIInterFace;
import com.rtovehicle.information.carbikeinfo.admodels.AdsModel;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SplashScreen_Activity extends AppCompatActivity {
    private InterstitialAd mInterstitialAd;
    boolean isActivityLeft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobileAds.initialize(this, initializationStatus -> {
        });
        getAdsData();

        MyApplication.appOpenManager.isAdShow = true;
        isActivityLeft = false;
        LoadAdInterstitial();
        runOnUiThread(this::setAnimation);

        new Handler().postDelayed(() -> {
            startActivity(new Intent(SplashScreen_Activity.this, Start_Activity.class));
            try {
                if (mInterstitialAd != null && !isActivityLeft) {
                    mInterstitialAd.show(SplashScreen_Activity.this);
                    MyApplication.appOpenManager.isAdShow = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            finish();
        }, 5000);
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void setAnimation() {
        RelativeLayout relativeLayout = new RelativeLayout(this);
        ProgressBar progressBar = new ProgressBar(this, null, 16842874);
        Sprite doubleBounce = new ThreeBounce();
        doubleBounce.setColor(getResources().getColor(R.color.black));
        progressBar.setIndeterminateDrawable(doubleBounce);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(110, 110);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        layoutParams.setMargins(0, 0, 0, 50);
        relativeLayout.addView(progressBar, layoutParams);
        setContentView(relativeLayout);
    }

    @Override
    public void onBackPressed() {

    }

    public void getAdsData() {
        Retrofit.Builder retrofit = new Retrofit.Builder().baseUrl(APIInterFace.adsAPI).addConverterFactory(GsonConverterFactory.create());
        APIInterFace service = retrofit.build().create(APIInterFace.class);
        Call<AdsModel> call = service.getads(24);
        call.enqueue(new Callback<AdsModel>() {
            @Override
            public void onResponse(@NonNull Call<AdsModel> call, @NonNull Response<AdsModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().isStatus()) {
                            if (response.body().getData() != null) {
                                MyApplication.set_AdsInt(response.body().getData().getPublisher_id());
                                if (response.body().getData().getPublishers() != null) {
                                    if (MyApplication.get_AdsInt() == 1) {
                                        if (response.body().getData().getPublishers().getAdmob() != null) {
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_interstitial() != null) {
                                                MyApplication.set_Admob_interstitial_Id(response.body().getData().getPublishers().getAdmob().getAdmob_interstitial().getAdmob_interstitial_id());
                                            } else {
                                                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_native() != null) {
                                                MyApplication.set_Admob_native_Id(response.body().getData().getPublishers().getAdmob().getAdmob_native().getAdmob_Native_id());
                                            } else {
                                                MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_banner() != null) {
                                                MyApplication.set_Admob_banner_Id(response.body().getData().getPublishers().getAdmob().getAdmob_banner().getAdmob_banner_id());
                                            } else {
                                                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                            }
                                            if (response.body().getData().getPublishers().getAdmob().getAdmob_open() != null) {
                                                MyApplication.set_Admob_openapp(response.body().getData().getPublishers().getAdmob().getAdmob_open().getAdmob_Open_id());
                                            } else {
                                                MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                                            }

                                        } else {
                                            MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                            MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                            MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                                            MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                                        }
                                    } else {
                                        MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                        MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                        MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                                        MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                                    }
                                } else {
                                    MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                    MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                    MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                                    MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                                }
                            } else {
                                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                                MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                                MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                            }
                        } else {
                            MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                            MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                            MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                            MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                        }
                    } else {
                        MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                        MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                        MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                        MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                    }
                } else {
                    MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                    MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                    MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                    MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
                }
            }

            @Override
            public void onFailure(@NonNull Call<AdsModel> call, @NonNull Throwable t) {
                MyApplication.set_Admob_banner_Id(getResources().getString(R.string.banner_id));
                MyApplication.set_Admob_native_Id(getResources().getString(R.string.native_ad));
                MyApplication.set_Admob_interstitial_Id(getResources().getString(R.string.interstitial));
                MyApplication.set_Admob_openapp(getResources().getString(R.string.open_app_ad_id));
            }
        });
    }

    public void LoadAdInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        InterstitialAd.load(this, getResources().getString(
                R.string.interstitial), adRequest, new InterstitialAdLoadCallback() {
            @Override
            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                mInterstitialAd = interstitialAd;
                mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                    @Override
                    public void onAdDismissedFullScreenContent() {
                        MyApplication.appOpenManager.isAdShow = false;
                        mInterstitialAd = null;
                    }

                    @Override
                    public void onAdFailedToShowFullScreenContent(@NonNull AdError adError) {
                        MyApplication.appOpenManager.isAdShow = false;
                    }

                    @Override
                    public void onAdShowedFullScreenContent() {
                        mInterstitialAd = null;
                        MyApplication.appOpenManager.isAdShow = true;
                    }
                });
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                mInterstitialAd = null;
                MyApplication.appOpenManager.isAdShow = false;

            }
        });
    }

    public void onResume() {
        super.onResume();
        this.isActivityLeft = false;
    }

    public void onPause() {
        super.onPause();
        this.isActivityLeft = true;
    }

    protected void onStop() {
        super.onStop();
        this.isActivityLeft = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.isActivityLeft = true;
    }


}
