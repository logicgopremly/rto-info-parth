package com.rtovehicle.information.carbikeinfo.model;


public class Model_Btn {
    public boolean status;
    public Data data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        public String title;
        public String url;
        public boolean flage;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public boolean isFlage() {
            return flage;
        }

        public void setFlage(boolean flage) {
            this.flage = flage;
        }
    }

}
