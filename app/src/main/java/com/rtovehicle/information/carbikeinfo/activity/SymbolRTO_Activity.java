package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;


public class SymbolRTO_Activity extends AppCompatActivity {

    private FrameLayout adContainerView;
    NativeAd nativeAds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symbolrto);
        getWindow().setBackgroundDrawable(null);
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        adContainerView = findViewById(R.id.adsframe);
        NativeadsLoad();
        click();
    }

    public void click() {
        findViewById(R.id.relativeMandatory).setOnClickListener(view -> {
            Intent intent = new Intent(SymbolRTO_Activity.this, SymbolsRTO_info_Activity.class);
            intent.putExtra("symbols", "Mandatory");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            SymbolRTO_Activity.this.startActivity(intent);
        });
        findViewById(R.id.relativeCautionary).setOnClickListener(view -> {
            Intent intent = new Intent(SymbolRTO_Activity.this, SymbolsRTO_info_Activity.class);
            intent.putExtra("symbols", "Cautionary");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            SymbolRTO_Activity.this.startActivity(intent);
        });
        findViewById(R.id.relativeInformatory).setOnClickListener(view -> {
            Intent intent = new Intent(SymbolRTO_Activity.this, SymbolsRTO_info_Activity.class);
            intent.putExtra("symbols", "Informatory");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            SymbolRTO_Activity.this.startActivity(intent);
        });
        findViewById(R.id.relativRoadSignals).setOnClickListener(view -> {
            Intent intent = new Intent(SymbolRTO_Activity.this, SymbolsRTO_info_Activity.class);
            intent.putExtra("symbols", "Road AND Signals");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            SymbolRTO_Activity.this.startActivity(intent);
        });
        findViewById(R.id.relativeDrivingRules).setOnClickListener(view -> {
            Intent intent = new Intent(SymbolRTO_Activity.this, SymbolsRTO_info_Activity.class);
            intent.putExtra("symbols", "Driving Rules");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            SymbolRTO_Activity.this.startActivity(intent);
        });
        findViewById(R.id.relativeTrafficPoliceSignals).setOnClickListener(view -> {
            Intent intent = new Intent(SymbolRTO_Activity.this, SymbolsRTO_info_Activity.class);
            intent.putExtra("symbols", "Traffic Police Signals");
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            SymbolRTO_Activity.this.startActivity(intent);
        });

    }

    private void NativeadsLoad() {

        AdLoader.Builder builder = new AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
                .forNativeAd(nativeAd -> {

                    nativeAds = nativeAd;

                    @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) getLayoutInflater()
                            .inflate(R.layout.small_native, null);
                    populateUnifiedNativeAdView(nativeAd, adView);
                    adContainerView.removeAllViews();
                    adContainerView.addView(adView);

                });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull @NotNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();

        adLoader.loadAd(new AdRequest.Builder().build());


    }


    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (nativeAds != null) {
            nativeAds.destroy();
        }
    }
}
