package com.rtovehicle.information.carbikeinfo.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.RC_info_Adapter;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

public class RC_Info_Activity extends AppCompatActivity {
    RecyclerView reconfirmation;
    RC_info_Adapter rInformationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rc_info);
        getWindow().setBackgroundDrawable(null);
        findId();
    }

    public void findId() {
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        reconfirmation = findViewById(R.id.rcinformation);
        reconfirmation.setLayoutManager(new GridLayoutManager(RC_Info_Activity.this, 3));
        rInformationAdapter = new RC_info_Adapter(RC_Info_Activity.this, Common_Utils.rcinfoname, Common_Utils.rcimage, (view, i) -> substring3(Common_Utils.rcids[i], Common_Utils.rcinfoname[i]));
        reconfirmation.setAdapter(rInformationAdapter);
    }


    public void substring3(int a, String str) {
        Intent intent = new Intent(this, RC_InfoWEB_Activity.class);
        intent.putExtra("number", a);
        intent.putExtra("name", str);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
