package com.rtovehicle.information.carbikeinfo.activity;



import static net.yslibrary.android.keyboardvisibilityevent.util.UIUtil.hideKeyboard;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputFilter.AllCaps;
import android.text.InputFilter.LengthFilter;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.MediaView;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.koushikdutta.async.http.body.UrlEncodedFormBody;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.koushikdutta.ion.builder.LoadBuilder;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;

import org.jetbrains.annotations.NotNull;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class Search_DrivingLicence_Activity extends AppCompatActivity {
    public int day;
    public int month;
    public int year;
    EditText editTextReg3;
    String strReg1;
    String strReg3;
    String str_dlCov;
    String str_dlCovCat;
    String str_dlCovIss;
    String str_dlCurSta;
    String str_dlHazVal;
    String str_dlHilVal;
    String str_dlIss;
    String str_dlNam;
    String str_dlNontFr;
    String str_dlNontTo;
    String str_dlNumDob;
    String str_dlTra;
    String str_dlTraFr;
    String str_dlTraTo;
    Common_Utils helper;
    NativeAd nativeAds;
    private FrameLayout adContainerView;
    Dialog dialog;
    boolean keyboard;

    public Search_DrivingLicence_Activity() {
        String str = "";
        this.str_dlTra = str;
        this.str_dlNumDob = str;
        this.str_dlNam = str;
        this.str_dlIss = str;
        this.str_dlNontFr = str;
        this.str_dlCurSta = str;
        this.str_dlNontTo = str;
        this.str_dlTraFr = str;
        this.str_dlHazVal = str;
        this.str_dlTraTo = str;
        this.str_dlHilVal = str;
        this.str_dlCovCat = str;
        this.str_dlCov = str;
        this.str_dlCovIss = str;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        this.helper = new Common_Utils(this, this);
        setContentView(R.layout.activity_search_drivinglicence);
        getWindow().setBackgroundDrawable(null);
        keyboard = false;
        this.helper.checkConnectivity();
        findid();
        NativeadsLoad();
        click();
    }

    public void findid() {
        findViewById(R.id.back_all).setOnClickListener(v -> onBackPressed());
        adContainerView = findViewById(R.id.adsframe);
    }

    @SuppressLint({"SimpleDateFormat", "ClickableViewAccessibility"})
    public void click() {
        final EditText editText = findViewById(R.id.reg1);
        editText.setOnTouchListener((view, motionEvent) -> {
            keyboard = true;
            return false;
        });
        editText.setFilters(new InputFilter[]{new LengthFilter(18), new AllCaps()});
        this.editTextReg3 = findViewById(R.id.reg3);
        editTextReg3.setOnTouchListener((view, motionEvent) -> {
            keyboard = true;
            return false;
        });
        editTextReg3.setOnClickListener(v -> {
            Calendar instance = Calendar.getInstance();
            new SimpleDateFormat("dd-MM-yyyy");
            Search_DrivingLicence_Activity.this.year = instance.get(1);
            Search_DrivingLicence_Activity.this.month = instance.get(2);
            Search_DrivingLicence_Activity.this.day = instance.get(5);
            DatePickerDialog datePickerDialog = new DatePickerDialog(Search_DrivingLicence_Activity.this, (datePicker, i, i2, i3) -> {
                String valueOf = String.valueOf(i3);
                String valueOf2 = String.valueOf(i2 + 1);
                EditText editText1 = Search_DrivingLicence_Activity.this.editTextReg3;
                String str = "-";
                String sb = valueOf +
                        str +
                        valueOf2 +
                        str +
                        i;
                editText1.setText(sb);
            }, Search_DrivingLicence_Activity.this.year, Search_DrivingLicence_Activity.this.month, Search_DrivingLicence_Activity.this.day);
            datePickerDialog.show();
        });

        findViewById(R.id.btn_search).setOnClickListener(view -> {
            Search_DrivingLicence_Activity.this.strReg1 = editText.getText().toString();
            Search_DrivingLicence_Activity drivingLicenceActivity = Search_DrivingLicence_Activity.this;
            drivingLicenceActivity.strReg3 = drivingLicenceActivity.editTextReg3.getText().toString();
            if (Search_DrivingLicence_Activity.this.strReg1.isEmpty()) {
                editText.setError("Please enter DL number");
                editText.requestFocus();
            } else if (Search_DrivingLicence_Activity.this.strReg3.isEmpty()) {
                Search_DrivingLicence_Activity.this.editTextReg3.setError("Please your Date of Birth");
                Search_DrivingLicence_Activity.this.editTextReg3.requestFocus();
            } else {
                ShowDialog();
                new GetData().execute(Search_DrivingLicence_Activity.this.strReg1, Search_DrivingLicence_Activity.this.strReg3);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        keyboard = false;
    }

    public void ShowError() {
        if (!isFinishing() && dialog.isShowing()) {
            dialog.dismiss();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Data Not Found");
        builder.setMessage("DL Details are not available for this number.\nPlease check your DL number.");
        builder.setPositiveButton("OK", (dialogInterface, i) -> {
        });
        builder.show();
    }

    public boolean GetFromSource3(String str, String str2) {
        try {
            Connection.Response execute = Jsoup.connect("https://parivahan.gov.in/rcdlstatus/?pur_cd=101").followRedirects(true).ignoreHttpErrors(true).method(Connection.Method.GET).execute();
            if (execute.statusCode() <= 500) {
                Map<String, String> cookies = execute.cookies();
                Document parse = Jsoup.parse(execute.body());
                Element first = parse.getElementsByAttributeValue("name", "javax.faces.ViewState").first();
                if (first == null) {
                    first = parse.getElementById("j_id1:javax.faces.ViewState:0");
                }
                String attr = first.attr("value");
                String trim = Jsoup.parse(execute.body()).getElementsByAttributeValueStarting("id", "form_rcdl:j_idt").select("button").get(0).attr("id").trim();
                String replace = Jsoup.connect(Common_Utils.src6).followRedirects(true).method(Connection.Method.POST).cookies(cookies).referrer("https://parivahan.gov.in/rcdlstatus/?pur_cd=101").header("Content-Type", UrlEncodedFormBody.CONTENT_TYPE).header("Host", "parivahan.gov.in").header("Accept", "application/xml, text/xml, */*; q=0.01").header("Accept-Language", "en-US,en;q=0.5").header("Accept-Encoding", "gzip, deflate, br").header("X-Requested-With", "XMLHttpRequest").header("Faces-Request", "partial/ajax").header("Origin", "https://parivahan.gov.in").userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36").data("javax.faces.partial.ajax", "true").data("javax.faces.source", trim).data("javax.faces.partial.execute", "@all").data("javax.faces.partial.render", "form_rcdl:pnl_show form_rcdl:pg_show form_rcdl:rcdl_pnl").data(trim, trim).data("form_rcdl", "form_rcdl").data("form_rcdl:tf_dlNO", str).data("form_rcdl:tf_dob_input", str2).data("javax.faces.ViewState", attr).execute().body().replace("<![CDATA[", "");
                if (replace.contains("No DL Details Found")) {
                    return false;
                }
                Document parse2 = Jsoup.parse(replace);
                Element first2 = parse2.select("table").first();
                Element element = parse2.select("table").get(1);
                Element element2 = parse2.select("table").get(2);
                Element element3 = parse2.select("table").get(3);
                if (first2 == null) {
                    return false;
                }
                this.str_dlNumDob = str + " : " + str2;
                this.str_dlCurSta = first2.select("tr").get(0).select("td").get(1).text();
                this.str_dlNam = first2.select("tr").get(1).select("td").get(1).text();
                this.str_dlIss = first2.select("tr").get(2).select("td").get(1).text();
                this.str_dlTra = first2.select("tr").get(3).select("td").get(1).text();
                this.str_dlNontFr = element.select("tr").get(0).select("td").get(1).text();
                this.str_dlNontTo = "NA";
                this.str_dlTraFr = element.select("tr").get(1).select("td").get(1).text();
                this.str_dlTraTo = "NA";
                this.str_dlHazVal = element2.select("tr").get(0).select("td").get(1).text();
                this.str_dlHilVal = "NA";
                this.str_dlCovCat = element3.select("tr").get(1).select("td").get(0).text();
                this.str_dlCov = element3.select("tr").get(1).select("td").get(1).text();
                this.str_dlCovIss = element3.select("tr").get(1).select("td").get(2).text();
                PrintStream printStream = System.out;
                printStream.println("text 1: " + this.str_dlNontTo);
                return true;
            }
            this.helper.Log("Something wrong!");
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    public void CacheDetails() {
        HashMap info = this.helper.getInfo();
        info.put("dl_num", Collections.singletonList(this.strReg1));
        info.put("dl_dob", Collections.singletonList(this.strReg3));
        info.put("dl_cur_sta", Collections.singletonList(this.str_dlCurSta));
        info.put("dl_nam", Collections.singletonList(this.str_dlNam));
        info.put("dl_doi", Collections.singletonList(this.str_dlIss));
        info.put("dl_las_tra", Collections.singletonList(this.str_dlTra));
        info.put("dl_non_tra_fr", Collections.singletonList(this.str_dlNontFr));
        info.put("dl_non_tra_to", Collections.singletonList(this.str_dlNontTo));
        info.put("dl_tra_fr", Collections.singletonList(this.str_dlTraFr));
        info.put("dl_tra_to", Collections.singletonList(this.str_dlTraTo));
        info.put("dl_haz_val", Collections.singletonList(this.str_dlHazVal));
        info.put("dl_hil_val", Collections.singletonList(this.str_dlHilVal));
        info.put("dl_cov_cat", Collections.singletonList(this.str_dlCovCat));
        info.put("dl_cov", Collections.singletonList(this.str_dlCov));
        info.put("dl_cov_iss", Collections.singletonList(this.str_dlCovIss));
        LoadBuilder with = Ion.with(this);
        ((Builders.Any.B) with.load(Common_Utils.src2)).setBodyParameters(info).asString().setCallback((exc, str) -> {
            if (exc != null) {
                exc.printStackTrace();
            }
            if (!isFinishing() && dialog.isShowing()) {
                dialog.dismiss();
            }
            Search_DrivingLicence_Activity.this.ShowResults();
        });
    }

    public void ShowResults() {
        Intent intent = new Intent(this, Owner_DrivingLicence_Info_Activity.class);
        intent.putExtra(Common_Utils.DL_NUM, this.strReg1);
        intent.putExtra(Common_Utils.DL_DOB, this.strReg3);
        intent.putExtra(Common_Utils.DL_CUR_STA, this.str_dlCurSta);
        intent.putExtra(Common_Utils.DL_NAM, this.str_dlNam);
        intent.putExtra(Common_Utils.DL_DOI, this.str_dlIss);
        intent.putExtra(Common_Utils.DL_LAS_TRA, this.str_dlTra);
        intent.putExtra(Common_Utils.DL_NON_TRA, this.str_dlNontFr);
        intent.putExtra(Common_Utils.DL_NON_TRA_TO, this.str_dlNontTo);
        intent.putExtra(Common_Utils.DL_TRA, this.str_dlTraFr);
        intent.putExtra(Common_Utils.DL_TRA_TO, this.str_dlTraTo);
        intent.putExtra(Common_Utils.DL_HAZ_VAL, this.str_dlHazVal);
        intent.putExtra(Common_Utils.DL_HIL_VAL, this.str_dlHilVal);
        intent.putExtra(Common_Utils.DL_COV_CAT, this.str_dlCovCat);
        intent.putExtra(Common_Utils.DL_COV, this.str_dlCov);
        intent.putExtra(Common_Utils.DL_COV_ISS, this.str_dlCovIss);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @SuppressLint("StaticFieldLeak")
    private class GetData extends AsyncTask<String, Void, Boolean> {
        private GetData() {
        }


        public Boolean doInBackground(String... strArr) {
            return Search_DrivingLicence_Activity.this.GetFromSource3(strArr[0], strArr[1]);
        }


        public void onPostExecute(Boolean bool) {
            if (bool) {
                Search_DrivingLicence_Activity.this.CacheDetails();
            } else {
                Search_DrivingLicence_Activity.this.ShowError();
            }
        }
    }

    private void NativeadsLoad() {

        AdLoader.Builder builder = new AdLoader.Builder(this, MyApplication.get_Admob_native_Id())
                .forNativeAd(nativeAd -> {

                    nativeAds = nativeAd;

                    @SuppressLint("InflateParams") NativeAdView adView = (NativeAdView) getLayoutInflater()
                            .inflate(R.layout.small_native, null);
                    populateUnifiedNativeAdView(nativeAd, adView);
                    adContainerView.removeAllViews();
                    adContainerView.addView(adView);

                });
        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(@NonNull @NotNull LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        }).build();

        adLoader.loadAd(new AdRequest.Builder().build());


    }


    public void onBackPressed() {

        if (keyboard) {
            keyboard = false;
            hideKeyboard(Search_DrivingLicence_Activity.this);
        } else {
            finish();
        }
    }


    public void populateUnifiedNativeAdView(NativeAd nativeAd, NativeAdView adView) {
        MediaView mediaView = adView.findViewById(R.id.ad_media);
        adView.setMediaView(mediaView);
        adView.setHeadlineView(adView.findViewById(R.id.ad_headline));
        adView.setBodyView(adView.findViewById(R.id.ad_body));
        adView.setCallToActionView(adView.findViewById(R.id.ad_call_to_action));
        adView.setIconView(adView.findViewById(R.id.ad_app_icon));
        ((TextView) Objects.requireNonNull(adView.getHeadlineView())).setText(nativeAd.getHeadline());
        if (nativeAd.getBody() == null) {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getBodyView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        }
        if (nativeAd.getCallToAction() == null) {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.INVISIBLE);
        } else {
            Objects.requireNonNull(adView.getCallToActionView()).setVisibility(View.VISIBLE);
            ((TextView) adView.getCallToActionView()).setText(nativeAd.getCallToAction());
        }
        if (nativeAd.getIcon() == null) {
            Objects.requireNonNull(adView.getIconView()).setVisibility(View.GONE);
        } else {
            ((ImageView) Objects.requireNonNull(adView.getIconView())).setImageDrawable(
                    nativeAd.getIcon().getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }
        adView.setNativeAd(nativeAd);
    }

    private void ShowDialog() {
        dialog = new Dialog(Search_DrivingLicence_Activity.this,
                android.R.style.Theme_Material_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_progress);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        dialog.getWindow().setAttributes(lp);
        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (nativeAds != null) {
            nativeAds.destroy();
        }
    }
}
