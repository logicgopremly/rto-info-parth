package com.rtovehicle.information.carbikeinfo.activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;
import com.rtovehicle.information.carbikeinfo.common.MyApplication;
import com.rtovehicle.information.carbikeinfo.model.Model_SuperVehicle;

import java.util.ArrayList;

public class SuperVehicleShow_Activity extends AppCompatActivity {
    ArrayList<Model_SuperVehicle.Super_Data> whatsNewModelArrayList;
    LinearLayout banner;
    TextView albumArtImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supervehicalshow);
        getWindow().setBackgroundDrawable(null);
        banner();
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        int i = getIntent().getIntExtra("abc", 0);
        whatsNewModelArrayList = new ArrayList<>();
        whatsNewModelArrayList = Common_Utils.superlist;

        if (whatsNewModelArrayList != null) {
            Model_SuperVehicle.Super_Data superModel = whatsNewModelArrayList.get(i);
            ((TextView) findViewById(R.id.vehicalname)).setText(superModel.getName());
            ((TextView) findViewById(R.id.vehicalmaker)).setText(superModel.getMaker());
            ((TextView) findViewById(R.id.vehicalprice)).setText(superModel.getExpected_price());
            ((TextView) findViewById(R.id.vehicalMileage)).setText(superModel.getMileage());
            ((TextView) findViewById(R.id.vehicalCapacity)).setText(superModel.getCapicity());
            ((TextView) findViewById(R.id.vehicaltopspeed)).setText(superModel.getTop_speed());
            ((TextView) findViewById(R.id.vehicalPower)).setText(superModel.getPower());
            ((TextView) findViewById(R.id.vehicallaunchdate)).setText(superModel.getExpected_launch());
            ((TextView) findViewById(R.id.vehicaldescription)).setText(superModel.getDescription1());
            Glide.with(this).load(superModel.getImage()).listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    return false;
                }
            }).into((ImageView) findViewById(R.id.vehicalimage));
        }


    }


    public void banner() {
        banner = findViewById(R.id.adaptive);
        albumArtImage = findViewById(R.id.albumArtImage);
        AdView adView = new AdView(this);
        adView.setAdUnitId(MyApplication.get_Admob_banner_Id());
        banner.removeAllViews();
        banner.addView(adView);
        loadBanner(adView);

    }


    private void loadBanner(AdView adView) {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.setAdSize(AdSize.MEDIUM_RECTANGLE);
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                albumArtImage.setVisibility(View.GONE);
            }

            @Override
            public void onAdFailedToLoad(@NonNull LoadAdError adError) {
                albumArtImage.setVisibility(View.GONE);
            }

            @Override
            public void onAdOpened() {
            }

            @Override
            public void onAdClicked() {
            }

            @Override
            public void onAdClosed() {
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
