package com.rtovehicle.information.carbikeinfo.adapter;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.nativead.NativeAdView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.activity.Owner_Vehicleinfo_Activity;
import com.rtovehicle.information.carbikeinfo.model.Model_HistoryVehicle;
import com.rtovehicle.information.carbikeinfo.common.DataBaseHelper;
import com.rtovehicle.information.carbikeinfo.common.Common_Utils;

import java.util.ArrayList;


public class HistoryVehicle_Adapter extends RecyclerView.Adapter<HistoryVehicle_Adapter.myh> {
    final Activity context;
    final ArrayList<Model_HistoryVehicle> model_historyVehicleArrayList;
    DataBaseHelper dataBaseHelper;

    public HistoryVehicle_Adapter(Activity context, ArrayList<Model_HistoryVehicle> favorite_arr) {
        this.context = context;
        this.model_historyVehicleArrayList = favorite_arr;
    }

    @NonNull
    @Override
    public myh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.history__search_vehicle_item, parent, false);
        return new myh(view);
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public void onBindViewHolder(@NonNull myh holder, @SuppressLint("RecyclerView") final int position) {

        dataBaseHelper = new DataBaseHelper(context);

        holder.ownername_text.setText(model_historyVehicleArrayList.get(position).getRc_owner_name());
        holder.number_vehicle_text.setText(model_historyVehicleArrayList.get(position).getRc_regn_no());

        holder.next_lin.setOnClickListener(view -> {
            Common_Utils.click_db = 1;
            Intent intent = new Intent(context, Owner_Vehicleinfo_Activity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("db_vehicle", model_historyVehicleArrayList.get(position).getRc_owner_name());
            intent.putExtra("db_vehicle1", model_historyVehicleArrayList.get(position).getRc_regn_no());
            intent.putExtra("db_vehicle2", model_historyVehicleArrayList.get(position).getRc_regn_dt());
            intent.putExtra("db_vehicle3", model_historyVehicleArrayList.get(position).getRc_maker_desc());
            intent.putExtra("db_vehicle4", model_historyVehicleArrayList.get(position).getRc_maker_model());
            intent.putExtra("db_vehicle5", model_historyVehicleArrayList.get(position).getRc_eng_no());
            intent.putExtra("db_vehicle6", model_historyVehicleArrayList.get(position).getRc_chasi_no());
            intent.putExtra("db_vehicle7", model_historyVehicleArrayList.get(position).getRc_fuel_desc());
            intent.putExtra("db_vehicle8", model_historyVehicleArrayList.get(position).getRc_vh_class_desc());
            intent.putExtra("db_vehicle9", model_historyVehicleArrayList.get(position).getRc_insurance_upto());
            intent.putExtra("db_vehicle10", model_historyVehicleArrayList.get(position).getRc_fit_upto());
            context.startActivity(intent);
        });

        holder.delete.setOnClickListener(view -> {
            final View alertLayout = context.getLayoutInflater().inflate(R.layout.history_delete_dailog, null);
            TextView notnow = alertLayout.findViewById(R.id.tv_later);
            TextView delete = alertLayout.findViewById(R.id.tv_submit);
            android.app.AlertDialog.Builder alert = new android.app.AlertDialog.Builder(context);
            alert.setView(alertLayout);
            alert.setCancelable(true);
            final android.app.AlertDialog dialog = alert.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialog.show();

            notnow.setOnClickListener(v -> dialog.dismiss());
            delete.setOnClickListener(v -> {
                dialog.dismiss();
                dataBaseHelper.Deletevehicle(model_historyVehicleArrayList.get(position).getId());
                notifyDataSetChanged();
                model_historyVehicleArrayList.remove(position);
            });
        });

       /* holder.delete.setOnClickListener(v -> new AlertDialog.Builder(context)
                .setIcon(R.drawable.ic_alert)
                .setTitle("Are you sure to delete")
                .setPositiveButton("Yes", (dialogInterface, i) -> {
                    dataBaseHelper.Deletevehicle(model_historyVehicleArrayList.get(position).getId());
                    notifyDataSetChanged();
                    model_historyVehicleArrayList.remove(position);
                    dialogInterface.dismiss();
                })
                .setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss())
                .show());
*/
    }


    @Override
    public int getItemCount() {
        return model_historyVehicleArrayList.size();
    }

    public static class myh extends RecyclerView.ViewHolder {

        final ImageView delete;
        final LinearLayout next_lin;
        final TextView ownername_text;
        final TextView number_vehicle_text;


        public myh(@NonNull View itemView) {
            super(itemView);
            ownername_text = itemView.findViewById(R.id.txvName);
            number_vehicle_text = itemView.findViewById(R.id.txvRegNo);
            delete = itemView.findViewById(R.id.delete_item);
            next_lin = itemView.findViewById(R.id.next_lin);
        }
    }

}
