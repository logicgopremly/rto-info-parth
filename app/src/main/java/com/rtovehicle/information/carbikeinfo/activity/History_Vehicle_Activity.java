package com.rtovehicle.information.carbikeinfo.activity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.rtovehicle.information.carbikeinfo.R;
import com.rtovehicle.information.carbikeinfo.adapter.HistoryVehicle_Adapter;
import com.rtovehicle.information.carbikeinfo.model.Model_HistoryVehicle;
import com.rtovehicle.information.carbikeinfo.common.DataBaseHelper;

import java.util.ArrayList;
import java.util.Collections;

public class History_Vehicle_Activity extends AppCompatActivity {
    RecyclerView history_recycleview;
    ArrayList<Model_HistoryVehicle> model_historyVehicleArrayList = new ArrayList<>();
    DataBaseHelper dataBaseHelper;
    TextView no_fav;
    LottieAnimationView nodata_vehicle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_vehicle);
        getWindow().setBackgroundDrawable(null);
        init();

        try (Cursor c = dataBaseHelper.getvehicle()) {
            if (c != null) {
                if (c.moveToFirst()) {
                    do {
                        Model_HistoryVehicle model_historyVehicle = new Model_HistoryVehicle();
                        model_historyVehicle.setId(c.getInt(0));
                        model_historyVehicle.setDatafrom(c.getString(1));
                        model_historyVehicle.setRc_owner_name(c.getString(2));
                        model_historyVehicle.setRc_regn_no(c.getString(3));
                        model_historyVehicle.setRc_maker_desc(c.getString(4));
                        model_historyVehicle.setRc_maker_model(c.getString(5));
                        model_historyVehicle.setRc_regn_dt(c.getString(6));
                        model_historyVehicle.setRc_fuel_desc(c.getString(7));
                        model_historyVehicle.setRc_vh_class_desc(c.getString(8));
                        model_historyVehicle.setRc_registered_at(c.getString(9));
                        model_historyVehicle.setRc_eng_no(c.getString(10));
                        model_historyVehicle.setRc_chasi_no(c.getString(11));
                        model_historyVehicle.setRc_insurance_upto(c.getString(12));
                        model_historyVehicle.setRc_fit_upto(c.getString(13));
                        model_historyVehicle.setRc_norms_desc(c.getString(14));
                        model_historyVehicleArrayList.add(model_historyVehicle);

                    } while (c.moveToNext());
                }
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Collections.reverse(model_historyVehicleArrayList);
        history_recycleview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        history_recycleview.setAdapter(new HistoryVehicle_Adapter(this, model_historyVehicleArrayList));

    }


    private void init() {
        model_historyVehicleArrayList = new ArrayList<>();
        findViewById(R.id.back_all).setOnClickListener(v -> onBackPressed());
        history_recycleview = findViewById(R.id.history_rc);
        nodata_vehicle = findViewById(R.id.nodata_vehicle);
        no_fav = findViewById(R.id.no_fav);
        model_historyVehicleArrayList.clear();
        dataBaseHelper = new DataBaseHelper(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (model_historyVehicleArrayList.size() > 0) {
            no_fav.setVisibility(View.GONE);
            nodata_vehicle.setVisibility(View.GONE);
        } else {
            no_fav.setVisibility(View.VISIBLE);
            nodata_vehicle.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

