package com.rtovehicle.information.carbikeinfo.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Model_MostTrending {
    @SerializedName("status")
    boolean status;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    public ArrayList<DATA_ALL_Most> rto_All_most;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DATA_ALL_Most> getRto_All_most() {
        return rto_All_most;
    }

    public void setRto_All_most(ArrayList<DATA_ALL_Most> rto_All_most) {
        this.rto_All_most = rto_All_most;
    }

    public static class DATA_ALL_Most {
        @SerializedName("id")
        private int id;

        @SerializedName("category_name")
        private String category_name;

        @SerializedName("image")
        private String image;

        @SerializedName("vehicle_details")
        ArrayList<vehicle_details_most> vehicle_details_most;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public ArrayList<DATA_ALL_Most.vehicle_details_most> getVehicle_details_most() {
            return vehicle_details_most;
        }

        public void setVehicle_details_most(ArrayList<DATA_ALL_Most.vehicle_details_most> vehicle_details_most) {
            this.vehicle_details_most = vehicle_details_most;
        }

        public static class vehicle_details_most {
            @SerializedName("id")
            private String id;

            @SerializedName("category_id")
            private String category_id;

            @SerializedName("name")
            private String name;


            @SerializedName("vehicle_number")
            private String vehicle_number;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getCategory_id() {
                return category_id;
            }

            public void setCategory_id(String category_id) {
                this.category_id = category_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getVehicle_number() {
                return vehicle_number;
            }

            public void setVehicle_number(String vehicle_number) {
                this.vehicle_number = vehicle_number;
            }
        }

    }

}
