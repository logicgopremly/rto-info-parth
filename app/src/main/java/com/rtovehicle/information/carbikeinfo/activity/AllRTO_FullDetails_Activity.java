package com.rtovehicle.information.carbikeinfo.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rtovehicle.information.carbikeinfo.R;


public class AllRTO_FullDetails_Activity extends AppCompatActivity {
    private TextView RTOAddress;
    private TextView RTOCity;
    private TextView RTOCode;
    private TextView RTOPhone;
    private TextView RTO_URL;
    private TextView Title;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_allrto_fulldetails);
        findID();
        set_data();
    }

    public void findID() {
        findViewById(R.id.back_all).setOnClickListener(view -> onBackPressed());
        RTOCity = findViewById(R.id.rtoCityValue);
        RTOCode = findViewById(R.id.rtoCodeValue);
        RTOAddress = findViewById(R.id.rtoAddressValue);
        RTOPhone = findViewById(R.id.rtoPhoneValue);
        RTO_URL = findViewById(R.id.rtoURLValue);
        Title = findViewById(R.id.id_header);
    }

    private void set_data() {

        if (!isNetworkConnected(this)) {
            Toast.makeText(AllRTO_FullDetails_Activity.this, getResources().getString(R.string.you_are_not_connected_to_the_internet), Toast.LENGTH_SHORT).show();
        } else {
            Title.setText(getIntent().getStringExtra("tv_title"));
            RTOCity.setText(getIntent().getStringExtra("tv_district"));
            RTOCode.setText(getIntent().getStringExtra("tv_code"));
            RTOAddress.setText(getIntent().getStringExtra("tv_address"));
            RTOPhone.setText(getIntent().getStringExtra("tv_phone"));
            RTO_URL.setText(getIntent().getStringExtra("tv_link"));
        }
    }

    public static boolean isNetworkConnected(Context context) {
        @SuppressLint("WrongConstant") ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return false;
        }
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
